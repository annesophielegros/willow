.PHONY: help
.DEFAULT_GOAL = help

SYMFONY_CONSOLE= php bin/console

clean-vendor: cc-hard ## Suppression du répertoire vendor puis un réinstall
	rm -Rf vendor
	rm composer.lock
	$(COMPOSER) install

cc:	## Vider le cache
	$(SYMFONY_CONSOLE) c:c

clean-db: ## Réinitialiser la base de donnée
	- $(SYMFONY_CONSOLE) d:d:d --force --connection
	$(SYMFONY_CONSOLE) d:d:c
	$(SYMFONY_CONSOLE) d:m:m --no-interaction
	$(SYMFONY_CONSOLE) d:f:l --no-interaction

clean-db-test: ## Réinitialiser la base de donnée en environnement de test
	- $(SYMFONY_CONSOLE) d:d:d --force --env=test
	$(SYMFONY_CONSOLE) d:d:c --env=test
	$(SYMFONY_CONSOLE) d:m:m --no-interaction --env=test
	$(SYMFONY_CONSOLE) d:f:l --no-interaction --env=test

test-unit: ## Lancement des tests unitaire
	php bin/phpunit tests/Unit/

test-func:	## Lancement des tests fonctionnel
	php bin/phpunit tests/Func/

tests: test-func test-unit	## Lancement de tous tests

phpstan: ## Lancement du php cs
	php vendor/bin/phpstan analyse src tests

phpcs: ## Lancement du php cs
	phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src

phpcbf: ## Lancement du phpcbf
	phpcbf

tests: ## Launch tests
	php bin/phpunit --testdox

migrations:
	$(SYMFONY_CONSOLE) make:migration
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate