<?php

namespace App\Builder;

class OptionRequestorBuilder
{
    private $build;

    public function __construct(array $options)
    {
        $this->build = $options;
    }

    public function replaceJson(string $key, ?string $value): void
    {
        $this->build['json'] = str_replace("{{$key}}", $value, $this->build['json']);
    }

    public function replaceBody(string $key, ?string $value): void
    {
        $this->build['body'] = str_replace("{{$key}}", $value, $this->build['body']);
    }

    public function rateDateReplace(string $key, ?string $value): void
    {
        $datePrepared = preg_replace('/T.*$/', 'T23:00:00.000Z', $value);
        $this->build['json'] = str_replace("{{$key}}", $datePrepared, $this->build['json']);
    }

    public function get(): array
    {
        return $this->build;
    }
}
