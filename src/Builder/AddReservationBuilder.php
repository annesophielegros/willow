<?php

namespace App\Builder;

use App\Entity\Brand;
use App\Factory\RequestorFactory;
use Doctrine\Persistence\ManagerRegistry;

class AddReservationBuilder
{
    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    private $em;

    public function __construct(
        RequestorFactory $requestorFactory,
        ManagerRegistry $doctrine
    ) {
        $this->requestorFactory = $requestorFactory;
        $this->em = $doctrine->getManager();
    }

    public function addReservationOption(Brand $brand, $enquiry, $optionDate)
    {
        // Todo with Jay for pxier
        $optionalStatus = $this->em->getRepository('App:Status')->findOptionalByBrand($brand->getId());
        $status = $optionalStatus->getProviderID();
        $requestor = $this->requestorFactory->createSendToPms($brand);
        $customerInfo = $this->addCustomer($enquiry, $requestor, $brand);
        $enquiryPre = $requestor->createReservation($enquiry, $status, $optionDate);
        $enquiries = $enquiryPre->getEnquiry();
        $providerId = $enquiries[0]->getId();
        $enquiry->setProviderId($providerId);
        $enquiry->setStatus($optionalStatus);
    }

    public function addReservationConfirmed(Brand $brand, $enquiry)
    {
        // Todo with Jay for pxier
        $requestor = $this->requestorFactory->createSendToPms($brand);
        $customerInfo = $this->addCustomer($enquiry, $requestor, $brand);
    }

    private function addCustomer($enquiry, $requestor, $brand)
    {
        if ($enquiry->getCompany()) {
            $company = $enquiry->getCompany();
        } else {
            $company = $this->em->getRepository('App:Company')->findDefaultByBrand($brand);
        }
        return $requestor->createCustomer($enquiry, $company);
    }
}

