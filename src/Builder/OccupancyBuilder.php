<?php

namespace App\Builder;

use App\Factory\RequestorFactory;
use Doctrine\ORM\EntityManagerInterface;

class OccupancyBuilder
{
    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        RequestorFactory $requestorFactory,
        EntityManagerInterface $entityManager
    ) {
        $this->requestorFactory = $requestorFactory;
        $this->entityManager = $entityManager;
    }

    public function findOccupancy($enquiry)
    {
        $brand = $enquiry->getProperty()->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $availabilities = $requestor->getAvailabilities($enquiry);
        // TODO once Mews advise how to get room quantity

        return 56.3487;
    }
}
