<?php

namespace App\Builder;

use App\Entity\Property;
use App\Entity\RateType;
use App\Factory\RequestorFactory;
use App\Generator\EncryptDecryptGenerator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RateTypeUpdateBuilder
{
    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    private $doctrine;

    private $session;

    public function __construct(
        RequestorFactory $requestorFactory,
        ManagerRegistry $doctrine,
        SessionInterface $session
    ) {
        $this->requestorFactory = $requestorFactory;
        $this->doctrine = $doctrine;
        $this->session = $session;
    }

    public function updateRate(Property $property, $rateType)
    {
        $brand = $property->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $brandToken = $brand->getApiKeyGetData();
        $propertyToken = $property->getApiKey();
        $ratesProvider = $requestor->getRateList($brandToken, $propertyToken);
        $defaultRateRule = $this->doctrine->getRepository('App:RateRule')
            ->findDefaultRateRuleByProperty($property->getId());
        $defaultRateType = $defaultRateRule->getRateType();
        foreach ($rateType as $rateWillow) {
            if (array_key_exists($rateWillow->getProviderId(), $ratesProvider)) {
                $rateWillow->setLabel($ratesProvider[$rateWillow->getProviderId()]->getName());
            } else {
                if ($rateWillow == $defaultRateType) {
                    $this->session->getFlashBag()->add(
                        'warning',
                        'you must change the rate type of your default rate rule before updating this rate type'
                    );
                } else {
                    $rateWillow->setActivated(false);
                }
            }
        }

        $rateTypeWillowId = array_map(function ($r) {
            return $r->getProviderId();
        }, $rateType);

        foreach ($ratesProvider as $rP) {
            if (!in_array($rP->getId(), $rateTypeWillowId)) {
                $rateTypeNew = new RateType();
                $rateTypeNew->setLabel($rP->getName());
                $rateTypeNew->setActivated(true);
                $rateTypeNew->setProperty($property);
                $rateTypeNew->setProviderId($rP->getId());
                $this->doctrine->getManager()->persist($rateTypeNew);
            }
        }
        $this->doctrine->getManager()->flush();
    }
}
