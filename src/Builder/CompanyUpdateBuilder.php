<?php

namespace App\Builder;

use App\Entity\Address;
use App\Entity\Brand;
use App\Entity\Company;
use App\Factory\RequestorFactory;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CompanyUpdateBuilder
{
    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    private $doctrine;

    private $session;

    public function __construct(
        RequestorFactory $requestorFactory,
        ManagerRegistry $doctrine,
        SessionInterface $session
    ) {
        $this->requestorFactory = $requestorFactory;
        $this->doctrine = $doctrine;
        $this->session = $session;
    }

    public function updateCompany(Brand $brand, $companies)
    {
        $requestor = $this->requestorFactory->create($brand);
        $companyProvider = $requestor->getCompanyList();
        foreach ($companies as $companyWillow) {
            if (array_key_exists($companyWillow->getProviderId(), $companyProvider)) {
                $correctCompany = $companyProvider[$companyWillow->getProviderId()];
                $companyWillow->setLabel($correctCompany->getName());
                $companyWillow->getAddress()->setPostalCode($correctCompany->getPostalCode());
                $companyWillow->getAddress()->setCity($correctCompany->getCity());
                $companyWillow->getAddress()->setAddressLine1($correctCompany->getLine1());
                $companyWillow->getAddress()->setAddressLine2($correctCompany->getLine2());
                if ($companyWillow->getAddress()->getCountry()) {
                    $companyWillow->getAddress()->getCountry()->setAbbreviation($correctCompany->getCountryCode());
                }
                $companyWillow->getAddress()->setPhoneNumber($correctCompany->getTelephone());
            } else {
                $companyWillow->setActivated(false);
            }
        }

        $companyWillowId = array_map(function ($c) {
            return $c->getProviderId();
        }, $companies);

        foreach ($companyProvider as $cP) {
            if (!in_array($cP->getId(), $companyWillowId)) {
                $companyNew = new Company();
                $companyNew->setLabel($cP->getName());
                $companyNew->setBrand($brand);
                $companyNew->setProviderId($cP->getId());
                $companyNew->setDefaultSelection(false);
                $address = new Address();
                $address->setPhoneNumber($cP->getTelephone());
                $address->setAddressLine1($cP->getLine1());
                $address->setAddressLine2($cP->getLine2());
                $address->setCity($cP->getCity());
                $address->setPostalCode($cP->getPostalCode());
                $companyNew->setAddress($address);
                $this->doctrine->getManager()->persist($companyNew);
            }
        }
        $this->doctrine->getManager()->flush();
    }

    public function addCompany(Company $company, Brand $brand)
    {
        $requestor = $this->requestorFactory->create($brand);
        $companiesPre = $requestor->createCompany($company);
        $companies = $companiesPre->getCompanies();
        $providerId = $companies[0]->getId();
        $company->setProviderId($providerId);
    }

    public function editCompany(Company $company, Brand $brand)
    {
        $requestor = $this->requestorFactory->create($brand);
        $requestor->updateCompany($company);
    }
}
