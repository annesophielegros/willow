<?php

namespace App\Builder\Reporting;

use App\Model\Reporting\RateEnquiriesModel;
use Doctrine\Persistence\ManagerRegistry;

class SummaryReportBuilder
{
    private $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
    }

    public function getConfirmedEnquiriesByBrand($brandId)
    {
        $properties = $this->entityManager->getRepository('App:Property')->findAllByBrand($brandId);
        $confirmedEnquiries = new RateEnquiriesModel();
        $propertyNames = [];
        $lastMonthRates = [];
        $lastYearRates = [];
        foreach ($properties as $property) {
            $idP = $property->getId();
            $createdLastMonth = $this->entityManager->getRepository('App:Enquiry')->findCreatedByPLastMonth($idP);
            $confLastMonth = $this->entityManager->getRepository('App:Enquiry')->findConfirmedByPLastMonth($idP);
            if ($createdLastMonth['count'] === 0) {
                $monthlyRate = 0;
            } else {
                $monthlyRateNotRounded = $confLastMonth['count'] / $createdLastMonth['count'] * 100;
                $monthlyRate = round($monthlyRateNotRounded, 2);
            }
            $lastMonthRates[] = $monthlyRate;
            $createdLastYear = $this->entityManager->getRepository('App:Enquiry')->findCreatedByPLastYear($idP);
            $confLastYear = $this->entityManager->getRepository('App:Enquiry')->findConfirmedByPLastYear($idP);
            if ($createdLastYear['count'] === 0) {
                $yearlyRate = 0;
            } else {
                $yearlyRateNotRounded = $confLastYear['count'] / $createdLastYear['count'] * 100;
                $yearlyRate = round($yearlyRateNotRounded, 2);
            }
            $lastYearRates[] = $yearlyRate;
            $propertyNames[] = $property->getLabel();
        }
        $confirmedEnquiries->setProperties($propertyNames);
        $confirmedEnquiries->setLastMonthRate($lastMonthRates);
        $confirmedEnquiries->setLastYearRate($lastYearRates);
        return $confirmedEnquiries;
    }

    public function getCancelledEnquiriesByBrand($brandId)
    {
        $properties = $this->entityManager->getRepository('App:Property')->findAllByBrand($brandId);
        $cancelledEnquiries = new RateEnquiriesModel();
        $cancelledEnquiries->setProperties($properties);
        $lastMonthRates = [];
        $lastYearRates = [];
        $propertyNames = [];
        foreach ($properties as $property) {
            $idP = $property->getId();
            $createdLastMonth = $this->entityManager->getRepository('App:Enquiry')->findCreatedByPLastMonth($idP);
            $cancelledLastMonth = $this->entityManager->getRepository('App:Enquiry')->findCancelledByPLastMonth($idP);
            if ($createdLastMonth['count'] === 0) {
                $monthlyRate = 0;
            } else {
                $monthlyRateNotRounded = $cancelledLastMonth['count'] / $createdLastMonth['count'] * 100;
                $monthlyRate = round($monthlyRateNotRounded, 2);
            }
            $lastMonthRates[] = $monthlyRate;
            $createdLastYear = $this->entityManager->getRepository('App:Enquiry')->findCreatedByPLastYear($idP);
            $cancelledLastYear = $this->entityManager->getRepository('App:Enquiry')->findCancelledByPLastYear($idP);
            if ($createdLastYear['count'] === 0) {
                $yearlyRate = 0;
            } else {
                $yearlyRateNotRounded = $cancelledLastYear['count'] / $createdLastYear['count'] * 100;
                $yearlyRate = round($yearlyRateNotRounded, 2);
            }
            $lastYearRates[] = $yearlyRate;
            $propertyNames[] = $property->getLabel();
        }
        $cancelledEnquiries->setProperties($propertyNames);
        $cancelledEnquiries->setLastMonthRate($lastMonthRates);
        $cancelledEnquiries->setLastYearRate($lastYearRates);
        return $cancelledEnquiries;
    }
}
