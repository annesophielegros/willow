<?php

namespace App\Builder\Reporting;

use App\Model\Reporting\SalesByCompanyModel;
use Doctrine\Persistence\ManagerRegistry;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SalesCompanyReportBuilder
{
    private $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
    }

    public function generateReport($form)
    {
        $criteria = $form->getData();
        if ($criteria['account_manager']) {
            $table = $this->entityManager->getRepository('App:Enquiry')->searchSalesByUserReport($criteria);
        } else {
            $table = $this->entityManager->getRepository('App:Enquiry')->searchSalesCompanyReport($criteria);
        }
        $companies = [];
        foreach ($table as $enquiry) {
            if (!array_key_exists($enquiry->getCompany()->getId(), $companies)) {
                $company = new SalesByCompanyModel();
                $company->setCompany($enquiry->getCompany());
                $company->setCompanyLabel($enquiry->getCompany()->getLabel());
                $userName = $enquiry->getCompany()->getUser()->getFirstName()
                    . ' ' . $enquiry->getCompany()->getUser()->getLastName();
                $company->setUser($userName);
                $companies[$company->getCompany()->getId()] = $company;
            }
        }
        foreach ($companies as $company) {
            $count = 1;
            $totalAmount = 0;
            foreach ($table as $enquiry) {
                if ($enquiry->getCompany()->getId() === $company->getCompany()->getId()) {
                    if ($enquiry->getOffer()->getRooms()) {
                        foreach ($enquiry->getOffer()->getRooms() as $room) {
                            $nights = $room->getCheckInDate()->diff($room->getCheckOutDate())->format("%r%a");
                            $amount = number_format($nights) * $room->getRoomQuantity() * $room->getAverageRate();
                            $totalAmount = $totalAmount + $amount;
                        }
                    }
                    $company->setTotalAmount($totalAmount);
                    $company->setProperty($enquiry->getProperty()->getLabel());
                    $company->setEnquiryNumber($count++);
                }
            }
        }
        return $companies;
    }

    public function extractExcel($enquiryReportForm)
    {
        $companies = $this->generateReport($enquiryReportForm);
        $data = [];
        foreach ($companies as $company) {
            $data[] = [
                'property' => $company->getProperty(),
                'account_manager' => $company->getUser(),
                'company' => $company->getCompanyLabel(),
                'enquiry_number' => $company->getEnquiryNumber(),
                'total_amount' => $company->getTotalAmount()
            ];
        }
        $excelData = [];
        $spreadsheet = new Spreadsheet();
        $firstLine = true;
        foreach ($data as $dataRow) {
            if ($firstLine) {
                $excelData[] = array_keys($dataRow);
                $firstLine = false;
            }
            $excelData[] = array_values($dataRow);
        }
        $spreadsheet->getActiveSheet()->fromArray($excelData);

        return new Xlsx($spreadsheet);
    }
}
