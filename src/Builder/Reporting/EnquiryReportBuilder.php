<?php

namespace App\Builder\Reporting;

use App\Model\Reporting\EnquiryReportModel;
use Doctrine\Persistence\ManagerRegistry;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class EnquiryReportBuilder
{
    private $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
    }

    public function generateReport($form, $brandId)
    {
        $criteria = $form->getData();
        if ($criteria['property']) {
            $table = $this->entityManager->getRepository('App:Enquiry')->searchEnquiryReportByProp($criteria);
        } else {
            $table = $this->entityManager->getRepository('App:Enquiry')->searchEnquiryReport($criteria, $brandId);
        }
        $enquiries = [];
        foreach ($table as $enquiry) {
            $enquiryReport = new EnquiryReportModel();
            $enquiryReport->setId($enquiry->getId());
            $enquiryReport->setProperty($enquiry->getProperty()->getLabel());
            $enquiryReport->setEnquiryDate(date_format($enquiry->getEnquiryDate(), 'Y-m-d'));
            if ($enquiry->getCompany()) {
                $enquiryReport->setCompanyLabel($enquiry->getCompany()->getLabel());
            }
            $enquiryReport->setGroupName($enquiry->getGroupName());
            $enquiryReport->setRefNumber($enquiry->getReservationNumber());
            $enquiryReport->setStatus($enquiry->getStatus()->getLabel());
            $enquiryReport->setCheckInDate(date_format($enquiry->getCheckInDate(), 'Y-m-d'));
            $enquiryReport->setCheckOutDate(date_format($enquiry->getCheckOutDate(), 'Y-m-d'));
            $nights = $enquiry->getCheckInDate()->diff($enquiry->getCheckOutDate())->format("%r%a");
            $enquiryReport->setNights(number_format($nights));
            $enquiryReport->setPersons($enquiry->getNumberOfPersons());
            if ($enquiry->getAge()) {
                $enquiryReport->setAgeRange($enquiry->getAge()->getLabel());
            }
            if ($enquiry->getCountry()) {
                $enquiryReport->setNationality($enquiry->getCountry()->getLabel());
            }
            if ($enquiry->getOffer()) {
                if ($enquiry->getOffer()->getRooms()) {
                    $totalAmount = 0;
                    foreach ($enquiry->getOffer()->getRooms() as $room) {
                        $nights = $room->getCheckInDate()->diff($room->getCheckOutDate())->format("%r%a");
                        $amount = number_format($nights) * $room->getRoomQuantity() * $room->getAverageRate();
                        $totalAmount = $totalAmount + $amount;
                    }
                    $enquiryReport->setTotalAmount($totalAmount);
                }
            }
            if ($enquiry->getHotelComment()) {
                $enquiryReport->setComments($enquiry->getHotelComment());
            }
            $enquiries[] = $enquiryReport;
        }
        return $enquiries;
    }

    public function extractExcel($enquiryReportForm, $brandId)
    {
        $enquiries = $this->generateReport($enquiryReportForm, $brandId);
        $data = [];
        foreach ($enquiries as $enquiry) {
            $data[] = [
                'property' => $enquiry->getProperty(),
                'enquiry_date' => $enquiry->getEnquiryDate(),
                'company' => $enquiry->getCompanyLabel(),
                'group_name' => $enquiry->getGroupName(),
                'reference_number' => $enquiry->getRefNumber(),
                'status' => $enquiry->getStatus(),
                'check_in_date' => $enquiry->getCheckInDate(),
                'check_out_date' => $enquiry->getCheckOutDate(),
                'nights' => $enquiry->getNights(),
                'persons' => $enquiry->getPersons(),
                'age_range' => $enquiry->getAgeRange(),
                'nationality' => $enquiry->getNationality(),
                'total_amount' => $enquiry->getTotalAmount(),
                'comments' => $enquiry->getComments(),
            ];
        }
        $excelData = [];
        $spreadsheet = new Spreadsheet();
        $firstLine = true;
        foreach ($data as $dataRow) {
            if ($firstLine) {
                $excelData[] = array_keys($dataRow);
                $firstLine = false;
            }
            $excelData[] = array_values($dataRow);
        }
        $spreadsheet->getActiveSheet()->fromArray($excelData);

        return new Xlsx($spreadsheet);
    }
}
