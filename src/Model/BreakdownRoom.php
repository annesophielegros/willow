<?php

namespace App\Model;

use App\Entity\RoomType;

class BreakdownRoom
{
    /**
     * @var RoomType
     */
    private $roomType;

    /**
     * @var int
     */
    private $availability;

    /**
     * @var int
     */
    private $bedsAvailable;

    /**
     * @var float
     */
    private $percentageAvailable;

    /**
     * @var float
     */
    private $paxNumberPerPercentage;

    /**
     * @var int
     */
    private $PAXRounddown;


    /**
     * @var int
     */
    private $roomsBreakdown = 0;

    /**
     * @var int
     */
    private $roomsRounddown = 0;

    /**
     * @var int
     */
    private $roomsPriority = 0;

    /**
     * @var int
     */
    private $roomsTooBig = 0;

    /**
     * @var int
     */
    private $roomsPicked = 0;

    /**
     * @var int
     */
    private $emptyBeds = 0;

    /**
     * @return RoomType
     */
    public function getRoomType(): RoomType
    {
        return $this->roomType;
    }

    /**
     * @param RoomType $roomType
     */
    public function setRoomType(RoomType $roomType): void
    {
        $this->roomType = $roomType;
    }

    /**
     * @return ?int
     */
    public function getAvailability(): ?int
    {
        return $this->availability;
    }

    /**
     * @param int $availability
     */
    public function setAvailabilityAndBeds(int $availability): void
    {
        if (is_null($this->availability) || $availability < $this->availability) {
            $this->availability = $availability;
            $this->bedsAvailable = $availability * $this->roomType->getCapacity();
        }
    }

    public function setAvailability(int $availability): void
    {
        $this->availability = $availability;
    }

    /**
     * @return int
     */
    public function getBedsAvailable(): int
    {
        return $this->bedsAvailable;
    }

    /**
     * @param int $bedsAvailable
     */
    public function setBedsAvailable(int $bedsAvailable): void
    {
        $this->bedsAvailable = $bedsAvailable;
    }

    /**
     * @return float
     */
    public function getPercentageAvailable(): float
    {
        return $this->percentageAvailable;
    }

    /**
     * @param int $totalBedsAvailble
     */
    public function calcPercentageAvailable(int $totalBedsAvailble): void
    {
        $this->percentageAvailable = $this->bedsAvailable / $totalBedsAvailble;
    }

    /**
     * @return float
     */
    public function getPaxNumberPerPercentage(): float
    {
        return $this->paxNumberPerPercentage;
    }

    /**
     * @param int $paxNumber
     */
    public function calcPaxNumberPerPercentage(int $paxNumber): void
    {
        $this->paxNumberPerPercentage = $this->percentageAvailable * $paxNumber;
    }

    /**
     * @return int
     */
    public function getRoomsRounddown(): int
    {
        return $this->roomsRounddown;
    }

    public function calcRoomsRounddown(): void
    {
        $this->roomsRounddown = (int)floor($this->paxNumberPerPercentage / $this->roomType->getCapacity());
    }

    /**
     * @return int
     */
    public function getPAXRounddown(): int
    {
        return $this->PAXRounddown;
    }

    public function calcPAXRounddown(): void
    {
        $this->PAXRounddown = $this->roomsRounddown * $this->roomType->getCapacity();
    }

    /**
     * @return int
     */
    public function getRoomsBreakdown(): int
    {
        return $this->roomsBreakdown;
    }

    /**
     * @param int $roomsBreakdown
     */
    public function setRoomsBreakdown(int $roomsBreakdown): void
    {
        $this->roomsBreakdown = $roomsBreakdown;
    }

    /**
     * @param int $roomsBreakdown
     */
    public function incrementRoomsBreakdown(int $roomsBreakdown): void
    {
        $this->roomsBreakdown += $roomsBreakdown;
    }

    /**
     * @return int
     */
    public function getRoomsPriority(): int
    {
        return $this->roomsPriority;
    }

    /**
     * @param int $roomsPriority
     */
    public function setRoomsPriority(int $roomsPriority): void
    {
        $this->roomsPriority = $roomsPriority;
    }

    /**
     * @return int
     */
    public function getRoomsTooBig(): int
    {
        return $this->roomsTooBig;
    }

    /**
     * @param int $roomsTooBig
     */
    public function setRoomsTooBig(int $roomsTooBig): void
    {
        $this->roomsTooBig = $roomsTooBig;
    }

    /**
     * @return int
     */
    public function getRoomsPicked(): int
    {
        return $this->roomsPicked;
    }

    /**
     * @param int $roomsPicked
     */
    public function incrementRoomsPicked(int $roomsPicked): void
    {
        $this->roomsPicked += $roomsPicked;
    }

    public function resetRoomsRounddown(): void
    {
        $this->roomsRounddown = 0;
    }

    public function resetRoomsPicked(): void
    {
        $this->roomsPicked = 0;
    }

    /**
     * @return int
     */
    public function getEmptyBeds(): int
    {
        return $this->emptyBeds;
    }

    /**
     * @param int $emptyBeds
     */
    public function incrementEmptyBeds(int $emptyBeds): void
    {
        $this->emptyBeds += $emptyBeds;
    }

    public function decrementAvailability(int $quantity)
    {
        $this->availability -= $quantity;
        if ($this->availability < 0) {
            $this->availability = 0;
        }
    }

    public function decrementBedsAvailable(int $numberOfBeds)
    {
        $this->bedsAvailable -= $numberOfBeds;
        if ($this->bedsAvailable < 0) {
            $this->bedsAvailable = 0;
        }
    }
}
