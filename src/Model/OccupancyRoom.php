<?php

namespace App\Model;

use App\Entity\RoomType;

class OccupancyRoom
{
    /**
     * @var RoomType
     */
    private $roomType;

    /**
     * @var OccupancyDates[]
     */
    private $occupancyDates;

    /**
     * @var int
     */
    private $lowestAvailability;

    /**
     * @return ?int
     */
    public function getLowestAvailability(): ?int
    {
        return $this->lowestAvailability;
    }

    /**
     * @param int $lowestAvailability
     */
    public function setLowestAvailability(int $lowestAvailability): void
    {
        if (is_null($this->lowestAvailability) || $lowestAvailability < $this->lowestAvailability) {
            $this->lowestAvailability = $lowestAvailability;
        }
    }

    /**
     * @return OccupancyDates[]
     */
    public function getOccupancyDates(): array
    {
        return $this->occupancyDates;
    }

    /**
     * @param OccupancyRoom[] $occupancyDates
     */
    public function setOccupancyDates(array $occupancyDates): void
    {
        $this->occupancyDates = $occupancyDates;
    }

    /**
     * @return RoomType
     */
    public function getRoomType(): RoomType
    {
        return $this->roomType;
    }

    /**
     * @param RoomType $roomType
     */
    public function setRoomType(RoomType $roomType): void
    {
        $this->roomType = $roomType;
    }
}
