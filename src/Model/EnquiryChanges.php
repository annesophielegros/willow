<?php

namespace App\Model;

class EnquiryChanges
{
    /**
     * @var int
     */
    private $numberOfPersons;

    /**
     * @var \DateTimeInterface
     */
    private $checkInDate;

    /**
     * @var \DateTimeInterface
     */
    private $checkOutDate;

    /**
     * @return ?int
     */
    public function getNumberOfPersons(): ?int
    {
        return $this->numberOfPersons;
    }

    public function setNumberOfPersons(?int $numberOfPersons): void
    {
        $this->numberOfPersons = $numberOfPersons;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCheckInDate(): ?\DateTimeInterface
    {
        return $this->checkInDate;
    }

    /**
     * @param \DateTimeInterface|null $checkInDate
     */
    public function setCheckInDate(?\DateTimeInterface $checkInDate): void
    {
        $this->checkInDate = $checkInDate;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCheckOutDate(): ?\DateTimeInterface
    {
        return $this->checkOutDate;
    }

    /**
     * @param \DateTimeInterface|null $checkOutDate
     */
    public function setCheckOutDate(?\DateTimeInterface $checkOutDate): void
    {
        $this->checkOutDate = $checkOutDate;
    }
}
