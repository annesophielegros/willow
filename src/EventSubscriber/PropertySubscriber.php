<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class PropertySubscriber implements EventSubscriberInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(RouterInterface $router, SessionInterface $session)
    {
        $this->router = $router;
        $this->session = $session;
    }

    public function onKernelController(ControllerEvent $event)
    {
        if (!in_array($event->getRequest()->get('_route'), $this->getRoutes())) {
            return;
        }
        if (null === $event->getRequest()->getSession()->get('property_id')) {
            $this->session->getFlashBag()->add('warning', 'No property selected');
            $event->setController(function () {
                return new RedirectResponse($this->router->generate('dashboard'));
            });
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    private function getRoutes(): array
    {
        return [
            'revenue_configuration',
            'preferences',
            'property_configuration',
            'breakdown_rule_add_new',
            'breakdown_rule_add_edit',
            'breakdown_rule_add_delete',
            'breakdown_rule_remaining_new',
            'breakdown_rule_remaining_edit',
            'breakdown_rule_remaining_delete',
            'breakdown_rule_total_new',
            'breakdown_rule_total_edit',
            'breakdown_rule_total_delete',
            'extra_rule_new',
            'extra_rule_edit',
            'extra_rule_delete',
            'filter_index',
            'filter_new',
            'filter_show',
            'filter_edit',
            'filter_delete',
            'rate_rule_new',
            'rate_rule_default',
            'rate_rule_edit',
            'rate_rule_delete',
            'rate_type_new',
            'rate_type_edit',
            'rate_type_delete',
            'extra_new',
            'extra_edit',
            'extra_delete',
            'room_type_new',
            'room_type_edit',
            'room_type_delete',
            'conditions_index',
            'conditions_new',
            'conditions_edit',
            'conditions_delete',
            'mail_template_new',
            'mail_template_edit',
            'mail_template_delete',
            'presentation_new',
            'presentation_edit',
            'presentation_delete',
            'template_proposal_new',
            'template_proposal_edit',
            'template_proposal_delete',
        ];
    }
}
