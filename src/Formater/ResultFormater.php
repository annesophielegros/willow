<?php

namespace App\Formater;

use App\Entity\RoomType;

class ResultFormater
{
    /**
     * @param RoomType[] $roomTypes
     * @return RoomType[]
     */
    public function changeKeyByName(array $roomTypes): array
    {
        $formated = [];
        foreach ($roomTypes as $item) {
            $formated[$item->getLabel()] = $item;
        }

        return $formated;
    }

    /**
     * @param RoomType[] $roomTypes
     * @return RoomType[]
     */
    public function changeKeyByProviderId(array $roomTypes): array
    {
        $formated = [];
        foreach ($roomTypes as $item) {
            $formated[$item->getProviderId()] = $item;
        }

        return $formated;
    }
}
