<?php

namespace App\Controller;

use App\Entity\BreakdownRuleRemaining;
use App\Form\BreakdownRuleRemainingType;
use App\Repository\BreakdownRuleRemainingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/breakdown/rule/remaining")
 */
class BreakdownRuleRemainingController extends AbstractController
{
    /**
     * @Route("/new", name="breakdown_rule_remaining_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $breakdownRuleRemaining = new BreakdownRuleRemaining();
        $form = $this->createForm(BreakdownRuleRemainingType::class, $breakdownRuleRemaining);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($breakdownRuleRemaining);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_remaining');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('breakdown_rule_remaining/new.html.twig', [
            'breakdown_rule_remaining' => $breakdownRuleRemaining,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="breakdown_rule_remaining_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BreakdownRuleRemaining $breakdownRuleRemaining): Response
    {
        $this->denyAccessUnlessGranted('edit', $breakdownRuleRemaining);
        $form = $this->createForm(BreakdownRuleRemainingType::class, $breakdownRuleRemaining);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_remaining');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('breakdown_rule_remaining/edit.html.twig', [
            'breakdown_rule_remaining' => $breakdownRuleRemaining,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="breakdown_rule_remaining_delete", methods={"DELETE"})
     */
    public function delete(Request $request, BreakdownRuleRemaining $breakdownRuleRemaining): Response
    {
        $this->denyAccessUnlessGranted('delete', $breakdownRuleRemaining);
        if ($this->isCsrfTokenValid('delete' . $breakdownRuleRemaining->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($breakdownRuleRemaining);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_remaining');
        }

        return $this->redirectToRoute('revenue_configuration');
    }
}
