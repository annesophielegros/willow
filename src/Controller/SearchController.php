<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class SearchController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    /**
     * @Route("search/{item}", name="search", requirements={"id"="\w+"}, options={"expose"=true})
     */
    public function search($item): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_ADMIN_BRAND')) {
            $brand = $this->security->getUser()->getBrand();
            if (!$brand) {
                $brandId = $this->container->get('session')->get('brand')->getId();
            } else {
                $brandId = $brand->getId();
            }
            $properties = $entityManager->getRepository('App:Property')->findAllByBrand($brandId);
            $propertyList = array_map(function ($property) {
                return $property->getId();
            }, $properties);
        } else {
            $propertyList = $this->getUser()->getPropertyListid();
        }

        $enquiries = $this->getDoctrine()->getRepository('App:Enquiry')->search($propertyList, $item);

        return $this->json($enquiries);
    }
}
