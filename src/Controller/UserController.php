<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Entity\User;
use App\Form\UserActivationType;
use App\Form\UserPasswordFormType;
use App\Form\UserType;
use App\Generator\TokenGenerator;
use App\Repository\UserRepository;
use DateInterval;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        if ($this->isGranted('ROLE_ADMIN_WILLOW')) {
            $brand = $this->container->get('session')->get('brand');
        } else {
            $user = $this->security->getUser();
            $brand = $user->getBrand();
        }
        return $this->render('user/index.html.twig', [
            'brand' => $brand,
            'users' => $userRepository->findByBrandAndRole(
                $brand,
                ['ROLE_REVENUE_MANAGER', 'ROLE_RESERVATION_AGENT', 'ROLE_ADMIN_BRAND']
            ),
        ]);
    }

    /**
     * @Route("/new/{id}", name="user_new", methods={"GET","POST"}, requirements={"id"="\d+?"})
     */
    public function new(Request $request, Brand $brand = null): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setBrand($brand);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('reset_password', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/switch/{id}", name="user_switch", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function switchUser(User $user, Request $request): Response
    {
        if ($user->getActivated()) {
            $user->setActivated(0);
        } else {
            $user->setActivated(1);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
