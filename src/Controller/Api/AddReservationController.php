<?php

namespace App\Controller\Api;

use App\Builder\AddReservationBuilder;
use App\Builder\CompanyUpdateBuilder;
use App\Entity\Brand;
use App\Entity\Enquiry;
use App\Form\OptionDateType;
use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/enquiry")
 */
class AddReservationController extends AbstractController
{
    /**
     * @Route("/add/{id}/provider/option", name="add_provider_option", methods={"GET","POST"})
     */
    public function addOption(Enquiry $enquiry, AddReservationBuilder $addReservation, Request $request): Response
    {
        $brand = $enquiry->getProperty()->getBrand();
        $dateForm = $this->createForm(OptionDateType::class);
        $dateForm->handleRequest($request);
        if ($dateForm->isSubmitted() && $dateForm->isValid()) {
            $optionDate = $dateForm['date']->getData();
            $addReservation->addReservationOption($brand, $enquiry, $optionDate);
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/add/{id}/provider/confirmed", name="add_provider_confirmed", methods={"GET","POST"})
     */
    public function addConfirmed(Enquiry $enquiry, AddReservationBuilder $addReservation, Request $request): Response
    {
        $brand = $enquiry->getProperty()->getBrand();
        $addReservation->addReservationConfirmed($brand, $enquiry);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($request->headers->get('referer'));
    }
}
