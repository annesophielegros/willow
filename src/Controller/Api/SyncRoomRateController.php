<?php

namespace App\Controller\Api;

use App\Builder\RateTypeUpdateBuilder;
use App\Builder\RoomTypeUpdateBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/property-configuration")
 */
class SyncRoomRateController extends AbstractController
{
    /**
     * @Route("/update", name="update_room_rate", methods={"GET","POST"})
     */
    public function sync(RateTypeUpdateBuilder $rateBuilder, RoomTypeUpdateBuilder $roomBuilder): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $property = $this->getDoctrine()->getManager()->getRepository('App:Property')->find($propertyId);
        $rates = $this->getDoctrine()->getManager()->getRepository('App:RateType')->findByProperty($propertyId);
        $rooms = $this->getDoctrine()->getManager()->getRepository('App:RoomType')->findByProperty($propertyId);
        $rateBuilder->updateRate($property, $rates);
        $roomBuilder->updateRoom($property, $rooms);
        return $this->redirectToRoute('property_configuration');
    }
}
