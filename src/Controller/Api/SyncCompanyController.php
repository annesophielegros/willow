<?php

namespace App\Controller\Api;

use App\Builder\CompanyUpdateBuilder;
use App\Entity\Brand;
use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/company/brand")
 */
class SyncCompanyController extends AbstractController
{
    /**
     * @Route("/updateApi/{id}", name="update_companies", methods={"GET","POST"})
     */
    public function sync(
        CompanyUpdateBuilder $companyBuilder,
        Brand $brand,
        CompanyRepository $companyRepository
    ): Response {
        $companies = $companyRepository->findByBrand($brand->getId());
        $companyBuilder->updateCompany($brand, $companies);
        return $this->redirectToRoute('company_index');
    }
}
