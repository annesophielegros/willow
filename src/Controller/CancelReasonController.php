<?php

namespace App\Controller;

use App\Entity\CancelReason;
use App\Form\CancelReasonType;
use App\Service\BrandIfAdminWillow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/cancel_reason")
 */
class CancelReasonController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/new", name="cancel_reason_new", methods={"GET","POST"})
     */
    public function new(Request $request, BrandIfAdminWillow $ifAdminWillow): Response
    {
        $cancelReason = new CancelReason();
        $form = $this->createForm(CancelReasonType::class, $cancelReason);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $session = $this->container->get('session');
            $roleAdminWillow = $this->isGranted('ROLE_ADMIN_WILLOW');
            $brand = $ifAdminWillow->getBrandViaRepoDependingOnUser($session, $roleAdminWillow);
            $cancelReason->setBrand($brand);
            $entityManager->persist($cancelReason);
            $entityManager->flush();

            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('cancel_reason/new.html.twig', [
            'cancel' => $cancelReason,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cancel_reason_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CancelReason $cancelReason): Response
    {
        $form = $this->createForm(CancelReasonType::class, $cancelReason);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('cancel_reason/edit.html.twig', [
            'cancel' => $cancelReason,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("{id}", name="cancel_reason_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CancelReason $cancelReason): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $brand = $cancelReason->getBrand();
        if ($this->isCsrfTokenValid('delete' . $cancelReason->getId(), $request->request->get('_token'))) {
            $cxlIsUsed = $entityManager->getRepository('App:Enquiry')->findCxlReasonByBrand($brand, $cancelReason);
            if ($cxlIsUsed) {
                $this->addFlash('error', 'This cancel reason is used in an enquiry, you cannot delete it.');
            } else {
                $entityManager->remove($cancelReason);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('brand_preferences');
    }
}