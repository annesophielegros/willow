<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEditType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileSettingsController extends AbstractController
{
    /**
     * @Route("/profile/{id}", name="profile", methods={"GET", "POST"})
     */
    public function index(Request $request, User $user): Response
    {
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('user_id', $user->getId());

            return $this->redirectToRoute('profile', [
                'id' => $user->getId()
            ]);
        }

        return $this->render('profile_settings/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
