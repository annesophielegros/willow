<?php

namespace App\Controller;

use App\Builder\CompanyUpdateBuilder;
use App\Entity\Company;
use App\Form\CompanyNewType;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/company")
 */
class CompanyController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/brand", name="company_index", methods={"GET"})
     */
    public function index(CompanyRepository $companyRepository): Response
    {
        if ($this->isGranted('ROLE_ADMIN_WILLOW')) {
            $brand = $this->container->get('session')->get('brand');
        } else {
            $brand = $this->security->getUser()->getBrand();
        }
        $brandId = $brand->getId();

        return $this->render('company/index.html.twig', [
            'companies' => $companyRepository->findByBrand($brandId),
            'brand' => $brand,
        ]);
    }

    /**
     * @Route("/enquiry/new", name="company_enquiry_new", methods={"GET","POST"})
     */
    public function newViaEnquiry(Request $request, CompanyUpdateBuilder $builder): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $property = $this->getDoctrine()->getRepository('App:Property')->find($propertyId);
        $brand = $property->getBrand();
        $newCompany = new Company();
        $newCompanyForm = $this->createForm(CompanyNewType::class, $newCompany);
        $newCompanyForm->handleRequest($request);
        if ($newCompanyForm->isSubmitted() && $newCompanyForm->isValid()) {
            if ($newCompanyForm->get('save')->isClicked()) {
                $newCompany->setBrand($brand);
                $builder->addCompany($newCompany, $brand);
                $this->getDoctrine()->getManager()->persist($newCompany);
                $this->getDoctrine()->getManager()->flush();
            }
        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/enquiry/{id}/edit", name="company_enquiry_edit", methods={"GET","POST"})
     */
    public function editViaEnquiry(Request $request, Company $company, CompanyUpdateBuilder $builder): Response
    {
        $form = $this->createForm(CompanyNewType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $brand = $company->getBrand();
                $builder->editCompany($company, $brand);
                $this->getDoctrine()->getManager()->flush();
            }
        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/brand/new", name="company_brand_new", methods={"GET","POST"})
     */
    public function newViaBrand(Request $request, CompanyUpdateBuilder $builder): Response
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->security->getUser();
            $brandId = $user->getBrand()->getId();
            $brand = $entityManager->getRepository('App:Brand')->find($brandId);
            $company->setBrand($brand);
            $builder->addCompany($company, $brand);
            $entityManager->persist($company);
            $entityManager->flush();

            return $this->redirectToRoute('company_index');
        }

        return $this->render('company/new.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/brand/{id}/edit", name="company_brand_edit", methods={"GET","POST"})
     */
    public function editViaBrand(Request $request, Company $company, CompanyUpdateBuilder $builder): Response
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $brand = $company->getBrand();
            $builder->editCompany($company, $brand);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('company_index');
        }

        return $this->render('company/edit.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }
}
