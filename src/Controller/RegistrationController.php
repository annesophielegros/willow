<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ForgetMailType;
use App\Form\UserPasswordFormType;
use App\Generator\TokenGenerator;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/register")
 */
class RegistrationController extends AbstractController
{
    private $mailer;
    private $userPasswordEncoder;
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        $this->mailer = $mailer;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/reset/{id}", name="reset_password", requirements={"id"="\d+"})
     */
    public function resetPassword(Request $request, User $user): Response
    {
        if ($user->getResetPassword()) {
            throw new \Exception("Your not allowed", 403);
        }
        $user->setResetPassword(1);
        $user->setExpirationDate((new \DateTime())->add(new DateInterval('P1D')));
        $user->setToken((new TokenGenerator())->create($user));
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $email = new TemplatedEmail();
        $email->from('support@willow-solution.com');
        $email->to($user->getEmail());
        $email->subject("Willow's password moment");
        $email->htmlTemplate('registration/confirmation_email.html.twig');
        $email->context([
            'user' => $user,
        ]);

        $this->mailer->send($email);

        $this->addFlash('success', 'The user password has been reset, 
        time to check mails even the spam and update the password.');

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/forget", name="forget_password")
     */
    public function forgetPassword(Request $request): Response
    {
        $form = $this->createForm(ForgetMailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $forget_mail = $request->request->get('forget_mail');
            $mail = $forget_mail['mail'];
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $mail]);

            return $this->redirectToRoute('reset_password', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('registration/forget_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/password_form/{token}",
     *      name="user_password_form",
     *      methods={"GET","POST"},
     *      requirements={"token":"[\w+]{64}"}
     *     )
     */
    public function passwordForm(Request $request, string $token): Response
    {
        $user = $this->getDoctrine()->getRepository('App:User')->getByToken($token);
        if (!$user || !$user->getResetPassword()) {
            throw new \Exception("Your not allowed", 403);
        }
        $today = new \DateTime();
        if ($user->getExpirationDate() < $today) {
            throw new \Exception("Your token is expired, please ask another one", 403);
        }
        $form = $this->createForm(UserPasswordFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encodedPassword = $this->userPasswordEncoder->encodePassword(
                $user,
                $user->getPassword()
            );
            $user->setToken(null);
            $user->setResetPassword(0);
            $user->setActivated(1);
            $user->setExpirationDate(null);
            $user->setPassword($encodedPassword);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Your password has been created, you can now log in.');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('user/password/form.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
