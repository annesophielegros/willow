<?php

namespace App\Controller;

use App\Entity\BreakdownRuleTotal;
use App\Form\BreakdownRuleTotalType;
use App\Repository\BreakdownRuleTotalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/breakdown/rule/total")
 */
class BreakdownRuleTotalController extends AbstractController
{
    /**
     * @Route("/new", name="breakdown_rule_total_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $breakdownRuleTotal = new BreakdownRuleTotal();
        $form = $this->createForm(BreakdownRuleTotalType::class, $breakdownRuleTotal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($breakdownRuleTotal);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_total');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('breakdown_rule_total/new.html.twig', [
            'breakdown_rule_total' => $breakdownRuleTotal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="breakdown_rule_total_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BreakdownRuleTotal $breakdownRuleTotal): Response
    {
        $this->denyAccessUnlessGranted('edit', $breakdownRuleTotal);
        $form = $this->createForm(BreakdownRuleTotalType::class, $breakdownRuleTotal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_total');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('breakdown_rule_total/edit.html.twig', [
            'breakdown_rule_total' => $breakdownRuleTotal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="breakdown_rule_total_delete", methods={"DELETE"})
     */
    public function delete(Request $request, BreakdownRuleTotal $breakdownRuleTotal): Response
    {
        $this->denyAccessUnlessGranted('delete', $breakdownRuleTotal);
        if ($this->isCsrfTokenValid('delete' . $breakdownRuleTotal->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($breakdownRuleTotal);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_total');
        }

        return $this->redirectToRoute('revenue_configuration');
    }
}
