<?php

namespace App\Controller\OnlineForm;

use App\Builder\OnlineForm\GeneratorBuilder;
use App\Model\OnlineForm\NewRequestGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/onlineForm/generator")
 */
class GeneratorController extends AbstractController
{
    /**
     * @Route("", name="generator_form", methods={"POST"})
     */
    public function newEnquiry(Request $request, SerializerInterface $serializer, GeneratorBuilder $builder): Response
    {
        $response = $request->getContent();
        /** @var NewRequestGenerator $form */
        $form = $serializer->deserialize($response, NewRequestGenerator::class, 'json');
        try {
            $resNumber = $builder->newEnquiry($form);
        } catch (\Exception $exception) {
            dd($exception);
        }


        return $this->json(
            ['reservation_id' => $resNumber],
            Response::HTTP_CREATED
        );
    }
}
