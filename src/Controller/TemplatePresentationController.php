<?php

namespace App\Controller;

use App\Entity\TemplatePresentation;
use App\Entity\Property;
use App\Form\TemplatePresentationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hotel_presentation")
 */
class TemplatePresentationController extends AbstractController
{
    /**
     * @Route("/new", name="presentation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $hotelPresentation = new TemplatePresentation();
        $form = $this->createForm(TemplatePresentationType::class, $hotelPresentation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($hotelPresentation);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-preference', 'presentation');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('hotel_presentation/new.html.twig', [
            'presentations' => $hotelPresentation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="presentation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TemplatePresentation $hotelPresentation): Response
    {
        $this->denyAccessUnlessGranted('edit', $hotelPresentation);
        $form = $this->createForm(TemplatePresentationType::class, $hotelPresentation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'presentation');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('hotel_presentation/edit.html.twig', [
            'presentation' => $hotelPresentation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="presentation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TemplatePresentation $hotelPresentation): Response
    {
        $this->denyAccessUnlessGranted('delete', $hotelPresentation);
        if ($this->isCsrfTokenValid('delete' . $hotelPresentation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($hotelPresentation);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-preference', 'presentation');
        }

        return $this->redirectToRoute('preferences');
    }
}
