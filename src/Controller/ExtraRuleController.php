<?php

namespace App\Controller;

use App\Entity\ExtraRule;
use App\Form\ExtraRuleType;
use App\Repository\ExtraRuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/extra_rule")
 */
class ExtraRuleController extends AbstractController
{
    /**
     * @Route("/new", name="extra_rule_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $extraRule = new ExtraRule();
        $form = $this->createForm(ExtraRuleType::class, $extraRule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($extraRule);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'extra_rules');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('extra_rule/new.html.twig', [
            'extra_rule' => $extraRule,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="extra_rule_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ExtraRule $extraRule): Response
    {
        $this->denyAccessUnlessGranted('edit', $extraRule);
        $form = $this->createForm(ExtraRuleType::class, $extraRule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'extra_rules');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('extra_rule/edit.html.twig', [
            'extra_rule' => $extraRule,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_rule_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ExtraRule $extraRule): Response
    {
        $this->denyAccessUnlessGranted('delete', $extraRule);
        if ($this->isCsrfTokenValid('delete' . $extraRule->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($extraRule);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'extra_rules');
        }

        return $this->redirectToRoute('revenue_configuration');
    }
}
