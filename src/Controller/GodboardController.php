<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Entity\Property;
use App\Entity\Status;
use App\Entity\User;
use App\Form\BrandType;
use App\Form\PropertyType;
use App\Form\UserAdminType;
use App\Formater\BrandRepositoryFormater;
use App\Generator\EncryptDecryptGenerator;
use App\Repository\BrandRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/godboard")
 */
class GodboardController extends AbstractController
{
    /**
     * @var BrandRepositoryFormater
     */
    private $brandRepositoryFormater;

    private $encryptDecryptGenerator;

    public function __construct(
        BrandRepositoryFormater $brandRepositoryFormater,
        EncryptDecryptGenerator $encryptDecryptGenerator
    ) {
        $this->brandRepositoryFormater = $brandRepositoryFormater;
        $this->encryptDecryptGenerator = $encryptDecryptGenerator;
    }

    /**
     * @Route("/", name="godboard_index")
     */
    public function index(): Response
    {
        return $this->render('godboard/index.html.twig');
    }

    /**
     * @Route("/admin/new", name="new_willow_admin", methods={"GET","POST"})
     */
    public function newWillowAdmin(Request $request, Brand $brand = null): Response
    {
        $user = new User();
        $form = $this->createForm(UserAdminType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setBrand(null);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('godboard_admin');
        }

        return $this->render('godboard/admin/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/edit/{id}", name="edit_willow_admin", methods={"GET","POST"})
     */
    public function editWillowAdmin(Request $request, User $user): Response
    {
        $form = $this->createForm(UserAdminType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('godboard_admin');
        }

        return $this->render('godboard/admin/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/delete/{id}", name="delete_willow_admin", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function deleteWillowAdmin(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('godboard_admin');
    }

    /**
     * @Route("/brand", name="godboard_brand_index", methods={"GET"})
     */
    public function brand(BrandRepository $brandRepository): Response
    {
        return $this->render('godboard/brand/index.html.twig', [
            'brands' => $brandRepository->findAll(),
            'userNumber' => $this->brandRepositoryFormater->changeKeyById($brandRepository->findUsersByBrand()),
            'propertyNumber' => $this->brandRepositoryFormater->changeKeyById($brandRepository->findPropertiesByBrand())
        ]);
    }

    /**
     * @Route("/brand/new", name="godboard_brand_new", methods={"GET","POST"})
     */
    public function newBrand(Request $request): Response
    {
        $brand = new Brand();
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $status = $this->getDoctrine()->getRepository(Status::class)->findAllNewBrandStatus();
            foreach ($status as $item) {
                $newStatus = new Status();
                $newStatus->setDefaultSelection($item->getDefaultSelection());
                $newStatus->setLabel($item->getLabel());
                $brand->addStatus($newStatus);
            }
            $this->encryptDecryptGenerator->encryptBrandTokens($brand);
            $entityManager->persist($brand);
            $entityManager->flush();

            return $this->redirectToRoute('godboard_brand_edit', [
                'id' => $brand->getId()
            ]);
        }

        return $this->render('godboard/brand/new.html.twig', [
            'brand' => $brand,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/brand/{id}/edit", name="godboard_brand_edit", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function editBrand(
        Request $request,
        Brand $brand,
        UserRepository $userRepository,
        EncryptDecryptGenerator $encryptDecryptGenerator
    ): Response {
        $this->encryptDecryptGenerator->decryptBrandTokens($brand);
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);
        $admins = $userRepository->findByBrandAndRole($brand, ['ROLE_ADMIN_BRAND']);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->encryptDecryptGenerator->encryptBrandTokens($brand);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('godboard_brand_edit', [
                'id' => $brand->getId()
            ]);
        }

        return $this->render('godboard/brand/edit.html.twig', [
            'admins' => $admins,
            'brand' => $brand,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/brand/switch/{id}", name="godboard_brand_switch", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function switchBrand(Request $request, Brand $brand): Response
    {
        $this->container->get('session')->set('brand', $brand);
        $this->container->get('session')->set('property_id', null);
        $this->container->get('session')->set('property_label', null);

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/brand/reset", name="godboard_brand_reset", methods={"GET","POST"})
     */
    public function resetBrand(Request $request): Response
    {
        $this->container->get('session')->set('brand', null);
        $this->container->get('session')->set('property_id', null);
        $this->container->get('session')->set('property_label', null);

        return $this->redirectToRoute('godboard_brand_index');
    }

    /**
     * @Route("/brand/{id}/property/new", name="godboard_property_new"
     * , methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function newProperty(Request $request, Brand $brand): Response
    {
        $property = new Property();
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $property->setBrand($brand);
            $this->encryptDecryptGenerator->encryptPropertyToken($property);
            $entityManager->persist($property);
            $entityManager->flush();

            return $this->redirectToRoute('godboard_brand_edit', [
                'id' => $brand->getId()
            ]);
        }

        return $this->render('godboard/property/new.html.twig', [
            'brand' => $brand,
            'property' => $property,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/property/{id}/edit", name="godboard_property_edit", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function editProperty(Request $request, Property $property): Response
    {
        $this->encryptDecryptGenerator->decryptPropertyToken($property);
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->encryptDecryptGenerator->encryptPropertyToken($property);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('godboard_brand_edit', [
                'id' => $property->getBrand()->getId()
            ]);
        }

        return $this->render('godboard/property/edit.html.twig', [
            'property' => $property,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin", name="godboard_admin")
     */
    public function admin(): Response
    {
        $users = $this->getDoctrine()
            ->getRepository('App:User')
            ->findAllByRole(['ROLE_ADMIN_WILLOW', 'ROLE_SUPER_ADMIN']);

        return $this->render('godboard/admin/index.html.twig', [
            'users' => $users,
        ]);
    }
}
