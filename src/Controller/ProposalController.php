<?php

namespace App\Controller;

use App\Builder\ProposalBuilder;
use App\Builder\ProposalTemplateBuilder;
use App\Entity\Enquiry;
use App\Helper\ProposalHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProposalController extends AbstractController
{
    private ProposalBuilder $proposalBuilder;
    private ProposalHelper $proposalHelper;
    private ProposalTemplateBuilder $proposalTemplateBuilder;

    public function __construct(
        ProposalBuilder $proposalBuilder,
        ProposalHelper $proposalHelper,
        ProposalTemplateBuilder $proposalTemplateBuilder
    )
    {
        $this->proposalBuilder = $proposalBuilder;
        $this->proposalHelper = $proposalHelper;
        $this->proposalTemplateBuilder = $proposalTemplateBuilder;
    }

    /**
     * @Route("/proposal/{key}", name="proposal", methods={"GET"}, requirements={"key"="\w+"})
     */
    public function index($key): Response
    {
        $enquiry = $this->getDoctrine()->getRepository(Enquiry::class)->findByProposalKey($key);
        if (!$enquiry) {
            throw new \Exception("Not found", 404);
        }
        $enquiry = $this->proposalHelper->hydrateDefaultConditionAndTemplate($enquiry);
        $proposal = $this->proposalBuilder->create($enquiry);
        $body = $this->proposalTemplateBuilder->create($proposal);

        return new Response($body);
    }
}
