<?php

namespace App\Controller;

use App\Entity\Age;
use App\Form\AgeType;
use App\Service\BrandIfAdminWillow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/age")
 */
class AgeController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/new", name="age_new", methods={"GET","POST"})
     */
    public function new(Request $request, BrandIfAdminWillow $ifAdminWillow): Response
    {
        $age = new Age();
        $form = $this->createForm(AgeType::class, $age);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $session = $this->container->get('session');
            $roleAdminWillow = $this->isGranted('ROLE_ADMIN_WILLOW');
            $brand = $ifAdminWillow->getBrandViaRepoDependingOnUser($session, $roleAdminWillow);
            $age->setBrand($brand);
            $entityManager->persist($age);
            $entityManager->flush();

            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('age/new.html.twig', [
            'age' => $age,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="age_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Age $age): Response
    {
        $form = $this->createForm(AgeType::class, $age);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('age/edit.html.twig', [
            'age' => $age,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("{id}", name="age_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Age $age): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $brand = $age->getBrand();
        if ($this->isCsrfTokenValid('delete' . $age->getId(), $request->request->get('_token'))) {
            $ageIsUsed = $entityManager->getRepository('App:Enquiry')->findAgeByBrand($brand, $age);
            $bAdd = $entityManager->getRepository('App:BreakdownRuleAdd')->findAgeByBrand($brand, $age);
            $bRemain = $entityManager->getRepository('App:BreakdownRuleRemaining')->findAgeByBrand($brand, $age);
            $bTotal = $entityManager->getRepository('App:BreakdownRuleTotal')->findAgeByBrand($brand, $age);
            $filter = $entityManager->getRepository('App:Filter')->findAgeByBrand($brand, $age);
            $extra = $entityManager->getRepository('App:ExtraRule')->findAgeByBrand($brand, $age);
            $rate = $entityManager->getRepository('App:RateRule')->findAgeByBrand($brand, $age);
            if ($ageIsUsed || $bAdd || $bRemain || $bTotal || $filter || $extra || $rate) {
                $this->addFlash('error', 'This age range is used in an enquiry or a rule, you cannot delete it.');
            } else {
                $entityManager->remove($age);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('brand_preferences');
    }
}
