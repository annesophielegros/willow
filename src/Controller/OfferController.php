<?php

namespace App\Controller;

use App\Builder\OccupancyBuilder;
use App\Entity\Enquiry;
use App\Entity\History;
use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use App\Service\EnquiryDatesUpdate;
use App\Service\HistoryManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/offer")
 */
class OfferController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    /**
     * @Route("/", name="offer_index", methods={"GET"})
     */
    public function index(OfferRepository $offerRepository): Response
    {
        return $this->render('offer/proposal_offer.html.twig', [
            'offers' => $offerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/new", name="offer_new", methods={"GET","POST"})
     */
    public function new(
        Request $request,
        Enquiry $enquiry,
        EnquiryDatesUpdate $enquiryDatesUpdate,
        HistoryManager $historyManager,
        OccupancyBuilder $occupancyBuilder
    ): Response {
        $entityManager = $this->getDoctrine()->getManager();
        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($offer->getRooms() as $room) {
                $prices = [];
                foreach ($room->getRates() as $r) {
                    $prices[] = $r->getPrice();
                }
                $sum = array_sum($prices);
                $days = count($room->getRates());
                $average = round($sum / $days, 2);
                $room->setAverageRate($average);
                $this->getDoctrine()->getManager()->persist($room);
                $newOfferUpdate = $historyManager->newOfferRoomUpdate($room);
                $room->addOfferRoomUpdate($newOfferUpdate);
            }
            foreach ($offer->getOfferExtras() as $extra) {
                $this->getDoctrine()->getManager()->persist($extra);
                $newExtraUpdate = $historyManager->newOfferExtraUpdate($extra);
                $extra->addOfferExtraUpdate($newExtraUpdate);
            }
            $offer->setOccupancy($occupancyBuilder->findOccupancy($enquiry));
            $offer->setEnquiry($enquiry);
            $today = new \DateTime();
            $offer->setOfferDate($today);
            $entityManager->persist($offer);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-enquiry', 'offer_rooms');
        }
        if (!$offer->getRooms()->isEmpty()) {
            $enquiryDatesUpdate->datesSetter($offer);
        }
        return $this->redirectToRoute('enquiry_show', ['id' => $offer->getEnquiry()->getId()]);
    }

    /**
     * @Route("/{id}/edit", name="offer_edit", methods={"GET","POST"})
     */
    public function edit(
        Request $request,
        Offer $offer,
        HistoryManager $historyManager,
        EnquiryDatesUpdate $enquiryDatesUpdate,
        OccupancyBuilder $occupancyBuilder
    ): Response {
        $user = $this->security->getUser();
        if (null === $task = $this->getDoctrine()->getRepository(Offer::class)->find($offer->getId())) {
            throw $this->createNotFoundException('No task found for id ' . $offer->getId());
        }
        $originalRooms = new ArrayCollection();
        $originalRates = [];
        foreach ($offer->getRooms() as $key => $room) {
            $originalRooms->add($room);
            foreach ($room->getRates() as $rate) {
                $originalRates[$key][] = $rate;
            }
        }
        $originalExtras = new ArrayCollection();
        foreach ($offer->getOfferExtras() as $room) {
            $originalExtras->add($room);
        }
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('session')->set('active-tab-enquiry', 'offer_rooms');
            foreach ($originalRooms as $key => $room) {
                if (false === $offer->getRooms()->contains($room)) {
                    $newHistory = new History();
                    $newHistory->setUser($user);
                    $newHistory->setEnquiry($offer->getEnquiry());
                    $newHistory->setOldValue(
                        "Room quantity: " . (strval($room->getRoomQuantity())) .
                        "<br/>Check In: " . $room->getCheckInDate()->format('d-m-Y') .
                        "<br/>Check Out: " . $room->getCheckOutDate()->format('d-m-Y') .
                        "<br/>Persons: " . (strval($room->getPersonsInTheRoom())) .
                        "<br/>Average: " . (strval($room->getAverageRate()))
                    );
                    $newHistory->setNewValue("Removed");
                    $newHistory->setFieldEdit($room->getRoomType()->getLabel());
                    $this->getDoctrine()->getManager()->persist($newHistory);
                    $room->setOffer(null);
                    $this->getDoctrine()->getManager()->remove($room);
                }
                if (isset($originalRates[$key])) {
                    foreach ($originalRates[$key] as $rate) {
                        if (!isset($offer->getRooms()[$key])) {
                            continue;
                        }
                        if (false === $offer->getRooms()[$key]->getRates()->contains($rate)) {
                            $this->getDoctrine()->getManager()->remove($rate);
                        }
                    }
                }
            }
            foreach ($originalExtras as $extra) {
                if (false === $offer->getOfferExtras()->contains($extra)) {
                    $newHistory = new History();
                    $newHistory->setUser($user);
                    $newHistory->setEnquiry($offer->getEnquiry());
                    $newHistory->setOldValue(
                        "Extra quantity: " . (strval($extra->getQuantity())) .
                        "<br/>Start Date: " . $extra->getStartDate()->format('d-m-Y') .
                        "<br/>End Date: " . $extra->getEndDate()->format('d-m-Y') .
                        "<br/>Price: " . (strval($extra->getPrice()))
                    );
                    $newHistory->setNewValue("Removed");
                    $newHistory->setFieldEdit($extra->getExtra()->getLabel());
                    $this->getDoctrine()->getManager()->persist($newHistory);
                    $extra->setOffer(null);
                    $this->getDoctrine()->getManager()->remove($extra);
                }
            }
            foreach ($offer->getRooms() as $room) {
                $prices = [];
                foreach ($room->getRates() as $r) {
                    $prices[] = $r->getPrice();
                }
                $sum = array_sum($prices);
                $days = count($room->getRates());
                $average = round($sum / $days, 2);
                $room->setAverageRate($average);
            }
            $this->getDoctrine()->getManager()->flush();
            $historyManager->offerHistorySetter($offer);
        }
        $offer->setOccupancy($occupancyBuilder->findOccupancy($offer->getEnquiry()));
        if (!$offer->getRooms()->isEmpty()) {
            $enquiryDatesUpdate->datesSetter($offer);
        }
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('enquiry_show', ['id' => $offer->getEnquiry()->getId()]);
    }
}
