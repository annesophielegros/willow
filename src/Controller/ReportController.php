<?php

namespace App\Controller;

use App\Builder\Reporting\EnquiryReportBuilder;
use App\Builder\Reporting\SalesCompanyReportBuilder;
use App\Builder\Reporting\SummaryReportBuilder;
use App\Form\Report\ReportEnquiryType;
use App\Form\Report\SalesByCompanyByUserType;
use App\Service\BrandIfAdminWillow;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    private $enquiryReportBuilder;

    private $salesCompanyReportBuilder;

    private $summaryReportBuilder;

    private $ifAdminWillow;

    public function __construct(
        EnquiryReportBuilder $enquiryReportBuilder,
        SalesCompanyReportBuilder $salesCompanyReportBuilder,
        SummaryReportBuilder $summaryReportBuilder,
        BrandIfAdminWillow $ifAdminWillow
    ) {
        $this->enquiryReportBuilder = $enquiryReportBuilder;
        $this->salesCompanyReportBuilder = $salesCompanyReportBuilder;
        $this->summaryReportBuilder = $summaryReportBuilder;
        $this->ifAdminWillow = $ifAdminWillow;
    }

    /**
     * @Route("/report", name="report")
     */
    public function index(Request $request)
    {
        $session = $this->container->get('session');
        $roleAdminWillow = $this->isGranted('ROLE_ADMIN_WILLOW');
        $brandId = $this->ifAdminWillow->getBrandIdDependingOnUser($session, $roleAdminWillow);
        $enquiryReportForm = $this->createForm(ReportEnquiryType::class);
        $enquiryReport = [];
        $salesByCompanyForm = $this->createForm(SalesByCompanyByUserType::class);
        $salesByCompanyReport = [];
        $confirmedEnquiries = $this->summaryReportBuilder->getConfirmedEnquiriesByBrand($brandId);
        $cancelledEnquiries = $this->summaryReportBuilder->getCancelledEnquiriesByBrand($brandId);
        if ($enquiryReportForm->handleRequest($request)->isSubmitted() && $enquiryReportForm->isValid()) {
            if ($enquiryReportForm->get('generate_report')->isClicked()) {
                $enquiryReport = $this->enquiryReportBuilder->generateReport($enquiryReportForm, $brandId);
            }
            if ($enquiryReportForm->get('export_excel')->isClicked()) {
                $writer = $this->enquiryReportBuilder->extractExcel($enquiryReportForm, $brandId);
                $today = date_format(new DateTime(), 'd-m-Y_h-i');
                $fileName = 'enquiry_report_by_property' . $today . '.xlsx';
                $temp_file = tempnam(sys_get_temp_dir(), $fileName);
                $writer->save($temp_file);
                return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
            }
            $this->container->get('session')->set('active-tab', 'enq_report');
        }
        if ($salesByCompanyForm->handleRequest($request)->isSubmitted() && $salesByCompanyForm->isValid()) {
            if ($salesByCompanyForm->get('generate_report')->isClicked()) {
                $salesByCompanyReport = $this->salesCompanyReportBuilder->generateReport($salesByCompanyForm);
            }
            if ($salesByCompanyForm->get('export_excel')->isClicked()) {
                $writer = $this->salesCompanyReportBuilder->extractExcel($salesByCompanyForm);
                $today = date_format(new DateTime(), 'd-m-Y_h-i');
                $fileName = 'sales_report_by_company' . $today . '.xlsx';
                $temp_file = tempnam(sys_get_temp_dir(), $fileName);
                $writer->save($temp_file);
                return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
            }
            $this->container->get('session')->set('active-tab', 'sales_report');
        }
        return $this->render('report/index.html.twig', [
            'form' => $enquiryReportForm->createView(),
            'report_enquiries' => $enquiryReport,
            'salesByCompanyForm' => $salesByCompanyForm->createView(),
            'salesByCompanyReport' => $salesByCompanyReport,
            'confirmedEnquiries' => $confirmedEnquiries,
            'cancelledEnquiries' => $cancelledEnquiries,
        ]);
    }
}
