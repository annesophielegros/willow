<?php

namespace App\Service;

use App\Entity\Property;
use App\Entity\TemplateCondition;
use App\Entity\TemplateMail;
use App\Entity\TemplateProposal;
use Doctrine\Persistence\ManagerRegistry;

class DefaultLanguage
{
    private $em;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public function defaultCondition(TemplateCondition $conditions, int $propertyId)
    {
        $defaultConditionsTrue = $conditions->getDefaultSelection();
        $langId = $conditions->getLanguage();
        $conditionsId = $conditions->getId();
        $defCondEx = $this->em->getRepository(TemplateCondition::class)->findDefByPByL($propertyId, $langId);
        if ($defCondEx) {
            if ($defaultConditionsTrue) {
                if ($conditionsId != $defCondEx->getId()) {
                    $defCondEx->setDefaultSelection(false);
                }
            }
        }
    }

    public function defaultMailProposal(TemplateMail $mail, int $propId)
    {
        $defaultMailTrue = $mail->getDefaultSelection();
        $langId = $mail->getLanguage();
        $mailId = $mail->getId();
        $defMailEx = $this->em->getRepository(TemplateMail::class)->findProposalDefByPByL($propId, $langId);
        if ($defMailEx) {
            if ($defaultMailTrue) {
                if ($mailId != $defMailEx->getId()) {
                    $defMailEx->setDefaultSelection(false);
                }
            }
        }
    }

    public function defaultMailPending(TemplateMail $mail, int $propId)
    {
        $defaultMailTrue = $mail->getDefaultSelection();
        $langId = $mail->getLanguage();
        $mailId = $mail->getId();
        $defMailEx = $this->em->getRepository(TemplateMail::class)->findPendingDefByPByL($propId, $langId);
        if ($defMailEx) {
            if ($defaultMailTrue) {
                if ($mailId != $defMailEx->getId()) {
                    $defMailEx->setDefaultSelection(false);
                }
            }
        }
    }

    public function defaultProposal(TemplateProposal $proposal, int $propertyId)
    {
        $defaultProposalTrue = $proposal->getDefaultSelection();
        $langId = $proposal->getLanguage();
        $proposalId = $proposal->getId();
        $defProEx = $this->em->getRepository(TemplateProposal::class)->findDefByPByL($propertyId, $langId);
        if ($defProEx) {
            if ($defaultProposalTrue) {
                if ($proposalId != $defProEx->getId()) {
                    $defProEx->setDefaultSelection(false);
                }
            }
        }
    }

    public function mailProposalWithoutDefault(Property $property): ?array
    {
        $mailDefRepo = $this->em->getRepository('App:TemplateMail')->findLangWithDefaultProposal($property);
        $languages = $property->getLanguageListid();
        $mailDefault = array_column($mailDefRepo, 'id');
        $mailLanguageWithoutDefault = array_diff($languages, $mailDefault);

        return $this->em->getRepository('App:Language')->findBy(['id' => $mailLanguageWithoutDefault]);
    }

    public function mailPendingWithoutDefault(Property $property): ?array
    {
        $mailDefRepo = $this->em->getRepository('App:TemplateMail')->findLangWithDefaultPending($property);
        $languages = $property->getLanguageListid();
        $mailDefault = array_column($mailDefRepo, 'id');
        $mailLanguageWithoutDefault = array_diff($languages, $mailDefault);

        return $this->em->getRepository('App:Language')->findBy(['id' => $mailLanguageWithoutDefault]);
    }

    public function conditionWithoutDefault(Property $property): ?array
    {
        $condDefRepo = $this->em->getRepository('App:TemplateCondition')->findLanguageWithDefault($property);
        $languages = $property->getLanguageListid();
        $condDefault = array_column($condDefRepo, 'id');
        $condLanguageWithoutDefault = array_diff($languages, $condDefault);

        return $this->em->getRepository('App:Language')->findBy(['id' => $condLanguageWithoutDefault]);
    }

    public function proposalWithoutDefault(Property $property): ?array
    {
        $propDefRepo = $this->em->getRepository('App:TemplateProposal')->findLanguageWithDefault($property);
        $languages = $property->getLanguageListid();
        $proposalDefault = array_column($propDefRepo, 'id');
        $proposalLanguageWithoutDefault = array_diff($languages, $proposalDefault);

        return $this->em->getRepository('App:Language')->findBy(['id' => $proposalLanguageWithoutDefault]);
    }
}
