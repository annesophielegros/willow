<?php

namespace App\Service;

use App\Builder\OccupancyBuilder;
use App\Builder\OfferBuilder;
use App\Entity\BreakdownEnquiry;
use App\Entity\Enquiry;
use App\Entity\EnquiryUpdates;
use App\Entity\FollowUp;
use App\Entity\Property;
use App\Entity\TemplateCondition;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class NewEnquirySetUp
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var OfferBuilder
     */
    private $offerBuilder;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OccupancyBuilder
     */
    private $occupancyBuilder;

    public function __construct(
        MailerInterface $mailer,
        OfferBuilder $offerBuilder,
        OccupancyBuilder $occupancyBuilder,
        EntityManagerInterface $entityManager
    ) {
        $this->mailer = $mailer;
        $this->offerBuilder = $offerBuilder;
        $this->occupancyBuilder = $occupancyBuilder;
        $this->entityManager = $entityManager;
    }

    public function createOnline(Enquiry $enquiry)
    {
        $property = $enquiry->getProperty();
        $brandId = $property->getBrand()->getId();
        $defaultStatus = $this->entityManager->getRepository('App:Status')->findDefaultStatusByBrand($brandId);
        $defaultLanguage = $enquiry->getProperty()->getDefaultLanguage();
        $enquiry->setLanguage($defaultLanguage);
        $defaultProposal = $this->entityManager->getRepository('App:TemplateProposal')->findDefByPByL(
            $enquiry->getProperty()->getId(),
            $enquiry->getLanguage()->getId(),
        );
        if ($defaultProposal) {
            $enquiry->setTemplateProposal($defaultProposal);
        }
        $defCondEx = $this->entityManager->getRepository('App:TemplateCondition')->findDefByPByL(
            $enquiry->getProperty()->getId(),
            $enquiry->getLanguage()->getId(),
        );
        if ($defCondEx) {
            $enquiry->setTemplateCondition($defCondEx);
        }
        foreach ($property->getBreakdownTypes() as $breakdownProperty) {
            $breakdownEnquiry = new BreakdownEnquiry();
            $breakdownEnquiry->setEnquiry($enquiry);
            $breakdownEnquiry->setBreakdownPref($breakdownProperty);
            $this->entityManager->persist($breakdownEnquiry);
        }
        $rateDisplay = $enquiry->getProperty()->getRateDisplay();
        $enquiry->setRateDisplay($rateDisplay);
        $emptyBeds = $property->getEmptyBedsCharged();
        $enquiry->setEmptyBedsCharged($emptyBeds);
        $enquiry->setStatus($defaultStatus);
        $this->createFollowAndUpdate($enquiry, $property, $defaultStatus);
        $this->entityManager->flush();
        $this->sendMail($enquiry);

        return $enquiry->getReservationNumber();
    }

    public function sendMail($enquiry)
    {
        $occupancy = $this->occupancyBuilder->findOccupancy($enquiry);
        $filters = $this->entityManager->getRepository('App:Filter')->findFilters($enquiry, $occupancy);
        if (empty($filters)) {
            $defaultMailPending = $this->entityManager->getRepository('App:TemplateMail')->findPendingDefByPByL(
                $enquiry->getProperty(),
                $enquiry->getLanguage()
            );
            $this->entityManager->flush();
            $email = new TemplatedEmail();
            $email->from('contact@willow-solution.com');
            $email->to($enquiry->getEmailAddress());
            $email->cc($enquiry->getProperty()->getPreferenceEmail()->getReceiverEnquiryFiltered());
            $email->subject($defaultMailPending->getSubject());
            $email->htmlTemplate('mail_template/pending_email.html.twig');
            $email->context([
                'enquiry' => $enquiry,
                'body1' => $defaultMailPending->getBody(),
                'body2' => $defaultMailPending->getBodySecond(),
            ]);

            $this->mailer->send($email);
        } else {
            $defaultMailProposal = $this->entityManager->getRepository('App:TemplateMail')->findProposalDefByPByL(
                $enquiry->getProperty(),
                $enquiry->getLanguage()
            );
            $this->offerBuilder->build($enquiry);
            $this->offerBuilder->save();
            $this->entityManager->flush();
            $email = new TemplatedEmail();
            $email->from($enquiry->getProperty()->getPreferenceEmail()->getSenderEnquiry());
            $email->to($enquiry->getEmailAddress());
            $email->subject($defaultMailProposal->getSubject());
            $email->htmlTemplate('mail_template/proposal_email.html.twig');
            $email->context([
                'body1' => $defaultMailProposal->getBody(),
                'body2' => $defaultMailProposal->getBodySecond(),
                'key' => $enquiry->getProposalKey(),
            ]);

            $this->mailer->send($email);
        }
    }

    public function createManualBeforeForm(Enquiry $enquiry, Property $property, $defaultStatus)
    {
        $defaultLanguage = $property->getDefaultLanguage();
        $enquiry->setLanguage($defaultLanguage);
        $rateDisplay = $property->getRateDisplay();
        $enquiry->setRateDisplay($rateDisplay);
        $emptyBeds = $property->getEmptyBedsCharged();
        $enquiry->setEmptyBedsCharged($emptyBeds);
        $enquiry->setStatus($defaultStatus);
        foreach ($property->getBreakdownTypes() as $breakdownProperty) {
            $breakdownEnquiry = new BreakdownEnquiry();
            $breakdownEnquiry->setEnquiry($enquiry);
            $breakdownEnquiry->setBreakdownPref($breakdownProperty);
            $this->entityManager->persist($breakdownEnquiry);
        }
    }

    public function createManualAfterForm(Enquiry $enquiry, $property, $defaultStatus, $user)
    {
        $propertyId = $property->getId();
        $enquiry->setProperty($property);
        $enquiry->setCreatedBy($user);
        $enquiry->setWishes('Enquiry created manually.');
        $langId = $enquiry->getLanguage();
        $defCondEx = $this->entityManager->getRepository(TemplateCondition::class)->findDefByPByL($propertyId, $langId);
        if ($defCondEx) {
            $enquiry->setTemplateCondition($defCondEx);
        }
        $defProposal = $this->entityManager->getRepository('App:TemplateProposal')->findDefByPByL($propertyId, $langId);
        if ($defProposal) {
            $enquiry->setTemplateProposal($defProposal);
        }
        $this->createFollowAndUpdate($enquiry, $property, $defaultStatus);
        $this->entityManager->flush();
    }

    public function createFollowAndUpdate($enquiry, Property $property, $defaultStatus) {
        $propertyId = $property->getId();
        $followUps = $this->entityManager->getRepository('App:FollowUpPreference')->findOrderByProperty($propertyId);
        foreach ($followUps as $followUp) {
            $newFollowUp = new FollowUp();
            $newFollowUp->setEnquiry($enquiry);
            $newFollowUp->setNumber($followUp->getClassification());
            $newFollowUp->setComment($followUp->getComment());
            $days = $followUp->getDays();
            $date = new \DateTime();
            $date->modify("+ $days day");
            $newFollowUp->setProvideDate($date);
            $this->entityManager->persist($newFollowUp);
        }
        $this->entityManager->persist($enquiry);
        $newEnquiryUpdate = new EnquiryUpdates();
        $newEnquiryUpdate->setEnquiry($enquiry);
        $newEnquiryUpdate->setStatus($defaultStatus);
        $newEnquiryUpdate->setCheckInDate($enquiry->getCheckInDate());
        $newEnquiryUpdate->setCheckOutDate($enquiry->getCheckOutDate());
        $newEnquiryUpdate->setNumberOfPersons($enquiry->getNumberOfPersons());
        $newEnquiryUpdate->setUpdateDateTime(new \DateTime());
        $this->entityManager->persist($newEnquiryUpdate);
    }
}
