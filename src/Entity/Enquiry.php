<?php

namespace App\Entity;

use App\Generator\EnquiryProposalKeyGenerator;
use App\Repository\EnquiryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EnquiryRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Enquiry
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    private $currentPlace;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $group_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email_address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $check_in_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $check_out_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $enquiry_date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $wishes;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfPersons;

    /**
     * @var string
     */
    private $code;

    /**
     * @ORM\OneToOne(targetEntity=Offer::class, inversedBy="enquiry", cascade={"persist", "remove"})
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="enquiries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;

    /**
     * @ORM\OneToMany(targetEntity=History::class, mappedBy="enquiry")
     */
    private $histories;

    /**
     * @ORM\Column(type="integer")
     */
    private $reservationNumber;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="enquiries")
     * @ORM\JoinColumn(nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $client_comment;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hotel_comment;

    /**
     * @ORM\OneToMany(targetEntity=FollowUp::class, mappedBy="enquiry", cascade="persist")
     */
    private $followUps;

    /**
     * @ORM\OneToMany(targetEntity=EnquiryUpdates::class, mappedBy="enquiry", cascade="persist")
     */
    private $enquiryUpdates;


    /**
     * @ORM\ManyToOne(targetEntity=Age::class, inversedBy="enquiry")
     */
    private $age;

    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="enquiries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=RateType::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $ratetype;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class)
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity=TemplateCondition::class)
     */
    private $templateCondition;

    /**
     * @ORM\ManyToOne(targetEntity=TemplateProposal::class)
     */
    private $templateProposal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $customConditions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $proposalKey;

    /**
     * @ORM\ManyToOne(targetEntity=CancelReason::class)
     */
    private $cancelReason;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity=RateDisplay::class)
     */
    private $rateDisplay;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $providerId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ageCode;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $emptyBedsCharged;

    /**
     * @ORM\OneToMany(targetEntity=BreakdownEnquiry::class, mappedBy="enquiry", orphanRemoval=true)
     */
    private $breakdownEnquiries;

    public function __construct()
    {
        $this->wishes = new ArrayCollection();
        $this->histories = new ArrayCollection();
        $this->followUps = new ArrayCollection();
        $this->check_in_date = new \DateTime();
        $this->active = true;
        $this->enquiryUpdates = new ArrayCollection();
        $this->breakdownEnquiries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrentPlace()
    {
        return $this->currentPlace;
    }

    public function setCurrentPlace($currentPlace, $context = [])
    {
        $this->currentPlace = $currentPlace;
    }

    public function getGroupName(): ?string
    {
        return $this->group_name;
    }

    public function setGroupName(string $group_name): self
    {
        $this->group_name = $group_name;

        return $this;
    }

    public function getContactFirstName(): ?string
    {
        return $this->contact_first_name;
    }

    public function setContactFirstName(?string $contact_first_name): self
    {
        $this->contact_first_name = $contact_first_name;

        return $this;
    }

    public function getContactLastName(): ?string
    {
        return $this->contact_last_name;
    }

    public function setContactLastName(?string $contact_last_name): self
    {
        $this->contact_last_name = $contact_last_name;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function setEmailAddress(string $email_address): self
    {
        $this->email_address = $email_address;

        return $this;
    }

    public function getCheckInDate(): ?\DateTimeInterface
    {
        return $this->check_in_date;
    }

    public function setCheckInDate(\DateTimeInterface $check_in_date): self
    {
        $this->check_in_date = $check_in_date;

        return $this;
    }

    public function getCheckOutDate(): ?\DateTimeInterface
    {
        return $this->check_out_date;
    }

    public function setCheckOutDate(\DateTimeInterface $check_out_date): self
    {
        $this->check_out_date = $check_out_date;

        return $this;
    }

    public function getEnquiryDate(): ?\DateTimeInterface
    {
        return $this->enquiry_date;
    }

    public function setEnquiryDate(\DateTimeInterface $enquiry_date): self
    {
        $this->enquiry_date = $enquiry_date;

        return $this;
    }

    public function getWishes(): ?string
    {
        return $this->wishes;
    }

    public function setWishes(?string $wishes): self
    {
        $this->wishes = $wishes;

        return $this;
    }

    public function getNumberOfPersons(): ?int
    {
        return $this->numberOfPersons;
    }

    public function setNumberOfPersons(int $numberOfPersons): self
    {
        $this->numberOfPersons = $numberOfPersons;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->enquiry_date = new \DateTime();
        $this->proposalKey = (new EnquiryProposalKeyGenerator())->create();
    }

    /**
     * @return Collection|History[]
     */
    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setEnquiry($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->removeElement($history)) {
            // set the owning side to null (unless already changed)
            if ($history->getEnquiry() === $this) {
                $history->setEnquiry(null);
            }
        }

        return $this;
    }

    public function getReservationNumber(): ?int
    {
        return $this->reservationNumber;
    }

    public function setReservationNumber(int $reservationNumber): self
    {
        $this->reservationNumber = $reservationNumber;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getClientComment(): ?string
    {
        return $this->client_comment;
    }

    public function setClientComment(?string $client_comment): self
    {
        $this->client_comment = $client_comment;

        return $this;
    }

    public function getHotelComment(): ?string
    {
        return $this->hotel_comment;
    }

    public function setHotelComment(?string $hotel_comment): self
    {
        $this->hotel_comment = $hotel_comment;

        return $this;
    }

    /**
     * @return Collection|FollowUp[]
     */
    public function getFollowUps(): Collection
    {
        return $this->followUps;
    }

    public function addFollowUp(FollowUp $followUp): self
    {
        if (!$this->followUps->contains($followUp)) {
            $this->followUps[] = $followUp;
            $followUp->setEnquiry($this);
        }

        return $this;
    }

    public function removeFollowUp(FollowUp $followUp): self
    {
        if ($this->followUps->removeElement($followUp)) {
            // set the owning side to null (unless already changed)
            if ($followUp->getEnquiry() === $this) {
                $followUp->setEnquiry(null);
            }
        }

        return $this;
    }

    public function getAge(): ?Age
    {
        return $this->age;
    }

    public function setAge(?Age $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRatetype(): ?RateType
    {
        return $this->ratetype;
    }

    public function setRatetype(?RateType $ratetype): self
    {
        $this->ratetype = $ratetype;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTemplateCondition(): ?TemplateCondition
    {
        return $this->templateCondition;
    }

    public function setTemplateCondition(?TemplateCondition $templateCondition): self
    {
        $this->templateCondition = $templateCondition;

        return $this;
    }

    public function getTemplateProposal(): ?TemplateProposal
    {
        return $this->templateProposal;
    }

    public function setTemplateProposal(?TemplateProposal $templateProposal): self
    {
        $this->templateProposal = $templateProposal;

        return $this;
    }

    public function getCustomConditions(): ?string
    {
        return $this->customConditions;
    }

    public function setCustomConditions(?string $customConditions): self
    {
        $this->customConditions = $customConditions;

        return $this;
    }

    public function getProposalKey(): ?string
    {
        return $this->proposalKey;
    }

    public function setProposalKey(string $proposalKey): self
    {
        $this->proposalKey = $proposalKey;

        return $this;
    }

    public function getCancelReason(): ?CancelReason
    {
        return $this->cancelReason;
    }

    public function setCancelReason(?CancelReason $cancelReason): self
    {
        $this->cancelReason = $cancelReason;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getRateDisplay(): ?RateDisplay
    {
        return $this->rateDisplay;
    }

    public function setRateDisplay(?RateDisplay $rateDisplay): self
    {
        $this->rateDisplay = $rateDisplay;

        return $this;
    }

    /**
     * @return Collection<int, EnquiryUpdates>
     */
    public function getEnquiryUpdates(): Collection
    {
        return $this->enquiryUpdates;
    }

    public function addEnquiryUpdate(EnquiryUpdates $enquiryUpdate): self
    {
        if (!$this->enquiryUpdates->contains($enquiryUpdate)) {
            $this->enquiryUpdates[] = $enquiryUpdate;
            $enquiryUpdate->setEnquiry($this);
        }

        return $this;
    }

    public function removeEnquiryUpdate(EnquiryUpdates $enquiryUpdate): self
    {
        if ($this->enquiryUpdates->removeElement($enquiryUpdate)) {
            // set the owning side to null (unless already changed)
            if ($enquiryUpdate->getEnquiry() === $this) {
                $enquiryUpdate->setEnquiry(null);
            }
        }

        return $this;
    }

    public function getProviderId(): ?string
    {
        return $this->providerId;
    }

    public function setProviderId(?string $providerId): self
    {
        $this->providerId = $providerId;

        return $this;
    }

    public function getAgeCode(): ?string
    {
        return $this->ageCode;
    }

    public function setAgeCode(?string $ageCode): self
    {
        $this->ageCode = $ageCode;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getEmptyBedsCharged(): ?bool
    {
        return $this->emptyBedsCharged;
    }

    public function setEmptyBedsCharged(bool $emptyBedsCharged): self
    {
        $this->emptyBedsCharged = $emptyBedsCharged;

        return $this;
    }

    /**
     * @return Collection<int, BreakdownEnquiry>
     */
    public function getBreakdownEnquiries(): Collection
    {
        return $this->breakdownEnquiries;
    }

    public function addBreakdownEnquiry(BreakdownEnquiry $breakdownEnquiry): self
    {
        if (!$this->breakdownEnquiries->contains($breakdownEnquiry)) {
            $this->breakdownEnquiries[] = $breakdownEnquiry;
            $breakdownEnquiry->setEnquiry($this);
        }

        return $this;
    }

    public function removeBreakdownEnquiry(BreakdownEnquiry $breakdownEnquiry): self
    {
        if ($this->breakdownEnquiries->removeElement($breakdownEnquiry)) {
            // set the owning side to null (unless already changed)
            if ($breakdownEnquiry->getEnquiry() === $this) {
                $breakdownEnquiry->setEnquiry(null);
            }
        }

        return $this;
    }
}
