<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Offer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $offer_date;

    /**
     * @ORM\OneToMany(targetEntity=OfferRoom::class, mappedBy="offer", cascade={"persist"})
     */
    private $rooms;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $errors = [];

    /**
     * @ORM\OneToOne(targetEntity=Enquiry::class, mappedBy="offer", cascade={"persist", "remove"})
     */
    private $enquiry;

    /**
     * @ORM\OneToMany(targetEntity=OfferExtra::class, mappedBy="offer", cascade={"persist", "remove"})
     */
    private $offerExtras;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $occupancy;

    public function __construct()
    {
        $this->rooms = new ArrayCollection();
        $this->offerExtras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOfferDate(): ?\DateTimeInterface
    {
        return $this->offer_date;
    }

    public function setOfferDate(\DateTimeInterface $offer_date): self
    {
        $this->offer_date = $offer_date;

        return $this;
    }

    /**
     * @return Collection|OfferRoom[]
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(OfferRoom $room): self
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms[] = $room;
            $room->setOffer($this);
        }

        return $this;
    }

    public function removeRoom(OfferRoom $room): self
    {
        if ($this->rooms->removeElement($room)) {
            // set the owning side to null (unless already changed)
            if ($room->getOffer() === $this) {
                $room->setOffer(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->offer_date = new \DateTime();
    }

    public function getErrors(): ?array
    {
        return $this->errors;
    }

    public function setErrors(?array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function getEnquiry(): ?Enquiry
    {
        return $this->enquiry;
    }

    public function setEnquiry(?Enquiry $enquiry): self
    {
        $this->enquiry = $enquiry;

        // set (or unset) the owning side of the relation if necessary
        $newOffer = null === $enquiry ? null : $this;
        if ($enquiry->getOffer() !== $newOffer) {
            $enquiry->setOffer($newOffer);
        }

        return $this;
    }

    /**
     * @return Collection|OfferExtra[]
     */
    public function getOfferExtras(): Collection
    {
        return $this->offerExtras;
    }

    public function addOfferExtra(OfferExtra $offerExtra): self
    {
        if (!$this->offerExtras->contains($offerExtra)) {
            $this->offerExtras[] = $offerExtra;
            $offerExtra->setOffer($this);
        }

        return $this;
    }

    public function removeOfferExtra(OfferExtra $offerExtra): self
    {
        if ($this->offerExtras->removeElement($offerExtra)) {
            // set the owning side to null (unless already changed)
            if ($offerExtra->getOffer() === $this) {
                $offerExtra->setOffer(null);
            }
        }

        return $this;
    }

    public function getOccupancy(): ?float
    {
        return $this->occupancy;
    }

    public function setOccupancy(?float $occupancy): self
    {
        $this->occupancy = $occupancy;

        return $this;
    }
}
