<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatusRepository::class)
 */
class Status
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="statuses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity=Enquiry::class, mappedBy="status")
     */
    private $enquiries;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $default_selection;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $new_brand;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $cancel_type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $optionalStatus;

    /**
     * @ORM\Column(type="boolean")
     */
    private $confirmedStatus;

    public function __construct()
    {
        $this->enquiries = new ArrayCollection();
        $this->optionalStatus = false;
        $this->confirmedStatus = false;
        $this->default_selection = false;
        $this->cancel_type = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection|Enquiry[]
     */
    public function getEnquiries(): Collection
    {
        return $this->enquiries;
    }

    public function addEnquiry(Enquiry $enquiry): self
    {
        if (!$this->enquiries->contains($enquiry)) {
            $this->enquiries[] = $enquiry;
            $enquiry->setStatus($this);
        }

        return $this;
    }

    public function removeEnquiry(Enquiry $enquiry): self
    {
        if ($this->enquiries->removeElement($enquiry)) {
            // set the owning side to null (unless already changed)
            if ($enquiry->getStatus() === $this) {
                $enquiry->setStatus(null);
            }
        }

        return $this;
    }

    public function getProviderId(): ?string
    {
        return $this->provider_id;
    }

    public function setProviderId(?string $provider_id): self
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    public function getDefaultSelection(): ?bool
    {
        return $this->default_selection;
    }

    public function setDefaultSelection(bool $default_selection): self
    {
        $this->default_selection = $default_selection;

        return $this;
    }

    public function getNewBrand(): ?bool
    {
        return $this->new_brand;
    }

    public function setNewBrand(?bool $new_brand): self
    {
        $this->new_brand = $new_brand;

        return $this;
    }

    public function getCancelType(): ?bool
    {
        return $this->cancel_type;
    }

    public function setCancelType(?bool $cancel_type): self
    {
        $this->cancel_type = $cancel_type;

        return $this;
    }

    public function getOptionalStatus(): ?bool
    {
        return $this->optionalStatus;
    }

    public function setOptionalStatus(bool $optionalStatus): self
    {
        $this->optionalStatus = $optionalStatus;

        return $this;
    }

    public function getConfirmedStatus(): ?bool
    {
        return $this->confirmedStatus;
    }

    public function setConfirmedStatus(bool $confirmedStatus): self
    {
        $this->confirmedStatus = $confirmedStatus;

        return $this;
    }
}
