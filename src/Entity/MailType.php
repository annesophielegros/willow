<?php

namespace App\Entity;

use App\Repository\MailTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MailTypeRepository::class)
 */
class MailType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=TemplateMail::class, mappedBy="type")
     */
    private $templateMails;

    public function __construct()
    {
        $this->templateMails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, TemplateMail>
     */
    public function getTemplateMails(): Collection
    {
        return $this->templateMails;
    }

    public function addTemplateMail(TemplateMail $templateMail): self
    {
        if (!$this->templateMails->contains($templateMail)) {
            $this->templateMails[] = $templateMail;
            $templateMail->setType($this);
        }

        return $this;
    }

    public function removeTemplateMail(TemplateMail $templateMail): self
    {
        if ($this->templateMails->removeElement($templateMail)) {
            // set the owning side to null (unless already changed)
            if ($templateMail->getType() === $this) {
                $templateMail->setType(null);
            }
        }

        return $this;
    }
}
