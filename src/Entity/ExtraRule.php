<?php

namespace App\Entity;

use App\Repository\ExtraRuleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtraRuleRepository::class)
 */
class ExtraRule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pax_min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pax_max;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $length_min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $length_max;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_end;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $occupancy_min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $occupancy_max;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="extraRules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;

    /**
     * @ORM\ManyToOne(targetEntity=Extra::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $extra;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $consumed;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $division;

    /**
     * @ORM\ManyToOne(targetEntity=Age::class)
     */
    private $age_min;

    /**
     * @ORM\ManyToOne(targetEntity=Age::class)
     */
    private $age_max;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $daySelection = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPaxMin(): ?int
    {
        return $this->pax_min;
    }

    public function setPaxMin(?int $pax_min): self
    {
        $this->pax_min = $pax_min;

        return $this;
    }

    public function getPaxMax(): ?int
    {
        return $this->pax_max;
    }

    public function setPaxMax(?int $pax_max): self
    {
        $this->pax_max = $pax_max;

        return $this;
    }

    public function getLengthMin(): ?int
    {
        return $this->length_min;
    }

    public function setLengthMin(?int $length_min): self
    {
        $this->length_min = $length_min;

        return $this;
    }

    public function getLengthMax(): ?int
    {
        return $this->length_max;
    }

    public function setLengthMax(?int $length_max): self
    {
        $this->length_max = $length_max;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->date_start;
    }

    public function setDateStart(?\DateTimeInterface $date_start): self
    {
        $this->date_start = $date_start;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->date_end;
    }

    public function setDateEnd(?\DateTimeInterface $date_end): self
    {
        $this->date_end = $date_end;

        return $this;
    }

    public function getOccupancyMin(): ?int
    {
        return $this->occupancy_min;
    }

    public function setOccupancyMin(?int $occupancy_min): self
    {
        $this->occupancy_min = $occupancy_min;

        return $this;
    }

    public function getOccupancyMax(): ?int
    {
        return $this->occupancy_max;
    }

    public function setOccupancyMax(?int $occupancy_max): self
    {
        $this->occupancy_max = $occupancy_max;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getExtra(): ?Extra
    {
        return $this->extra;
    }

    public function setExtra(?Extra $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getConsumed(): ?string
    {
        return $this->consumed;
    }

    public function setConsumed(string $consumed): self
    {
        $this->consumed = $consumed;

        return $this;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    public function setDivision(string $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getAgeMin(): ?Age
    {
        return $this->age_min;
    }

    public function setAgeMin(?Age $age_min): self
    {
        $this->age_min = $age_min;

        return $this;
    }

    public function getAgeMax(): ?Age
    {
        return $this->age_max;
    }

    public function setAgeMax(?Age $age_max): self
    {
        $this->age_max = $age_max;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDaySelection(): ?array
    {
        return $this->daySelection;
    }

    public function setDaySelection(?array $daySelection): self
    {
        $this->daySelection = $daySelection;

        return $this;
    }
}
