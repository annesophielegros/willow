<?php

namespace App\Entity;

use App\Repository\PreferenceEmailRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PreferenceEmailRepository::class)
 */
class PreferenceEmail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sender_enquiry;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $receiver_enquiry_filtered;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSenderEnquiry(): ?string
    {
        return $this->sender_enquiry;
    }

    public function setSenderEnquiry(string $sender_enquiry): self
    {
        $this->sender_enquiry = $sender_enquiry;

        return $this;
    }

    public function getReceiverEnquiryFiltered(): ?string
    {
        return $this->receiver_enquiry_filtered;
    }

    public function setReceiverEnquiryFiltered(string $receiver_enquiry_filtered): self
    {
        $this->receiver_enquiry_filtered = $receiver_enquiry_filtered;

        return $this;
    }
}
