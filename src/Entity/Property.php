<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PropertyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Property
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="properties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity=Enquiry::class, mappedBy="property")
     */
    private $enquiries;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=RoomType::class, mappedBy="property")
     */
    private $roomTypes;

    /**
     * @ORM\OneToMany(targetEntity=Filter::class, mappedBy="property")
     */
    private $filters;

    /**
     * @ORM\OneToMany(targetEntity=RateType::class, mappedBy="property")
     */
    private $rateTypes;

    /**
     * @ORM\OneToMany(targetEntity=RateRule::class, mappedBy="property")
     */
    private $rateRules;

    /**
     * @ORM\OneToMany(targetEntity=Extra::class, mappedBy="property")
     */
    private $extras;

    /**
     * @ORM\OneToMany(targetEntity=ExtraRule::class, mappedBy="property")
     */
    private $extraRules;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity=PreferenceEmail::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $preference_email;

    /**
     * @ORM\ManyToMany(targetEntity=Language::class)
     */
    private $language;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $followup_days;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $default_language;

    /**
     * @ORM\ManyToOne(targetEntity=RateDisplay::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $rateDisplay;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apiKey;

    /**
     * @ORM\Column(type="boolean")
     */
    private $emptyBedsCharged;

    /**
     * @ORM\ManyToMany(targetEntity=BreakdownPreferences::class, inversedBy="properties")
     */
    private $breakdownTypes;

    /**
     * @ORM\OneToMany(targetEntity=FollowUpPreference::class, mappedBy="property", orphanRemoval=true)
     */
    private $followUpPreferences;

    public function __construct()
    {
        $this->enquiries = new ArrayCollection();
        $this->roomTypes = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->rateTypes = new ArrayCollection();
        $this->rateRules = new ArrayCollection();
        $this->extras = new ArrayCollection();
        $this->extraRules = new ArrayCollection();
        $this->language = new ArrayCollection();
        $this->breakdownTypes = new ArrayCollection();
        $this->followUpPreferences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection|Enquiry[]
     */
    public function getEnquiries(): Collection
    {
        return $this->enquiries;
    }

    public function addEnquiry(Enquiry $enquiry): self
    {
        if (!$this->enquiries->contains($enquiry)) {
            $this->enquiries[] = $enquiry;
            $enquiry->setProperty($this);
        }

        return $this;
    }

    public function removeEnquiry(Enquiry $enquiry): self
    {
        if ($this->enquiries->removeElement($enquiry)) {
            // set the owning side to null (unless already changed)
            if ($enquiry->getProperty() === $this) {
                $enquiry->setProperty(null);
            }
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|RoomType[]
     */
    public function getRoomTypes(): Collection
    {
        return $this->roomTypes;
    }

    public function addRoomType(RoomType $roomType): self
    {
        if (!$this->roomTypes->contains($roomType)) {
            $this->roomTypes[] = $roomType;
            $roomType->setProperty($this);
        }

        return $this;
    }

    public function removeRoomType(RoomType $roomType): self
    {
        if ($this->roomTypes->removeElement($roomType)) {
            // set the owning side to null (unless already changed)
            if ($roomType->getProperty() === $this) {
                $roomType->setProperty(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCodeValue(): void
    {
        $this->code = md5($this->id . $this->label . microtime() . rand());
    }

    /**
     * @return Collection|Filter[]
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    public function addFilter(Filter $filter): self
    {
        if (!$this->filters->contains($filter)) {
            $this->filters[] = $filter;
            $filter->setProperty($this);
        }

        return $this;
    }

    public function removeFilter(Filter $filter): self
    {
        if ($this->filters->removeElement($filter)) {
            // set the owning side to null (unless already changed)
            if ($filter->getProperty() === $this) {
                $filter->setProperty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RateType[]
     */
    public function getRateTypes(): Collection
    {
        return $this->rateTypes;
    }

    public function addRateType(RateType $rateType): self
    {
        if (!$this->rateTypes->contains($rateType)) {
            $this->rateTypes[] = $rateType;
            $rateType->setProperty($this);
        }

        return $this;
    }

    public function removeRateType(RateType $rateType): self
    {
        if ($this->rateTypes->removeElement($rateType)) {
            // set the owning side to null (unless already changed)
            if ($rateType->getProperty() === $this) {
                $rateType->setProperty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RateRule[]
     */
    public function getRateRules(): Collection
    {
        return $this->rateRules;
    }

    public function addRateRule(RateRule $rateRule): self
    {
        if (!$this->rateRules->contains($rateRule)) {
            $this->rateRules[] = $rateRule;
            $rateRule->setProperty($this);
        }

        return $this;
    }

    public function removeRateRule(RateRule $rateRule): self
    {
        if ($this->rateRules->removeElement($rateRule)) {
            // set the owning side to null (unless already changed)
            if ($rateRule->getProperty() === $this) {
                $rateRule->setProperty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Extra[]
     */
    public function getExtras(): Collection
    {
        return $this->extras;
    }

    public function addExtra(Extra $extra): self
    {
        if (!$this->extras->contains($extra)) {
            $this->extras[] = $extra;
            $extra->setProperty($this);
        }

        return $this;
    }

    public function removeExtra(Extra $extra): self
    {
        if ($this->extras->removeElement($extra)) {
            // set the owning side to null (unless already changed)
            if ($extra->getProperty() === $this) {
                $extra->setProperty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExtraRule[]
     */
    public function getExtraRules(): Collection
    {
        return $this->extraRules;
    }

    public function addExtraRule(ExtraRule $extraRule): self
    {
        if (!$this->extraRules->contains($extraRule)) {
            $this->extraRules[] = $extraRule;
            $extraRule->setProperty($this);
        }

        return $this;
    }

    public function removeExtraRule(ExtraRule $extraRule): self
    {
        if ($this->extraRules->removeElement($extraRule)) {
            // set the owning side to null (unless already changed)
            if ($extraRule->getProperty() === $this) {
                $extraRule->setProperty(null);
            }
        }

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPreferenceEmail(): ?PreferenceEmail
    {
        return $this->preference_email;
    }

    public function setPreferenceEmail(?PreferenceEmail $preference_email): self
    {
        $this->preference_email = $preference_email;

        return $this;
    }

    /**
     * @return Collection|Language[]
     */
    public function getLanguage(): Collection
    {
        return $this->language;
    }

    /**
     * @return array
     */
    public function getLanguageListid(): array
    {
        return array_map(function ($language) {
            return $language->getId();
        }, $this->language->toArray());
    }

    public function addLanguage(language $language): self
    {
        if (!$this->language->contains($language)) {
            $this->language[] = $language;
        }

        return $this;
    }

    public function removeLanguage(language $language): self
    {
        $this->language->removeElement($language);

        return $this;
    }

    public function getFollowupDays(): ?int
    {
        return $this->followup_days;
    }

    public function setFollowupDays(?int $followup_days): self
    {
        $this->followup_days = $followup_days;

        return $this;
    }

    public function getDefaultLanguage(): ?Language
    {
        return $this->default_language;
    }

    public function setDefaultLanguage(?Language $default_language): self
    {
        $this->default_language = $default_language;

        return $this;
    }

    public function getRateDisplay(): ?RateDisplay
    {
        return $this->rateDisplay;
    }

    public function setRateDisplay(?RateDisplay $rateDisplay): self
    {
        $this->rateDisplay = $rateDisplay;

        return $this;
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function setApiKey(?string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getEmptyBedsCharged(): ?bool
    {
        return $this->emptyBedsCharged;
    }

    public function setEmptyBedsCharged(bool $emptyBedsCharged): self
    {
        $this->emptyBedsCharged = $emptyBedsCharged;

        return $this;
    }

    /**
     * @return Collection<int, BreakdownPreferences>
     */
    public function getBreakdownTypes(): Collection
    {
        return $this->breakdownTypes;
    }

    public function addBreakdownType(BreakdownPreferences $breakdownType): self
    {
        if (!$this->breakdownTypes->contains($breakdownType)) {
            $this->breakdownTypes[] = $breakdownType;
        }

        return $this;
    }

    public function removeBreakdownType(BreakdownPreferences $breakdownType): self
    {
        $this->breakdownTypes->removeElement($breakdownType);

        return $this;
    }

    /**
     * @return Collection<int, FollowUpPreference>
     */
    public function getFollowUpPreferences(): Collection
    {
        return $this->followUpPreferences;
    }

    public function addFollowUpPreference(FollowUpPreference $followUpPreference): self
    {
        if (!$this->followUpPreferences->contains($followUpPreference)) {
            $this->followUpPreferences[] = $followUpPreference;
            $followUpPreference->setProperty($this);
        }

        return $this;
    }

    public function removeFollowUpPreference(FollowUpPreference $followUpPreference): self
    {
        if ($this->followUpPreferences->removeElement($followUpPreference)) {
            // set the owning side to null (unless already changed)
            if ($followUpPreference->getProperty() === $this) {
                $followUpPreference->setProperty(null);
            }
        }

        return $this;
    }
}
