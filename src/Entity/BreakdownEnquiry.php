<?php

namespace App\Entity;

use App\Repository\BreakdownEnquiryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BreakdownEnquiryRepository::class)
 */
class BreakdownEnquiry
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $persons;

    /**
     * @ORM\ManyToOne(targetEntity=Enquiry::class, inversedBy="breakdownEnquiries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $enquiry;

    /**
     * @ORM\ManyToOne(targetEntity=BreakdownPreferences::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $breakdownPref;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersons(): ?int
    {
        return $this->persons;
    }

    public function setPersons(?int $persons): self
    {
        $this->persons = $persons;

        return $this;
    }

    public function getEnquiry(): ?Enquiry
    {
        return $this->enquiry;
    }

    public function setEnquiry(?Enquiry $enquiry): self
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    public function getBreakdownPref(): ?BreakdownPreferences
    {
        return $this->breakdownPref;
    }

    public function setBreakdownPref(?BreakdownPreferences $breakdownPref): self
    {
        $this->breakdownPref = $breakdownPref;

        return $this;
    }
}
