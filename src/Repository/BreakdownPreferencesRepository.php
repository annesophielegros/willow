<?php

namespace App\Repository;

use App\Entity\BreakdownPreferences;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BreakdownPreferences>
 *
 * @method BreakdownPreferences|null find($id, $lockMode = null, $lockVersion = null)
 * @method BreakdownPreferences|null findOneBy(array $criteria, array $orderBy = null)
 * @method BreakdownPreferences[]    findAll()
 * @method BreakdownPreferences[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BreakdownPreferencesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BreakdownPreferences::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(BreakdownPreferences $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(BreakdownPreferences $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param string $brandId
     * @return BreakdownPreferences[]
     */
    public function findByBrand(string $brandId): array
    {
        return $this->createQueryBuilder('b')
            ->where('b.brand = :brand')
            ->setParameter('brand', $brandId)
            ->getQuery()
            ->getResult()
            ;
    }
}
