<?php

namespace App\Repository;

use App\Entity\Status;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Status|null find($id, $lockMode = null, $lockVersion = null)
 * @method Status|null findOneBy(array $criteria, array $orderBy = null)
 * @method Status[]    findAll()
 * @method Status[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Status::class);
    }
    /**
     * @param string $brandId
     * @return Status[]
     */
    public function findByBrand(string $brandId): array
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.brand = :brand')
            ->setParameter('brand', $brandId)
            ->getQuery()
            ->getResult()
            ;
    }
    /**
     * @param $brandId
     * @return ?Status
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findDefaultStatusByBrand(int $brandId): ?Status
    {
        $qb = $this->createQueryBuilder('s');
        $qb
            ->select('s')
            ->where('s.default_selection = true')
            ->andWhere('s.brand = :brand')
            ->setParameter('brand', $brandId);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return ?Status[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAllNewBrandStatus(): array
    {
        return $this->createQueryBuilder('s')
            ->where('s.new_brand = true')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOptionalByBrand(string $brandId): ?Status
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.brand = :brand')
            ->setParameter('brand', $brandId)
            ->andWhere('s.optionalStatus = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findConfirmedByBrand(string $brandId): ?Status
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.brand = :brand')
            ->setParameter('brand', $brandId)
            ->andWhere('s.confirmedStatus = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
