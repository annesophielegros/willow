<?php

namespace App\Repository;

use App\Entity\FollowUpPreference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FollowUpPreference>
 *
 * @method FollowUpPreference|null find($id, $lockMode = null, $lockVersion = null)
 * @method FollowUpPreference|null findOneBy(array $criteria, array $orderBy = null)
 * @method FollowUpPreference[]    findAll()
 * @method FollowUpPreference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FollowUpPreferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FollowUpPreference::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(FollowUpPreference $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(FollowUpPreference $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('f')
            ->join('f.property', 'property')
            ->andWhere('f.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findOrderByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->orderBy('f.classification', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
