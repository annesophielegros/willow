<?php

namespace App\Repository;

use App\Entity\TemplatePresentation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemplatePresentation|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplatePresentation|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplatePresentation[]    findAll()
 * @method TemplatePresentation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HotelPresentationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemplatePresentation::class);
    }

    /**
     * @param string $propertyId
     * @return TemplatePresentation[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('p')
            ->join('p.property', 'property')
            ->andWhere('p.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->getQuery()
            ->getResult()
            ;
    }
}
