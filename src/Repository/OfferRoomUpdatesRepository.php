<?php

namespace App\Repository;

use App\Entity\OfferRoomUpdates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OfferRoomUpdates>
 *
 * @method OfferRoomUpdates|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferRoomUpdates|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferRoomUpdates[]    findAll()
 * @method OfferRoomUpdates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRoomUpdatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferRoomUpdates::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(OfferRoomUpdates $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(OfferRoomUpdates $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findLastOneCreated($room): ?OfferRoomUpdates
    {
        return $this->createQueryBuilder('o')
            ->where('o.offerRoom = :room')
            ->setParameter('room', $room)
            ->orderBy('o.updatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
