<?php

namespace App\Repository;

use App\Entity\TemplateCondition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemplateCondition|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplateCondition|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplateCondition[]    findAll()
 * @method TemplateCondition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemplateCondition::class);
    }

    /**
     * @param string $propertyId
     * @return TemplateCondition[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('c')
            ->join('c.property', 'property')
            ->andWhere('c.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findDefByPByL($propertyId, $languageId): ?TemplateCondition
    {
        return $this->createQueryBuilder('c')
            ->where('c.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('c.language = :language')
            ->setParameter('language', $languageId)
            ->andWhere('c.defaultSelection = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findLanguageWithDefault($propertyId): ?array
    {
        return $this->createQueryBuilder('c')
            ->join('c.language', 'l')
            ->where('c.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('c.defaultSelection = 1')
            ->select('l.id')
            ->getQuery()
            ->getResult()
            ;
    }
}
