<?php

namespace App\Repository;

use App\Entity\PreferenceEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PreferenceEmail|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreferenceEmail|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreferenceEmail[]    findAll()
 * @method PreferenceEmail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreferenceEmailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PreferenceEmail::class);
    }

    // /**
    //  * @return PreferenceEmail[] Returns an array of PreferenceEmail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PreferenceEmail
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
