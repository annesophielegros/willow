<?php

namespace App\Repository;

use App\Entity\Enquiry;
use App\Entity\FollowUp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FollowUp|null find($id, $lockMode = null, $lockVersion = null)
 * @method FollowUp|null findOneBy(array $criteria, array $orderBy = null)
 * @method FollowUp[]    findAll()
 * @method FollowUp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FollowUpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FollowUp::class);
    }

    /**
     * @param string $propertyId
     */

    public function findNumberLastCreated(string $enquiry): ?int
    {
        return $this->createQueryBuilder('f')
            ->select('f.number')
            ->where('f.enquiry = :enquiry')
            ->setParameter('enquiry', $enquiry)
            ->orderBy('f.number', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $propertyId
     * @return FollowUp[] Returns an array of Enquiry objects
     */

    public function findPassed(string $propertyId): array
    {
        $today = new \DateTime();
        return $this->createQueryBuilder('f')
            ->join('f.enquiry', 'e')
            ->andWhere('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('f.provide_date <= :today')
            ->setParameter('today', $today, Types::DATETIME_MUTABLE)
            ->andWhere('f.executedDate IS NULL')
            ->orderBy('e.enquiry_date')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $propertyId
     */

    public function findPassedFirst(string $propertyId): ?int
    {
        $today = new \DateTime();
        return $this->createQueryBuilder('f')
            ->select('count(f.id) as count')
            ->join('f.enquiry', 'e')
            ->where('f.executedDate IS NULL')
            ->andWhere('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('f.provide_date <= :today')
            ->setParameter('today', $today, Types::DATETIME_MUTABLE)
            ->andWhere('f.number = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $propertyId
     */

    public function findPassedSecond(string $propertyId): ?int
    {
        $today = new \DateTime();
        return $this->createQueryBuilder('f')
            ->select('count(f.id) as count')
            ->join('f.enquiry', 'e')
            ->where('f.executedDate IS NULL')
            ->andWhere('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('f.provide_date <= :today')
            ->setParameter('today', $today, Types::DATETIME_MUTABLE)
            ->andWhere('f.number = 2')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $propertyId
     */

    public function findPassedThird(string $propertyId): ?int
    {
        $today = new \DateTime();
        return $this->createQueryBuilder('f')
            ->select('count(f.id) as count')
            ->join('f.enquiry', 'e')
            ->where('f.executedDate IS NULL')
            ->andWhere('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('f.provide_date <= :today')
            ->setParameter('today', $today, Types::DATETIME_MUTABLE)
            ->andWhere('f.number >= 3')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
