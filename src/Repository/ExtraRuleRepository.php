<?php

namespace App\Repository;

use App\Entity\Enquiry;
use App\Entity\ExtraRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraRule[]    findAll()
 * @method ExtraRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraRule::class);
    }

    /**
     * @param Enquiry $enquiry
     * @return ExtraRule[]
     */
    public function findExtras(Enquiry $enquiry, int $occupancy): array
    {
        $numberOfNights = $enquiry->getCheckInDate()->diff($enquiry->getCheckOutDate())->format("%a");
        $dayName = $enquiry->getCheckInDate()->format('l');
        if ($enquiry->getAge()) {
            $ageMin = $enquiry->getAge()->getMin();
            $ageMax = $enquiry->getAge()->getMax();
        } else {
            $ageMin = 0;
            $ageMax = 1000;
        }

        return $this->createQueryBuilder('extra_rule')
            ->leftJoin('extra_rule.age_min', 'age_min')
            ->leftJoin('extra_rule.age_max', 'age_max')
            ->Where('
                (extra_rule.pax_max >= :pax_number OR extra_rule.pax_max is null)
                AND (extra_rule.pax_min <= :pax_number OR extra_rule.pax_min is null)
             ')
            ->setParameter('pax_number', $enquiry->getNumberOfPersons())
            ->andWhere('
                (extra_rule.length_max >= :night_number OR extra_rule.length_max is null)
                AND (extra_rule.length_min <= :night_number OR extra_rule.length_min is null)
            ')
            ->setParameter('night_number', $numberOfNights)
            ->andWhere('
                (extra_rule.date_end >= :check_in_date OR extra_rule.date_end is null)
                AND (extra_rule.date_start <= :check_in_date OR extra_rule.date_start is null)
            ')
            ->setParameter('check_in_date', $enquiry->getCheckInDate(), Types::DATETIME_MUTABLE)
            ->andWhere("extra_rule.daySelection LIKE :day OR extra_rule.daySelection LIKE '%a:0:{}%'")
            ->setParameter('day', '%' . $dayName . '%')
            ->andWhere('
                (extra_rule.occupancy_max >= :occupancy OR extra_rule.occupancy_max is null)
                AND (extra_rule.occupancy_min <= :occupancy OR extra_rule.occupancy_min is null)
            ')
            ->setParameter('occupancy', $occupancy)
            ->andWhere('
                (age_max.max >= :enq_age_max OR age_max is null OR age_max.max is null)
                AND (age_min.min <= :enq_age_min OR age_min is null OR age_min.min is null)
            ')
            ->setParameter('enq_age_min', $ageMin)
            ->setParameter('enq_age_max', $ageMax)
            ->andWhere('extra_rule.active = 1')
            ->andWhere('extra_rule.property = :property')
            ->setParameter('property', $enquiry->getProperty()->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllByProperty($property_id)
    {
        return $this->createQueryBuilder('extra_rule')
            ->where('extra_rule.property = :property')
            ->setParameter('property', $property_id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAgeByBrand($brand, $age): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('e.age_min = :age OR e.age_max = :age')
            ->setParameter('age', $age)
            ->getQuery()
            ->getResult();
    }

    public function findExtraByProperty($property, $extra): ?array
    {
        return $this->createQueryBuilder('e')
            ->where('e.property = :property')
            ->setParameter('property', $property)
            ->andWhere('e.extra = :extra')
            ->setParameter('extra', $extra)
            ->getQuery()
            ->getResult();
    }
}
