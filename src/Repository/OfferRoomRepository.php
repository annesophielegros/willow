<?php

namespace App\Repository;

use App\Entity\OfferRoom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfferRoom|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferRoom|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferRoom[]    findAll()
 * @method OfferRoom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferRoom::class);
    }

    // /**
    //  * @return OfferRoom[] Returns an array of OfferRoom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferRoom
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
