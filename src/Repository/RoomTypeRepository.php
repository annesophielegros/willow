<?php

namespace App\Repository;

use App\Entity\Enquiry;
use App\Entity\RoomType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoomType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomType[]    findAll()
 * @method RoomType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoomType::class);
    }

    /**
     * @param Enquiry $enquiry
     * @return RoomType[] Returns an array of RoomType objects
     */
    public function findOrderByPriority(Enquiry $enquiry): array
    {
        return $this->createQueryBuilder('room_type')
            ->addSelect('CASE WHEN room_type.priority IS NULL THEN 1 ELSE 0 END AS HIDDEN priorityIsNull')
            ->where('room_type.groupOffers = 1')
            ->orderBy('priorityIsNull', 'ASC')
            ->andWhere('room_type.activated = 1')
            ->andWhere('room_type.property = :property')
            ->setParameter('property', $enquiry->getProperty())
            ->addOrderBy('room_type.priority', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Enquiry $enquiry
     * @return RoomType[] Returns an array of RoomType objects
     */
    public function findRoomsForOccupancy(Enquiry $enquiry): array
    {
        return $this->createQueryBuilder('room_type')
            ->andWhere('room_type.property = :property')
            ->setParameter('property', $enquiry->getProperty())
            ->andWhere('room_type.activated = 1')
            ->addOrderBy('room_type.capacity', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return RoomType[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('rt')
            ->join('rt.property', 'property')
            ->andWhere('rt.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->orderBy('rt.priority', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return RoomType[] Returns an array of Enquiry objects
     */
    public function findGroupOffersByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('rt')
            ->join('rt.property', 'property')
            ->andWhere('rt.property = :propertyId')
            ->andWhere('rt.activated = true')
            ->setParameter('propertyId', $propertyId)
            ->orderBy('rt.priority', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findSamePriorityByP($propertyId, $priorityId): ?RoomType
    {
        return $this->createQueryBuilder('rt')
            ->where('rt.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('rt.priority = :priority')
            ->setParameter('priority', $priorityId)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findLowestPriorityByP($propertyId): array
    {
        return $this->createQueryBuilder('rt')
            ->where('rt.property = :property')
            ->setParameter('property', $propertyId)
            ->orderBy('rt.priority', 'DESC')
            ->setMaxResults(1)
            ->select('rt.priority')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
