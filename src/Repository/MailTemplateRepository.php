<?php

namespace App\Repository;

use App\Entity\TemplateMail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemplateMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplateMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplateMail[]    findAll()
 * @method TemplateMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemplateMail::class);
    }

    /**
     * @param string $propertyId
     * @return TemplateMail[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('mt')
            ->join('mt.property', 'property')
            ->andWhere('mt.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findProposalDefByPByL($propertyId, $languageId): ?TemplateMail
    {
        return $this->createQueryBuilder('m')
            ->where('m.property = :property')
            ->join('m.type', 't')
            ->setParameter('property', $propertyId)
            ->andWhere('m.language = :language')
            ->setParameter('language', $languageId)
            ->andWhere('m.defaultSelection = 1')
            ->andWhere('t.label = :label')
            ->setParameter('label', 'Proposal')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findPendingDefByPByL($propertyId, $languageId): ?TemplateMail
    {
        return $this->createQueryBuilder('m')
            ->where('m.property = :property')
            ->join('m.type', 't')
            ->setParameter('property', $propertyId)
            ->andWhere('m.language = :language')
            ->setParameter('language', $languageId)
            ->andWhere('m.defaultSelection = 1')
            ->andWhere('t.label = :label')
            ->setParameter('label', 'Pending to quote')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findLangWithDefaultProposal($propertyId): ?array
    {
        return $this->createQueryBuilder('m')
            ->join('m.language', 'l')
            ->join('m.type', 't')
            ->where('m.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('m.defaultSelection = 1')
            ->andWhere('t.label = :label')
            ->setParameter('label', 'Proposal')
            ->select('l.id')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLangWithDefaultPending($propertyId): ?array
    {
        return $this->createQueryBuilder('m')
            ->join('m.language', 'l')
            ->join('m.type', 't')
            ->where('m.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('m.defaultSelection = 1')
            ->andWhere('t.label = :label')
            ->setParameter('label', 'Pending to quote')
            ->select('l.id')
            ->getQuery()
            ->getResult()
            ;
    }
}
