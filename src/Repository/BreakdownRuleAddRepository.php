<?php

namespace App\Repository;

use App\Entity\BreakdownRuleAdd;
use App\Entity\Enquiry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BreakdownRuleAdd|null find($id, $lockMode = null, $lockVersion = null)
 * @method BreakdownRuleAdd|null findOneBy(array $criteria, array $orderBy = null)
 * @method BreakdownRuleAdd[]    findAll()
 * @method BreakdownRuleAdd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BreakdownRuleAddRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BreakdownRuleAdd::class);
    }

    /**
     * @param Enquiry $enquiry
     * @return BreakdownRuleAdd[] Returns an array of BreakdownRuleAdd objects
     */
    public function findAddRules(Enquiry $enquiry, $occupancy): array
    {
        $numberOfNights = $enquiry->getCheckInDate()->diff($enquiry->getCheckOutDate())->format("%a");
        $dayName = $enquiry->getCheckInDate()->format('l');
        if ($enquiry->getAge()) {
            $ageMin = $enquiry->getAge()->getMin();
            $ageMax = $enquiry->getAge()->getMax();
        } else {
            $ageMin = 0;
            $ageMax = 1000;
        }

        return $this->createQueryBuilder('breakdown_rule_add')
            ->leftJoin('breakdown_rule_add.age_min', 'age_min')
            ->leftJoin('breakdown_rule_add.age_max', 'age_max')
            ->Where('
                (breakdown_rule_add.pax_max >= :pax_number OR breakdown_rule_add.pax_max is null)
                AND (breakdown_rule_add.pax_min <= :pax_number OR breakdown_rule_add.pax_min is null)
             ')
            ->setParameter('pax_number', $enquiry->getNumberOfPersons())
            ->andWhere('
                (breakdown_rule_add.length_max >= :night_number OR breakdown_rule_add.length_max is null)
                AND (breakdown_rule_add.length_min <= :night_number OR breakdown_rule_add.length_min is null)
            ')
            ->setParameter('night_number', $numberOfNights)
            ->andWhere('
                (breakdown_rule_add.occupancy_max >= :occupancy OR breakdown_rule_add.occupancy_max is null)
                AND (breakdown_rule_add.occupancy_min <= :occupancy OR breakdown_rule_add.occupancy_min is null)
            ')
            ->setParameter('occupancy', $occupancy)
            ->andWhere('
                (age_max.max >= :enq_age_max OR age_max is null OR age_max.max is null)
                AND (age_min.min <= :enq_age_min OR age_min is null OR age_min.min is null)
            ')
            ->setParameter('enq_age_min', $ageMin)
            ->setParameter('enq_age_max', $ageMax)
            ->andWhere('
                (breakdown_rule_add.date_end >= :check_in_date OR breakdown_rule_add.date_end is null)
                AND (breakdown_rule_add.date_start <= :check_in_date OR breakdown_rule_add.date_start is null)
            ')
            ->setParameter('check_in_date', $enquiry->getCheckInDate(), Types::DATETIME_MUTABLE)
            ->andWhere("breakdown_rule_add.daySelection LIKE :day OR breakdown_rule_add.daySelection LIKE '%a:0:{}%'")
            ->setParameter('day', '%' . $dayName . '%')
            ->andWhere('breakdown_rule_add.active = 1')
            ->andWhere('breakdown_rule_add.property = :property')
            ->setParameter('property', $enquiry->getProperty()->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllByProByRoom($propertyId, $roomId): array
    {
        return $this->createQueryBuilder('b')
            ->where('b.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('b.room_to_add = :room')
            ->setParameter('room', $roomId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllByProperty($property_id): array
    {
        return $this->createQueryBuilder('breakdown_rule_add')
            ->where('breakdown_rule_add.property = :property')
            ->setParameter('property', $property_id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findInactiveRoom(int $property_id): ?array
    {
        return $this->createQueryBuilder('b')
            ->join('b.room_to_add', 'rt')
            ->where('b.property = :property')
            ->setParameter('property', $property_id)
            ->andWhere('rt.activated = 0')
            ->select('rt.label')
            ->groupBy('rt.label')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAgeByBrand($brand, $age): ?array
    {
        return $this->createQueryBuilder('b')
            ->join('b.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('b.age_min = :age OR b.age_max = :age')
            ->setParameter('age', $age)
            ->getQuery()
            ->getResult();
    }
}
