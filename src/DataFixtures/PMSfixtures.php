<?php

namespace App\DataFixtures;

use App\Entity\PropertyManagementSystem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PMSfixtures extends Fixture
{
    public const PMS_LIST = array(
        'Mews',
        'Mock',
        'Pxier',
    );

    public function load(ObjectManager $manager)
    {
        $pms_reference = [];
        $i = 1;
        foreach (self::PMS_LIST as $pmsValue) {
            $pms = new PropertyManagementSystem();
            $pms->setLabel($pmsValue);
            $pms_reference['pms_' . $i] = $pms;
            $manager->persist($pms);
            $i++;
        }

        $manager->flush();

        foreach ($pms_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }
}
