<?php

namespace App\DataFixtures;

use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PropertyFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const PROPERTY_NUMBER = 8;

    protected $faker;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();
        $properties_reference = [];
        for ($i = 1; $i <= self::PROPERTY_NUMBER; $i++) {
            $property = new Property();
            $property->setLabel($this->faker->city);
            $property->setBrand($this->getReference('brand_' . rand(1, BrandFixtures::BRAND_NUMBER)));
            $property->setCode($this->faker->hexColor);
            $property->setAddress($this->getReference('address_' . rand(1, AddressFixtures::ADDRESS_COUNT)));
            $refCurrency = 'currency_' . rand(1, count(CurrencyFixtures::CURRENCY_LIST));
            $property->setCurrency($this->getReference($refCurrency));
            $refEmail = 'preference_email_' . rand(1, PreferenceEmailFixtures::PREFERENCE_EMAIL_COUNT);
            $property->setPreferenceEmail($this->getReference($refEmail));
            $property->addLanguage($this->getReference('language_1'));
            $property->addLanguage($this->getReference('language_2'));
            $property->setDefaultLanguage($this->getReference('language_1'));
            $property->setRateDisplay($this->getReference('display_1'));
            $property->setEmptyBedsCharged(true);
            $properties_reference['property_' . $i] = $property;
            $manager->persist($property);
        }

        $manager->flush();

        foreach ($properties_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            BrandFixtures::class,
            AddressFixtures::class,
            PreferenceEmailFixtures::class,
            LanguageFixtures::class,
            CurrencyFixtures::class,
            RateDisplayFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
