<?php

namespace App\DataFixtures;

use App\Entity\TemplatePresentation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TemplatePresentationFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class,
            LanguageFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= PropertyFixtures::PROPERTY_NUMBER; $i++) {
            $hotelPresentation = new TemplatePresentation();
            $hotelPresentation->setLabel('2022 EN');
            $hotelPresentation->setProperty($this->getReference('property_' . $i));
            $html = "<p>Hotel Willow welcome you anytime</p>";
            $hotelPresentation->setContent($html);
            $hotelPresentation->setFeatures(
                'We can offer a <b>large variety</b> of services from swimming pool to pool table'
            );
            $hotelPresentation->setLanguage($this->getReference('language_1'));
            $references['property_' . $i . '_hotel_presentation'] = $hotelPresentation;
            $manager->persist($hotelPresentation);
        }

        $manager->flush();

        foreach ($references as $key => $item) {
            $this->addReference($key, $item);
        }
    }
}
