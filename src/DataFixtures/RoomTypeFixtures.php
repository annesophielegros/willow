<?php

namespace App\DataFixtures;

use App\Entity\RoomType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RoomTypeFixtures extends Fixture implements DependentFixtureInterface
{
    protected $faker;

    public const ROOMTYPE = [
        "Single" => [
            "capacity" => 1,
            "quantity" => 12,
            "breakdown" => true,
            "priority" => 8,
        ],
        "Twin" => [
            "capacity" => 2,
            "quantity" => 12,
            "breakdown" => true,
            "priority" => 7,
        ],
        "5BR" => [
            "capacity" => 5,
            "quantity" => 12,
            "breakdown" => true,
            "priority" => 5,
        ],
        "7BR" => [
            "capacity" => 7,
            "quantity" => 12,
            "breakdown" => true,
            "priority" => 6,
        ],
        "12BR" => [
            "capacity" => 12,
            "quantity" => 20,
            "breakdown" => true,
            "priority" => 1,
        ],
        "14BR" => [
            "capacity" => 14,
            "quantity" => 20,
            "breakdown" => true,
            "priority" => 2,
        ],
        "16BR" => [
            "capacity" => 16,
            "quantity" => 12,
            "breakdown" => true,
            "priority" => 4,
        ],
        "20BR" => [
            "capacity" => 20,
            "quantity" => 12,
            "breakdown" => true,
            "priority" => 3,
        ],
    ];

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();
        for ($i = 1; $i <= 8; $i++) {
            $count = 1;
            foreach (self::ROOMTYPE as $label => $room) {
                $roomType = new RoomType();
                $roomType->setLabel($label);
                $roomType->setProperty($this->getReference((string)'property_' . $i));
                $roomType->setGroupOffers($room['breakdown']);
                $roomType->setQuantity($room['quantity']);
                $roomType->setCapacity($room['capacity']);
                $roomType->setPriority($room['priority']);
                $roomType->setProviderId($count);
                $roomstype_reference['property_' . $i . '_roomtype_' . $count] = $roomType;
                $manager->persist($roomType);
                $count++;
            }
        }
        $manager->flush();

        foreach ($roomstype_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            PropertyFixtures::class,
        );
    }
}
