<?php

namespace App\DataFixtures;

use App\Entity\Language;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LanguageFixtures extends Fixture
{
    public const LANGUAGE_LIST = array(
        'EN' => 'English',
        'ES' => 'Spanish',
        'FR' => 'French',
        'GE' => 'German',
        'IT' => 'Italian',
    );

    public function load(ObjectManager $manager)
    {
        $language_reference = [];
        $i = 1;
        foreach (self::LANGUAGE_LIST as $languageKey => $languageValue) {
            $language = new Language();
            $language->setLabel($languageValue);
            $language->setAbbreviation($languageKey);
            $language_reference['language_' . $i] = $language;
            $manager->persist($language);
            $i++;
        }

        $manager->flush();

        foreach ($language_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }
}
