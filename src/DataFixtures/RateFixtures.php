<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RateFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->createRatesAll();
        $this->createRatesPrincing();
    }

    private function createRatesAll()
    {
        $resources = [];
        $resources['RateRestrictions'] = null;
        $resources['RateGroups'] = null;

        $rates = [];
        $id = 1;
        foreach (RateTypeFixtures::RATES_TYPE as $label => $r) {
            $rate = [
                "Id" => "$id",
                "GroupId" => 'b5487848-0149-4e8b-8a31-6c771e89435e',
                "ServiceId" => 'bd26d8db-86da-4f96-9efc-e5a4654a4a94',
                "BaseRateId" => true,
                "IsActive" => true,
                "IsEnabled" => true,
                "IsPublic" => true,
                "Type" => "Public",
                "Name" => $label,
                "ShortName" => "FB",
                "ExternalNames" => [
                    "en-US" => $label,
                ]
            ];
            $rates[] = $rate;
            $id++;
        }
        $resources['Rates'] = $rates;

        $fileDir = __DIR__ . '/../Requestor/mock/rates';
        if (!is_dir($fileDir)) {
            mkdir($fileDir, 0777, true);
        }
        $fp = fopen("{$fileDir}/getAll.json", 'w');
        fwrite($fp, json_encode($resources));
        fclose($fp);
    }

    private function createRatesPrincing()
    {
        $date_start = (new \DateTime())->modify('-1 month');
        $date_end = (new \DateTime())->modify('+15 days');

        $period = new \DatePeriod(
            $date_start,
            new \DateInterval('P1D'),
            $date_end
        );

        foreach (RateTypeFixtures::RATES_TYPE as $rateLabel => $ratePrices) {
            $nbDays = 0;
            $dateUTC = [];
            foreach ($period as $dateTime) {
                $dateUTC[] = $dateTime->format('c');
                $nbDays++;
            }
            $pricing = [];
            $categoryPrices = [];
            $id = 1;
            foreach (RoomTypeFixtures::ROOMTYPE as $label => $room) {
                $prices = [];
                for ($i = 1; $i <= $nbDays; $i++) {
                    $prices[] = rand($id * $ratePrices[0], $id * $ratePrices[1]);
                }

                $adjustment = [];
                for ($i = 1; $i <= $nbDays; $i++) {
                    $adjustment[] = rand(1, 3);
                }

                $categorie = [
                    "CategoryId" => "$id",
                    "Prices" => $prices,
                ];
                $categoryPrices[] = $categorie;
                $id++;
            }
            $pricing['DatesUtc'] = $dateUTC;
            $pricing['BasePrices'] = null;
            $pricing['Currency'] = "GBP";
            $pricing['CategoryPrices'] = $categoryPrices;

            $fileDir = __DIR__ . '/../Requestor/mock/rates';
            if (!is_dir($fileDir)) {
                mkdir($fileDir, 0777, true);
            }
            $fp = fopen("{$fileDir}/getPricing_{$rateLabel}.json", 'w');
            fwrite($fp, json_encode($pricing));
            fclose($fp);
        }
    }
}
