<?php

namespace App\DataFixtures;

use App\Entity\FollowUp;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class FollowUpFixtures extends Fixture implements DependentFixtureInterface
{
    protected $faker;

    public function getDependencies()
    {
        return array(
            EnquiryFixtures::class
        );
    }

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();

        for ($i = 1; $i <= 20; $i++) {
            $followUp = new FollowUp();
            $followUp->setComment($this->faker->text);
            $date = $this->faker->dateTimeInInterval('-3 days', '+5 days');
            $followUp->setProvideDate($date);
            $followUp->setEnquiry($this->getReference('enquiry_' . rand(1, EnquiryFixtures::ENQUIRY_NUMBER)));
            $followUp->setNumber(1);

            $manager->persist($followUp);
        }

        $manager->flush();
    }
}
