<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const USER_NUMBER = 8;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    protected $faker;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $roles = ['ROLE_REVENUE_MANAGER' , 'ROLE_RESERVATION_AGENT'];
        $this->faker = Factory::create();

        $god = $this->createGod();
        $manager->persist($god);
        $wadmin = $this->createWillowAdmin();
        $manager->persist($wadmin);
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $admin = $this->createAdmin($i);
            $manager->persist($admin);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $admin = $this->createRevenueManager($i);
            $manager->persist($admin);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $admin = $this->createUserAgent($i);
            $manager->persist($admin);
        }
        for ($i = 1; $i <= self::USER_NUMBER; $i++) {
            $brand = $this->getReference('brand_' . rand(1, BrandFixtures::BRAND_NUMBER));
            $user = new User();
            $user->setFirstname($this->faker->firstName);
            $user->setLastname($this->faker->lastName);
            $user->setEmail($this->faker->companyEmail);
            $user->setBrand($brand);
            $user->setRoles([$roles[rand(0, 1)]]);
            $user->setPassword($this->userPasswordEncoder->encodePassword(
                $user,
                'pass'
            ));
            for ($j = 1; $j <= PropertyFixtures::PROPERTY_NUMBER; $j++) {
                $property = $this->getReference('property_' . $j);
                if ($property->getBrand()->getId() === $brand->getId()) {
                    $user->addProperty($property);
                }
            }

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            BrandFixtures::class,
            PropertyFixtures::class,
        );
    }

    private function createGod(): User
    {
        $user = new User();
        $user->setFirstname($this->faker->firstName);
        $user->setLastname($this->faker->lastName);
        $user->setEmail("super@admin.fr");
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $user->setPassword($this->userPasswordEncoder->encodePassword(
            $user,
            'pass'
        ));

        return $user;
    }

    private function createWillowAdmin(): User
    {
        $user = new User();
        $user->setFirstname($this->faker->firstName);
        $user->setLastname($this->faker->lastName);
        $user->setEmail("admin@willlow.fr");
        $user->setRoles(['ROLE_ADMIN_WILLOW']);
        $user->setPassword($this->userPasswordEncoder->encodePassword(
            $user,
            'pass'
        ));

        return $user;
    }

    private function createAdmin($num): User
    {
        $user = new User();
        $user->setFirstname($this->faker->firstName);
        $user->setLastname($this->faker->lastName);
        $user->setEmail("admin$num@admin.fr");
        $user->setBrand($this->getReference('brand_' . $num));
        $user->setRoles(['ROLE_ADMIN_BRAND']);
        $user->setPassword($this->userPasswordEncoder->encodePassword(
            $user,
            'pass'
        ));

        return $user;
    }

    private function createRevenueManager($num): User
    {
        $user = new User();
        $user->setFirstname($this->faker->firstName);
        $user->setLastname($this->faker->lastName);
        $user->setEmail("revenue$num@manager.fr");
        $user->setBrand($this->getReference('brand_' . $num));
        $user->setRoles(['ROLE_REVENUE_MANAGER']);
        $user->setPassword($this->userPasswordEncoder->encodePassword(
            $user,
            'pass'
        ));

        return $user;
    }

    private function createUserAgent($num): User
    {
        $user = new User();
        $user->setFirstname($this->faker->firstName);
        $user->setLastname($this->faker->lastName);
        $user->setEmail("user$num@agent.fr");
        $user->setBrand($this->getReference('brand_' . $num));
        $user->setRoles(['ROLE_RESERVATION_AGENT']);
        $user->setPassword($this->userPasswordEncoder->encodePassword(
            $user,
            'pass'
        ));

        return $user;
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
