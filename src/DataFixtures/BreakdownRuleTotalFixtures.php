<?php

namespace App\DataFixtures;

use App\Entity\BreakdownRuleAdd;
use App\Entity\BreakdownRuleRemaining;
use App\Entity\BreakdownRuleTotal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;

class BreakdownRuleTotalFixtures extends Fixture implements DependentFixtureInterface
{
    protected $faker;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();

        for ($i = 1; $i <= 8; $i++) {
            for ($j = 1; $j <= 3; $j++) {
                $breakdown = new BreakdownRuleTotal();
                $roomRef = 'property_' . $i . '_roomtype_' . rand(1, count(RoomTypeFixtures::ROOMTYPE));
                $breakdown->setRoom($this->getReference($roomRef));
                $breakdown->setProperty($this->getReference('property_' . $i));
                $breakdown->setLabel('breakdown rule total ' . $i);
                $breakdown->setPercentageLeft(rand(20, 70));
                $breakdown->setActive(1);
                $breakdown->setPaxMin(rand(15, 20));
                $breakdown->setPaxMax(rand(60, 100));
                $date = (new \DateTime('now'));
                $breakdown->setDateStart($date);
                $newDate = (new \DateTime('now'))->modify('+15 day');
                $breakdown->setDateEnd($newDate);
                $breakdown->setLengthMin(rand(1, 2));
                $breakdown->setLengthMax(rand(4, 5));

                $manager->persist($breakdown);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PropertyFixtures::class,
            RoomTypeFixtures::class,
        );
    }
}
