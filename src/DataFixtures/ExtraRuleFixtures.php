<?php

namespace App\DataFixtures;

use App\Entity\ExtraRule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ExtraRuleFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class,
            ExtraFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 8; $i++) {
            //first night per pax
            $extra = new ExtraRule();
            $extra->setActive(true);
            $extra->setLabel('extra rule 1 ' . $i);
            $extra->setPaxMin(rand(5, 7));
            $extra->setPaxMax(rand(120, 140));
            $date = (new \DateTime('now'))->modify('-1 month');
            $extra->setDateStart($date);
            $newDate = (new \DateTime('now'))->modify('+1 month');
            $extra->setDateEnd($newDate);
            $extra->setLengthMin(rand(1, 2));
            $extra->setLengthMax(rand(15, 20));
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setExtra($this->getReference('property_' . $i . '_extra_towels'));
            $extra->setConsumed('1st night');
            $extra->setDivision('per pax');

            $manager->persist($extra);

            //all night per pax
            $extra = new ExtraRule();
            $extra->setActive(true);
            $extra->setLabel('extra rule 2 ' . $i);
            $extra->setPaxMin(rand(5, 7));
            $extra->setPaxMax(rand(120, 140));
            $date = (new \DateTime('now'))->modify('-1 month');
            $extra->setDateStart($date);
            $newDate = (new \DateTime('now'))->modify('+1 month');
            $extra->setDateEnd($newDate);
            $extra->setLengthMin(rand(1, 2));
            $extra->setLengthMax(rand(15, 20));
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setExtra($this->getReference('property_' . $i . '_extra_breakfast'));
            $extra->setConsumed('every next day');
            $extra->setDivision('per pax');

            $manager->persist($extra);

            //first night per group
            $extra = new ExtraRule();
            $extra->setActive(true);
            $extra->setLabel('extra rule 3 ' . $i);
            $extra->setPaxMin(rand(5, 7));
            $extra->setPaxMax(rand(120, 140));
            $date = (new \DateTime('now'))->modify('-1 month');
            $extra->setDateStart($date);
            $newDate = (new \DateTime('now'))->modify('+1 month');
            $extra->setDateEnd($newDate);
            $extra->setLengthMin(rand(1, 2));
            $extra->setLengthMax(rand(15, 20));
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setExtra($this->getReference('property_' . $i . '_extra_room'));
            $extra->setConsumed('1st night');
            $extra->setDivision('per group');

            $manager->persist($extra);

            //first night per room
            $extra = new ExtraRule();
            $extra->setActive(true);
            $extra->setLabel('extra rule 4 ' . $i);
            $extra->setPaxMin(rand(5, 7));
            $extra->setPaxMax(rand(120, 140));
            $date = (new \DateTime('now'))->modify('-1 month');
            $extra->setDateStart($date);
            $newDate = (new \DateTime('now'))->modify('+1 month');
            $extra->setDateEnd($newDate);
            $extra->setLengthMin(rand(1, 2));
            $extra->setLengthMax(rand(15, 20));
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setExtra($this->getReference('property_' . $i . '_extra_flowers'));
            $extra->setConsumed('1st night');
            $extra->setDivision('per room');

            $manager->persist($extra);

            //all night per room
            $extra = new ExtraRule();
            $extra->setActive(true);
            $extra->setLabel('extra rule 5 ' . $i);
            $extra->setPaxMin(rand(5, 7));
            $extra->setPaxMax(rand(120, 140));
            $date = (new \DateTime('now'))->modify('-1 month');
            $extra->setDateStart($date);
            $newDate = (new \DateTime('now'))->modify('+1 month');
            $extra->setDateEnd($newDate);
            $extra->setLengthMin(rand(1, 2));
            $extra->setLengthMax(rand(15, 20));
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setExtra($this->getReference('property_' . $i . '_extra_apero'));
            $extra->setConsumed('every day');
            $extra->setDivision('per room');

            $manager->persist($extra);
        }

        $manager->flush();
    }
}
