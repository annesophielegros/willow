<?php

namespace App\DataFixtures;

use App\Entity\RateDisplay;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RateDisplayFixtures extends Fixture
{
    public const DISPLAY_LIST = array(
        'Average',
        'Nightly',
    );

    public function load(ObjectManager $manager)
    {
        $display_reference = [];
        $i = 1;
        foreach (self::DISPLAY_LIST as $displayValue) {
            $display = new RateDisplay();
            $display->setLabel($displayValue);
            $display_reference['display_' . $i] = $display;
            $manager->persist($display);
            $i++;
        }

        $manager->flush();

        foreach ($display_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }
}
