<?php

namespace App\DataFixtures;

use App\Entity\RateRule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RateRuleFixture extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class,
            RateTypeFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 8; $i++) {
            //Default
            $rateRule = new RateRule();
            $rateRule->setActive(true);
            $rateRule->setProperty($this->getReference('property_' . $i));
            $rateRule->setRateType($this->getReference('property_' . $i . '_ratetype_' . rand(1, 4)));
            $rateRule->setDefaultSelection(1);
            $rateRule->setPriority(4);

            $manager->persist($rateRule);

            for ($j = 1; $j <= 3; $j++) {
                $rateRule = new RateRule();
                $rateRule->setActive(true);
                $rateRule->setProperty($this->getReference('property_' . $i));
                $rateRule->setRateType($this->getReference('property_' . $i . '_ratetype_' . rand(1, 4)));
                $rateRule->setPaxMin(rand(15, 20));
                $rateRule->setPaxMax(rand(60, 100));
                $date = (new \DateTime('now'));
                $rateRule->setDateStart($date);
                $newDate = (new \DateTime('now'))->modify('+15 day');
                $rateRule->setDateEnd($newDate);
                $rateRule->setLengthMin(rand(1, 2));
                $rateRule->setLengthMax(rand(4, 5));
                if (rand(1, 2) === 2) {
                    $rateRule->setBaseRateFix(rand(10, 50));
                } else {
                    $rateRule->setBaseRateVariable(rand(10, 50));
                }
                $rateRule->setDefaultSelection(0);
                $rateRule->setPriority(rand(1, 3));

                $manager->persist($rateRule);
            }
        }

        $manager->flush();
    }
}
