<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BrandFixtures extends Fixture implements FixtureGroupInterface
{
    public const BRAND_NUMBER = 3;

    protected $faker;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();
        $brands_reference = [];
        for ($i = 1; $i <= self::BRAND_NUMBER; $i++) {
            $brand = new Brand();
            $label = $this->faker->domainName;
            if ($i == 1) {
                $label = 'Willow';
            }
            $brand->setLabel($label);
            $brand->setCreationDate(new \DateTime());
            $brand->setComment($this->faker->text);
            $brand->setPmsGetData($this->getReference('pms_2'));
            $brand->setPmsPostData($this->getReference('pms_2'));
            if ($i == 1) {
                $brand->setPmsGetData($this->getReference('pms_1'));
                $brand->setPmsPostData($this->getReference('pms_1'));
            }
            $brands_reference['brand_' . $i] = $brand;
            $manager->persist($brand);
        }

        $manager->flush();

        foreach ($brands_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }

    public function getDependencies()
    {
        return array(
            PMSfixtures::class,
        );
    }
}
