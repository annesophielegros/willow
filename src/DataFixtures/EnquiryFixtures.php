<?php

namespace App\DataFixtures;

use App\Entity\Enquiry;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EnquiryFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENQUIRY_NUMBER = 200;

    protected $faker;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();

        $enquiries_reference = [];
        for ($i = 1; $i <= self::ENQUIRY_NUMBER; $i++) {
            $enquiry = new Enquiry();
            $enquiry->setNumberOfPersons(rand(10, 80));
            $dateCheckIn = $this->faker->dateTimeBetween('-1 month');
            $enquiry->setCheckInDate($dateCheckIn);
            $new = clone $dateCheckIn;
            $dateCheckOut = $new->modify('+' . rand(2, 8) . ' day');
            $enquiry->setCheckOutDate($dateCheckOut);
            $enquiry->setContactFirstName($this->faker->lastName);
            $enquiry->setContactLastName($this->faker->firstName);
            $enquiry->setGroupName($this->faker->name);
            $enquiry->setEmptyBedsCharged(true);
            $enquiry->setEmailAddress($this->faker->email);
            $enquiry->setPhoneNumber($this->faker->phoneNumber);
            $enquiry->setProperty($this->getReference('property_' . rand(1, 8)));
            $propertyId = $enquiry->getProperty()->getId();
            $brandId = $enquiry->getProperty()->getBrand()->getId();
            $enquiry->setReservationNumber($brandId . $propertyId . rand(1, 1000));
            $enquiry->setStatus($this->getReference('status_' . $brandId));
            $enquiry->setRatetype($this->getReference('property_' . rand(1, 8) . '_ratetype_' . rand(1, 4)));
            $enquiry->setLanguage($this->getReference('language_1'));
            $enquiry->setRateDisplay($this->getReference('display_1'));
            $enquiries_reference['enquiry_' . $i] = $enquiry;
            $manager->persist($enquiry);
        }

        $manager->flush();

        foreach ($enquiries_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            PropertyFixtures::class,
            BrandFixtures::class,
            StatusFixtures::class,
            RateTypeFixtures::class,
            LanguageFixtures::class,
            RateDisplayFixtures::class,
            BreakdownPreferencesFixtures::class,
        );
    }
}
