<?php

namespace App\Security\Voter;

use App\Entity\BreakdownRuleAdd;
use App\Entity\BreakdownRuleRemaining;
use App\Entity\BreakdownRuleTotal;
use App\Entity\Extra;
use App\Entity\ExtraRule;
use App\Entity\Filter;
use App\Entity\RateRule;
use App\Entity\RateType;
use App\Entity\RoomType;
use App\Entity\TemplateCondition;
use App\Entity\TemplateMail;
use App\Entity\TemplatePresentation;
use App\Entity\TemplateProposal;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class RevenueConfigurationVoter extends Voter
{
    private const EDIT = 'edit';
    private const DELETE = 'delete';

    private const SUBJECTS = [
        Filter::class,
        BreakdownRuleAdd::class,
        BreakdownRuleRemaining::class,
        BreakdownRuleTotal::class,
        RateRule::class,
        ExtraRule::class,
        RoomType::class,
        RateType::class,
        Extra::class,
        TemplateCondition::class,
        TemplateMail::class,
        TemplateProposal::class,
        TemplatePresentation::class,
    ];

    /**
     * @var Security
     */
    private $security;

    private EntityManagerInterface $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::DELETE])
            && in_array(get_class($subject), self::SUBJECTS);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }
        if ($this->security->isGranted('ROLE_ADMIN_WILLOW')) {
            return true;
        }
        if ($this->security->isGranted('ROLE_ADMIN_BRAND')) {
            return $this->canAdminBrand($subject, $user);
        }
        switch ($attribute) {
            case self::EDIT:
            case self::DELETE:
                return $this->canEditOrDelete($subject, $user);
                break;
        }

        return false;
    }

    private function canAdminBrand($subject, $user)
    {
        $list = $this->entityManager->getRepository('App:Property')->findAllByBrand($user->getBrand()->getId());
        $list = array_map(function ($item) {
            return $item->getId();
        }, $list);

        return in_array($subject->getProperty()->getId(), $list);
    }

    private function canEditOrDelete(Filter $filter, UserInterface $user): bool
    {
        return in_array($filter->getProperty()->getId(), $user->getPropertyListid());
    }
}
