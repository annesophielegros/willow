<?php

namespace App\EventListener;

use App\Entity\BreakdownRuleAdd;
use App\Entity\BreakdownRuleRemaining;
use App\Entity\BreakdownRuleTotal;
use App\Entity\Extra;
use App\Entity\ExtraRule;
use App\Entity\Filter;
use App\Entity\RateRule;
use App\Entity\RateType;
use App\Entity\RoomType;
use App\Entity\TemplateCondition;
use App\Entity\TemplateMail;
use App\Entity\TemplatePresentation;
use App\Entity\TemplateProposal;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PropertySetter
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (
            $entity instanceof RateRule
            || $entity instanceof RoomType
            || $entity instanceof RateType
            || $entity instanceof Filter
            || $entity instanceof Extra
            || $entity instanceof ExtraRule
            || $entity instanceof BreakdownRuleAdd
            || $entity instanceof BreakdownRuleRemaining
            || $entity instanceof BreakdownRuleTotal
            || $entity instanceof TemplatePresentation
            || $entity instanceof TemplateProposal
            || $entity instanceof TemplateMail
            || $entity instanceof TemplateCondition
        ) {
            if (!$entity->getProperty()) {
                $propertyId = $this->session->get('property_id');
                $entityManager = $args->getObjectManager();
                $property = $entityManager->getRepository('App:Property')->find($propertyId);
            } else {
                $property = $entity->getProperty();
            }
            $entity->setProperty($property);
        }
    }
}
