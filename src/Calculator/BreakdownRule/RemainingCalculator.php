<?php

namespace App\Calculator\BreakdownRule;

use App\Entity\BreakdownRuleRemaining;

class RemainingCalculator extends AbstractBreakdownRuleCalculator
{
    public function __construct(BreakdownRuleRemaining $entity)
    {
        $this->entityCapacity = $entity->getRoom()->getCapacity();
        $this->entityPercentage = $entity->getPercentageLeft();
    }

    public function calculate(): void
    {
        $quantity = (int)ceil($this->getRoomAvailability() * ($this->getEntityPercentage() / 100));
        $numberOfBeds = $this->getEntityCapacity() * $quantity;
        $this->roomsToRemove = $quantity;
        $this->bedsToRemove = $numberOfBeds;
    }
}
