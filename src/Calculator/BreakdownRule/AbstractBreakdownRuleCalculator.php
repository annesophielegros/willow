<?php

namespace App\Calculator\BreakdownRule;

abstract class AbstractBreakdownRuleCalculator implements BreakdownRuleCalculatorInterface
{
    /**
     * @var int
     */
    protected $entityQuantity = 0;

    /**
     * @var int
     */
    protected $entityCapacity = 0;

    /**
     * @var int
     */
    protected $entityPercentage = 0;

    /**
     * @var int
     */
    protected $entityQuantityMax = 0;

    /**
     * @var int
     */
    protected $entityRatio = 0;

    /**
     * @var int
     */
    protected $paxNumber = 0;

    /**
     * @var int
     */
    protected $roomAvailability = 0;

    /**
     * @var int
     */
    protected $roomsPickedToAdd = 0;

    /**
     * @var int
     */
    protected $roomsBreakdownToAdd = 0;

    /**
     * @var int
     */
    protected $roomsToRemove = 0;

    /**
     * @var int
     */
    protected $bedsToRemove = 0;

    /**
     * @var int
     */
    protected $paxNumberToDecrement = 0;

    /**
     * @var int
     */
    protected $leftToAssignDecrement = 0;

    /**
     * @return int
     */
    public function getEntityQuantity(): int
    {
        return $this->entityQuantity;
    }

    /**
     * @return int
     */
    public function getEntityCapacity(): int
    {
        return $this->entityCapacity;
    }

    /**
     * @return int
     */
    public function getEntityPercentage(): int
    {
        return $this->entityPercentage;
    }

    /**
     * @return int
     */
    public function getEntityQuantityMax(): int
    {
        return $this->entityQuantityMax;
    }

    /**
     * @return int
     */
    public function getEntityRatio(): int
    {
        return $this->entityRatio;
    }

    /**
     * @param int $entityRatio
     */
    public function setEntityRatio(int $entityRatio): void
    {
        $this->entityRatio = $entityRatio;
    }

    /**
     * @return int
     */
    public function getPaxNumber(): int
    {
        return $this->paxNumber;
    }

    /**
     * @param int $paxNumber
     */
    public function setPaxNumber(int $paxNumber): void
    {
        $this->paxNumber = $paxNumber;
    }

    /**
     * @return int
     */
    public function getRoomAvailability(): int
    {
        return $this->roomAvailability;
    }

    /**
     * @param int $roomAvailability
     */
    public function setRoomAvailability(int $roomAvailability): void
    {
        $this->roomAvailability = $roomAvailability;
    }

    /**
     * @return int
     */
    public function getRoomsPickedToAdd(): int
    {
        return $this->roomsPickedToAdd;
    }

    /**
     * @return int
     */
    public function getRoomsBreakdownToAdd(): int
    {
        return $this->roomsBreakdownToAdd;
    }

    /**
     * @return int
     */
    public function getRoomsToRemove(): int
    {
        return $this->roomsToRemove;
    }

    /**
     * @return int
     */
    public function getBedsToRemove(): int
    {
        return $this->bedsToRemove;
    }

    /**
     * @return int
     */
    public function getPaxNumberToDecrement(): int
    {
        return $this->paxNumberToDecrement;
    }

    /**
     * @return int
     */
    public function getLeftToAssignDecrement(): int
    {
        return $this->leftToAssignDecrement;
    }
}
