<?php

namespace App\Calculator\BreakdownRule;

interface BreakdownRuleCalculatorInterface
{
    public function calculate(): void;
}
