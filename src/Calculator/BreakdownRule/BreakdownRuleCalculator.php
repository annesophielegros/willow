<?php

namespace App\Calculator\BreakdownRule;

use App\Entity\BreakdownRuleAdd;
use App\Entity\BreakdownRuleRemaining;
use App\Entity\BreakdownRuleTotal;
use Doctrine\ORM\EntityManagerInterface;

class BreakdownRuleCalculator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var \App\Entity\Enquiry
     */
    private $enquiry;

    private $occupancy;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function calculate(): void
    {
        $rules = [];

        foreach ($this->findAdd() as $rule) {
            $calculator = new AddCalculator($rule);
            $rules['adds'][$rule->getRoomToAdd()->getLabel()][] = $calculator;
        }

        foreach ($this->findRemain() as $rule) {
            $calculator = new RemainingCalculator($rule);
            $rules['saved'][$rule->getRoom()->getLabel()][] = $calculator;
        }

        foreach ($this->findTotal() as $rule) {
            $calculator = new TotalCalculator($rule);
            $rules['saved'][$rule->getRoom()->getLabel()][] = $calculator;
        }
        $this->rules = $rules;
    }

    public function getAdds(): array
    {
        return $this->rules['adds'] ?? [];
    }

    public function getSaved(): array
    {
        return $this->rules['saved'] ?? [];
    }

    /**
     * @return BreakdownRuleAdd[]
     */
    private function findAdd(): array
    {
        return $this->entityManager->getRepository('App:BreakdownRuleAdd')
            ->findAddRules($this->enquiry, $this->occupancy) ?? [];
    }

    /**
     * @return BreakdownRuleRemaining[]
     */
    private function findRemain(): array
    {
        return $this->entityManager->getRepository('App:BreakdownRuleRemaining')
                ->findRemainingRules($this->enquiry, $this->occupancy) ?? [];
    }

    /**
     * @return BreakdownRuleTotal[]
     */
    private function findTotal(): array
    {
        return $this->entityManager->getRepository('App:BreakdownRuleTotal')
                ->findTotalRules($this->enquiry, $this->occupancy) ?? [];
    }

    public function setEnquiry(\App\Entity\Enquiry $enquiry)
    {
        $this->enquiry = $enquiry;
    }

    public function setOccupancy($occupancy)
    {
        $this->occupancy = $occupancy;
    }
}
