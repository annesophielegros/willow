<?php

namespace App\Calculator\BreakdownRule;

use App\Entity\BreakdownRuleTotal;

class TotalCalculator extends AbstractBreakdownRuleCalculator
{
    public function __construct(BreakdownRuleTotal $entity)
    {
        $this->entityCapacity = $entity->getRoom()->getCapacity();
        $this->entityQuantityMax = $entity->getRoom()->getQuantity();
        $this->entityPercentage = $entity->getPercentageLeft();
    }

    public function calculate(): void
    {
        $quantity = (int)ceil($this->getEntityQuantityMax() * ($this->getEntityPercentage() / 100));
        $numberOfBeds = $this->getEntityCapacity() * $quantity;

        $this->roomsToRemove = $quantity;
        $this->bedsToRemove = $numberOfBeds;
    }
}
