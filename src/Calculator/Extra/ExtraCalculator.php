<?php

namespace App\Calculator\Extra;

use App\Entity\Enquiry;
use App\Entity\ExtraRule;
use App\Entity\OfferExtra;
use App\Model\BreakdownRoom;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

class ExtraCalculator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $result;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Enquiry $enquiry
     * @param BreakdownRoom[] $breakdown
     * @param int $occupancy
     */
    public function calculate(Enquiry $enquiry, array $breakdown, int $occupancy): void
    {
        $extraRules = $this->entityManager->getRepository('App:ExtraRule')->findExtras($enquiry, $occupancy);
        $entities = [];
        foreach ($extraRules as $extraRule) {
            switch ($extraRule->getConsumed()) {
                case '1st night':
                    $startDate = $enquiry->getCheckInDate();
                    $entities[] = $this->createOfferExtra($enquiry, $extraRule, $breakdown, $startDate, $startDate);
                    break;
                case 'every day':
                    $startDate = $enquiry->getCheckInDate();
                    $checkOut = new \DateTime($enquiry->getCheckOutDate()->format('Y-m-d'));
                    $endDate = $checkOut->sub(new DateInterval('P1D'));
                    $entities[] = $this->createOfferExtra($enquiry, $extraRule, $breakdown, $startDate, $endDate);
                    break;
                case 'every next day':
                    $checkIn = new \DateTime($enquiry->getCheckInDate()->format('Y-m-d'));
                    $startDate = $checkIn->add(new DateInterval('P1D'));
                    $endDate = $enquiry->getCheckOutDate();
                    $entities[] = $this->createOfferExtra($enquiry, $extraRule, $breakdown, $startDate, $endDate);
                    break;
            }
        }

        $this->result = $entities;
    }

    private function createOfferExtra(
        Enquiry $enquiry,
        ExtraRule $extraRule,
        array $breakdown,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate
    ): OfferExtra {
        $entity = new OfferExtra();
        $extraPrice = $extraRule->getExtra()->getPrice();
        switch ($extraRule->getDivision()) {
            case 'per pax':
                $entity->setQuantity($enquiry->getNumberOfPersons());
                $entity->setPrice((float)$extraPrice);
                break;
            case 'per room':
                $quantity = 0;
                foreach ($breakdown as $room) {
                    $quantity += $room->getRoomsPicked();
                }
                $entity->setQuantity($quantity);
                $entity->setPrice($extraPrice);
                break;
            case 'per group':
                $entity->setQuantity(1);
                $entity->setPrice($extraPrice);
                break;
        }
        $entity->setStartDate($startDate);
        $entity->setEndDate($endDate);
        $entity->setExtra($extraRule->getExtra());

        return $entity;
    }

    public function result()
    {
        return $this->result;
    }
}
