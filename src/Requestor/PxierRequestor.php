<?php

namespace App\Requestor;

use App\Builder\OptionRequestorBuilder;
use App\Entity\Company;
use App\Entity\Enquiry;
use App\Model\Availability;
use App\Model\Rate;
use App\Requestor\Model\Mews\MewsGetCompaniesPreModel;
use App\Requestor\Model\Pxier\PxierUpdateCustomerModel;

class PxierRequestor extends AbstractRequestor
{
    private const METHOD = 'POST';
    private const ACCESS_TOKEN = 'd00914989fd7c6eb72a1687933df74de.eve.1.7.dev';
    private const URL_ADD_CUSTOMER = 'https://test.api.pxier.com:8443/api/events/updateCustomer';
    private const OPTIONS_ADD_CUSTOMER = [
        'auth_basic' => 'username1:password1',
        'headers' => ['Content-Type' => 'application/json'],
        'body' => '{
                "accessToken": "{access_token}",
                "customerId": 0,
                "customerName": "{customer_name}",
                "address1": "{address1}",
                "address2": "{address2}",
                "zipCode": "",
                "city": "{city}",
                "countryCode": "{country_code}",
                "stateCode": "-",
                "contact": [
                    {
                        "contactId": 0,
                        "firstName": "Group",
                        "lastName": "{group_name}",
                        "phone": "0",
                        "email": "{group_mail}"
                    }
                ]
            }'
    ];
    private const URL_ADD_CONTRACT = 'https://test.api.pxier.com:8443/api/events/updateEvent';
    private const OPTIONS_ADD_CONTRACT = [
        'body' => '
            {
                "accessToken": "d00914989fd7c6eb72a1687933df74de.eve.1.7.dev",
                "contractId": "0",
                "contactId": "1282",
                "customerId": "3956",
                "eventId": "",
                "fromTime": 450,
                "toTime": 650,
                "guestCount": 5,
                "hallId": 80,
                "eventTypeId": 1268,
                "styleId": 0,
                "invoiceAmount": 100,
                "subTotalAmount": 400,
                "hallPrice": 500,
                "eventDate": "12/15/2019",
                "menu": [
                    {
                        "menuId": 0,
                        "menuCategoryId": 0,
                        "menuName": "soecial Menu",
                        "menuPrice": 200,
                        "menuTypeCode": 0,
                        "startTime": 420,
                        "endTime": 480,
                        "quantity": 2,
                        "menuUnitId": 0,
                        "packageId": 0,
                        "taxPercent": 0,
                        "taxPercentCode": 0,
                        "menuSourceId": 480
                    }
                ],
                "service": [
                    {
                        "equipId": 1578,
                        "quantity": 0,
                        "price": 200,
                        "equipDescription": "Divers",
                        "packageId": 0,
                        "taxPercent": 0,
                        "taxPercentCode": 0
                    }
                ]
            }
            ',
        'auth_basic' => ['username1', 'password1'],
    ];
    private const URL_ADD_RESERVATION_OPTIONAL = 'http://test.api.pxier.com/api/event/update';
    private const OPTIONS_ADD_RESERVATION_OPTIONAL = [
        'body' => '
            {
                "AccessToken": "{access_token}",
                "customerId": 0,
                "customerName": "{customer_name}",
                "address1": "{address1}",
                "address2": "{address2}",
                "zipCode": "{zip_code}",
                "city": "{city}",
                "countryCode": "{country_code}",
                "stateCode": "-",
                "langCode": "en",
                "emailSubscribe": "N",
                "customerTypecode": "0",
                "contact": [
                    {
                      "contactId": 0,
                      "firstName": "Group",
                      "lastName": "{group_name}",
                      "phone": "0",
                      "mobile": "",
                      "fax": "",
                      "email": "{group_mail}",
                      "title": ""
                    }
                ]
            }
            ',
        'auth_basic' => ['username1', 'password1'],
    ];
    private const URL_ADD_RESERVATION_CONFIRMED = 'http://test.api.pxier.com/api/event/update';
    private const OPTIONS_ADD_RESERVATION_CONFIRMED = [
        'body' => '
            {
                "AccessToken": "{access_token}",
                "langCode": "en",
                "status": "confirmed"
            }
            ',
    ];


    /**
     * @param Enquiry $enquiry
     * @return Availability[]
     */
    public function getAvailabilities(Enquiry $enquiry): array
    {
        return ['no need'];
    }

    /**
     * @param Enquiry $enquiry
     * @param string $id
     * @return Rate[]
     * @throws \Exception
     */
    public function getRates(Enquiry $enquiry, $rateId): array
    {
        return ['no need'];
    }

    public function getRateList($brandToken, $propertyToken): array
    {
        return ['no need'];
    }

    public function getRoomList($brandToken, $propertyToken): array
    {
        return ['no need'];
    }

    public function getCompanyList(): array
    {
        return ['no need'];
    }

    public function createCompany(Company $company): MewsGetCompaniesPreModel
    {
        return new MewsGetCompaniesPreModel();
    }

    public function updateCompany(Company $company): MewsGetCompaniesPreModel
    {
        return new MewsGetCompaniesPreModel();
    }

    public function createCustomer($enquiry, $company)
    {
        /** @var Company $company */
        $groupMail = $enquiry->getReservationNumber();
        $builder = new OptionRequestorBuilder(self::OPTIONS_ADD_CUSTOMER);
        $builder->replaceBody('access_token', self::ACCESS_TOKEN);
        $builder->replaceBody('customer_name', $company->getLabel());
        $builder->replaceBody('address1', 'testssss');
        $builder->replaceBody('address2', $company->getAddress()->getAddressLine2());
        $builder->replaceBody('city', $company->getAddress()->getCity());
        $builder->replaceBody('country_code', $company->getAddress()->getCountry()->getAbbreviation());
        $builder->replaceBody('group_name', $enquiry->getGroupName());
        $builder->replaceBody('group_mail', $groupMail . '@' . $groupMail . '.com');
        $response = $this->request(
            self::METHOD,
            self::URL_ADD_CUSTOMER,
            $builder->get()
        );
        /** @var PxierUpdateCustomerModel $contact */
        $contact = $this->serializer->deserialize($response, PxierUpdateCustomerModel::class, 'json');
        dd($contact);

        return $contact;
    }

    public function createReservationOptional($enquiry, $status, $optionDate)
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_ADD_RESERVATION_OPTIONAL);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $builder->replace('enq_id', $enquiry->getId());
        $response = $this->request(
            self::METHOD,
            self::URL_ADD_RESERVATION_OPTIONAL,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var PxierUpdateCustomerModel $companies */
        $companies = $this->serializer->deserialize($response, PxierUpdateCustomerModel::class, 'json');

        return $companies;
    }

    public function createReservationConfirmed($enquiry, $status): MewsGetCompaniesPreModel
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_ADD_RESERVATION_CONFIRMED);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $builder->replace('enq_id', $enquiry->getId());
        $response = $this->request(
            self::METHOD,
            self::URL_ADD_RESERVATION_CONFIRMED,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var MewsGetCompaniesPreModel $companies */
        $companies = $this->serializer->deserialize($response, MewsGetCompaniesPreModel::class, 'json');

        return $companies;
    }
}
