<?php

namespace App\Requestor;

use App\Builder\OptionRequestorBuilder;
use App\Entity\Company;
use App\Entity\Enquiry;
use App\Model\Availability;
use App\Model\AvailabilityRoom;
use App\Model\CompanyList;
use App\Model\Rate;
use App\Model\RateList;
use App\Model\RateRoom;
use App\Model\RoomList;
use App\Requestor\Model\Mews\MewsGetAllModel;
use App\Requestor\Model\Mews\MewsGetAllRatesModel;
use App\Requestor\Model\Mews\MewsGetAvailabilityModel;
use App\Requestor\Model\Mews\MewsGetCompaniesPreModel;
use App\Requestor\Model\Mews\MewsGetPricingModel;
use DateInterval;

class MewsRequestor extends AbstractRequestor
{
    private const METHOD = 'POST';
    // TODO check token corresponds to property or brand
    private const CLIENT_TOKEN = 'E0D439EE522F44368DC78E1BFB03710C-D24FB11DBE31D4621C4817E028D9E1D';
    private const ACCESS_TOKEN = 'C66EF7B239D24632943D115EDE9CB810-EA00F8FD8294692C940F6B5A8F9453D';
    private const URL_SERVICES_GET_AVAILABILITY = 'https://api.mews-demo.com/api/connector/v1/services/getAvailability';
    private const OPTIONS_SERVICES_GET_AVAILABILITY = [
        'json' => [
            'ClientToken' => "{client_token}",
            "AccessToken" => "{access_token}",
            "Client" => "Sample Client 1.0.0",
            "ServiceId" => "bd26d8db-86da-4f96-9efc-e5a4654a4a94",
            "StartUtc" => "{start_date}",
            "EndUtc" => "{end_date}",
        ]
    ];
    private const URL_RESSOURCES_GET_ALL = 'https://api.mews-demo.com/api/connector/v1/resources/getAll';
    private const OPTIONS_RESSOURCES_GET_ALL = [
        'json' => [
            'ClientToken' => "{client_token}",
            "AccessToken" => "{access_token}",
            "Client" => "Sample Client 1.0.0",
            "Extent" => [
                    "Resources" => false,
                    "ResourceCategories" => true,
                    "ResourceCategoryAssignments" => false,
                    "ResourceCategoryImageAssignments" => false,
                    "ResourceFeatures" => false,
                    "ResourceFeatureAssignments" => false,
                    "Inactive" => false
            ]
        ]
    ];
    private const URL_RATES_GET_ALL = 'https://api.mews-demo.com/api/connector/v1/rates/getAll';
    private const OPTIONS_RATES_GET_ALL = [
        'json' => [
                "ClientToken" => "{client_token}",
                "AccessToken" => "{access_token}",
                "ServiceIds" => [
                    "bd26d8db-86da-4f96-9efc-e5a4654a4a94"
                ],
                "Extent" => [
                    "Rates" => true,
                    "RateGroups" => false,
                    "RateRestrictions" => false
                ]
        ],
    ];
    private const URL_RATES_GET_PRICING = 'https://api.mews-demo.com/api/connector/v1/rates/getPricing';
    private const OPTIONS_RATES_GET_PRICING = [
        'json' => [
                "ClientToken" => "{client_token}",
                "AccessToken" => "{access_token}",
                "Client" => "Sample Client 1.0.0",
                "RateId" => "{rate_id}",
                "FirstTimeUnitStartUtc" => "{start_date}",
                "LastTimeUnitStartUtc" => "{end_date}"
        ]
    ];
    private const URL_COMPANIES_GET_ALL = 'https://api.mews-demo.com/api/connector/v1/companies/getAll';
    private const OPTIONS_COMPANIES_GET_ALL = [
        'json' => [
                'ClientToken' => "{client_token}",
                "AccessToken" => "{access_token}",
                "Client" => "Sample Client 1.0.0",
                "CreatedUtc" => [
                    "StartUtc" => "2022-08-15T00:00:00Z",
                    "EndUtc" => "2022-11-10T00:00:00Z"
                ]
        ]
    ];
    private const URL_ADD_COMPANY = 'https://api.mews-demo.com/api/connector/v1/companies/add';
    private const OPTIONS_ADD_COMPANY = [
        'json' => [
                "ClientToken" => "{client_token}",
                "AccessToken" => "{access_token}",
                "Client" => "Sample Client 1.0.0",
                "Name" => "{name}",
                "Options" => [
                    "Invoiceable" => false,
                    "AddFeesToInvoices" => false
                ],
                "Address" => [
                    "Line1" => "{line1}",
                    "Line2" => "{line2}",
                    "City" => "{city}",
                    "PostalCode" => "{postal_code}",
                    "CountryCode" => "{country_code}"
                ],
                "Telephone" => "{phone}",
        ]
    ];
    private const URL_UPDATE_COMPANY = 'https://api.mews-demo.com/api/connector/v1/companies/update';
    private const OPTIONS_UPDATE_COMPANY = [
        'json' => [
                "ClientToken" => "{client_token}",
                "AccessToken" => "{access_token}",
                "Client" => "Sample Client 1.0.0",
                "CompanyId" => "{id}",
                "Name" => [
                    "Value" => "{name}"
                ]
        ]
    ];
    private const URL_ADD_RESERVATION = 'https://api.mews-demo.com/api/connector/v1/reservations/add';
    private const OPTIONS_ADD_RESERVATION = [
        'json' => [
                "ClientToken" => "{client_token}",
                "AccessToken" => "{access_token}",
                "Client" => "Sample Client 1.0.0",
                "ServiceId" => "bd26d8db-86da-4f96-9efc-e5a4654a4a94",
                "GroupId" => '{enq_id}',
                "Reservations" => [
                        "State" => "{status}",
                        "StartUtc" => "{check_id}",
                        "EndUtc" => "{check_out}",
                        "ReleasedUtc" => "{option_date}",
                ],
                "Telephone" => "{phone}",
        ]
    ];

    /**
     * @param Enquiry $enquiry
     * @return Availability[]
     */
    public function getAvailabilities(Enquiry $enquiry): array
    {
        $servicesAvailability =  $this->getServicesAvailability($enquiry);
        $resourcesAll = $this->getRessourcesAll();
        $resourceCategories = $resourcesAll->getResourceCategories();
        $resourceCategoriesId = array_map(function ($resource) {
            return $resource->getId();
        }, $resourceCategories);
        $resourceCategoriesLabel = array_map(function ($resource) {
            return $resource->getShortNames()->getenUS();
        }, $resourceCategories);
        $labelByRoomsWithNull = array_combine($resourceCategoriesId, $resourceCategoriesLabel);
        // TODO ** if no Shortname en US replace null with "no name" **
        $labelByRooms = array_map(function ($v) {
            return (is_null($v)) ? "no name" : $v;
        }, $labelByRoomsWithNull);

        $availabilities = [];
        foreach ($servicesAvailability->getDatesUtc() as $dateKey => $dateFormatted) {
            $new = new Availability();
            $new->setDate($dateFormatted);
            foreach ($servicesAvailability->getCategoryAvailabilities() as $categoryAvailability) {
                if (!in_array($categoryAvailability->getCategoryId(), $resourceCategoriesId)) {
                    continue;
                }
                $room = new AvailabilityRoom();
                $room->setAvailability($categoryAvailability->getAvailabilities()[$dateKey]);
                $label = $labelByRooms[$categoryAvailability->getCategoryId()];
                $room->setLabel($label);
                $new->addRoom($room, $label);
            }
            $dateFormattedKey = (new \DateTime($dateFormatted))->format('d/m/Y');
            $availabilities[$dateFormattedKey] = $new;
        }
        return $availabilities;
    }

    public function getRoomList($brandToken, $propertyToken): array
    {
        $resourcesAll = $this->getRessourcesAll();
        $resourceCategories = $resourcesAll->getResourceCategories();
        $roomList = [];
        foreach ($resourceCategories as $resource) {
            $room = new RoomList();
            $room->setId($resource->getId());
            if ($resource->getShortNames()->getEnUS() === null) {
                // todo ** if no Shortname en US replace null with "no name" **
                $room->setName('no name');
            } else {
                $room->setName($resource->getShortNames()->getEnUS());
            }
            $room->setCapacity($resource->getCapacity());
            $roomList[$resource->getId()] = $room;
        }
        return $roomList;
    }

    /**
     * @return array
     */
    public function getCompanyList(): array
    {
        $companiesPre = $this->getAllCompanies();
        $companies = $companiesPre->getCompanies();
        $companiesActive = [];
        foreach ($companies as $company) {
            if (!$company->isActive()) {
                continue;
            }
            $newCompany = new CompanyList();
            $newCompany->setId($company->getId());
            $newCompany->setName($company->getName());
            if (!isset($company->getAddress()['City'])) {
                $newCompany->setCity(null);
            } else {
                $newCompany->setCity($company->getAddress()['City']);
            }
            if (!isset($company->getAddress()['CountryCode'])) {
                $newCompany->setCountryCode(null);
            } else {
                $newCompany->setCountryCode($company->getAddress()['CountryCode']);
            }
            if (!isset($company->getAddress()['Line1'])) {
                $newCompany->setLine1(null);
            } else {
                $newCompany->setLine1($company->getAddress()['Line1']);
            }
            if (!isset($company->getAddress()['Line2'])) {
                $newCompany->setLine2(null);
            } else {
                $newCompany->setLine2($company->getAddress()['Line2']);
            }
            if (!isset($company->getAddress()['PostalCode'])) {
                $newCompany->setPostalCode(null);
            } else {
                $newCompany->setPostalCode($company->getAddress()['PostalCode']);
            }
            $newCompany->setTelephone($company->getTelephone());
            $companiesActive[$company->getId()] = $newCompany;
        }
        return $companiesActive;
    }

    public function getRateList($brandToken, $propertyToken): array
    {
        $ratesAll = $this->getAllRates($brandToken, $propertyToken);
        $rates = $ratesAll->getRates();
        $ratesActive = [];
        foreach ($rates as $rate) {
            if (!$rate->isActive()) {
                continue;
            }
            if (!$rate->isEnabled()) {
                continue;
            }
            $newRate = new RateList();
            $newRate->setId($rate->getId());
            $newRate->setName($rate->getName());
            $ratesActive[$rate->getId()] = $newRate;
        }

        return $ratesActive;
    }

    /**
     * @param Enquiry $enquiry
     * @param string $id
     * @return Rate[]
     * @throws \Exception
     */
    public function getRates(Enquiry $enquiry, $rateId): array
    {
        $rate = $this->entityManager->getRepository('App:RateType')->findOneBy(['id' => $rateId]);
        $rateProviderId = $rate->getProviderId();
        $princing = $this->getPricing($rateProviderId, $enquiry);
        $rates = [];
        foreach ($princing->getDatesUtc() as $dateKey => $date) {
            $rate = new Rate();
            $rate->setDate(new \DateTime($date));
            $rateRooms = [];
            foreach ($princing->getCategoryPrices() as $categoryPriceModel) {
                $rateRoom = new RateRoom();
                $rateRoom->setLabel($categoryPriceModel->getCategoryId());
                $rateRoom->setRate($categoryPriceModel->getPrices()[$dateKey]);
                $rateRooms[$categoryPriceModel->getCategoryId()] = $rateRoom;
            }
            $rate->setRooms($rateRooms);
            $dateFormattedKey = (new \DateTime($date))->format('d/m/Y');
            $rates[$dateFormattedKey] = $rate;
        }

        return $rates;
    }

    private function getServicesAvailability(Enquiry $enquiry): MewsGetAvailabilityModel
    {
        $checkIn = new \DateTime($enquiry->getCheckInDate()->format('Y-m-d'));
        $checkOut = new \DateTime($enquiry->getCheckOutDate()->format('Y-m-d'));
        $start = $checkIn->sub(new DateInterval('P14D'));
        $end = $checkOut->add(new DateInterval('P14D'));
        $builder = new OptionRequestorBuilder(self::OPTIONS_SERVICES_GET_AVAILABILITY);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $builder->replace('start_date', $start->format('c'));
        $builder->replace('end_date', $end->format('c'));
        $response = $this->request(
            self::METHOD,
            self::URL_SERVICES_GET_AVAILABILITY,
            $builder->get()
        );
        /** @var MewsGetAvailabilityModel $bookings */
        $bookings = $this->serializer->deserialize($response, MewsGetAvailabilityModel::class, 'json');

        return $bookings;
    }

    private function getRessourcesAll(): MewsGetAllModel
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_RESSOURCES_GET_ALL);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $response = $this->request(
            self::METHOD,
            self::URL_RESSOURCES_GET_ALL,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);

        return $this->serializer->deserialize($response, MewsGetAllModel::class, 'json');
    }

    private function getPricing($rateProviderId, $enquiry): MewsGetPricingModel
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_RATES_GET_PRICING);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $builder->replace('rate_id', $rateProviderId);
        $builder->rateDateReplace('start_date', $enquiry->getCheckInDate()->format('c'));
        $builder->rateDateReplace('end_date', $enquiry->getCheckOutDate()->format('c'));
        $response = $this->request(
            self::METHOD,
            self::URL_RATES_GET_PRICING,
            $builder->get()
        );
        /** @var MewsGetPricingModel $pricing */
        $pricing = $this->serializer->deserialize($response, MewsGetPricingModel::class, 'json');

        return $pricing;
    }

    private function getAllRates($brandToken, $propertyToken): MewsGetAllRatesModel
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_RATES_GET_ALL);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $response = $this->request(
            self::METHOD,
            self::URL_RATES_GET_ALL,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var MewsGetAllRatesModel $rates */
        $rates = $this->serializer->deserialize($response, MewsGetAllRatesModel::class, 'json');

        return $rates;
    }

    private function getAllCompanies(): MewsGetCompaniesPreModel
    {
        // TODO check with Mews how to get all companies
        $builder = new OptionRequestorBuilder(self::OPTIONS_COMPANIES_GET_ALL);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $response = $this->request(
            self::METHOD,
            self::URL_COMPANIES_GET_ALL,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var MewsGetCompaniesPreModel $companies */
        $companies = $this->serializer->deserialize($response, MewsGetCompaniesPreModel::class, 'json');

        return $companies;
    }

    public function createCompany(Company $company): MewsGetCompaniesPreModel
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_ADD_COMPANY);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $builder->replace('name', $company->getLabel());
        $builder->replace('line1', $company->getAddress()->getAddressLine1());
        $builder->replace('line2', $company->getAddress()->getAddressLine2());
        $builder->replace('city', $company->getAddress()->getCity());
        $builder->replace('postal_code', $company->getAddress()->getPostalCode());
        if ($company->getAddress()->getCountry()) {
            $builder->replace('country_code', $company->getAddress()->getCountry()->getAbbreviation());
        }
        $builder->replace('phone', $company->getAddress()->getPhoneNumber());
        $response = $this->request(
            self::METHOD,
            self::URL_ADD_COMPANY,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var MewsGetCompaniesPreModel $companies */
        $companies = $this->serializer->deserialize($response, MewsGetCompaniesPreModel::class, 'json');

        return $companies;
    }

    public function updateCompany(Company $company): MewsGetCompaniesPreModel
    {
        $builder = new OptionRequestorBuilder(self::OPTIONS_UPDATE_COMPANY);
        $builder->replace('client_token', self::CLIENT_TOKEN);
        $builder->replace('access_token', self::ACCESS_TOKEN);
        $builder->replace('id', $company->getProviderId());
        $builder->replace('name', $company->getLabel());
        $response = $this->request(
            self::METHOD,
            self::URL_UPDATE_COMPANY,
            $builder->get()
        );
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var MewsGetCompaniesPreModel $companies */
        $companies = $this->serializer->deserialize($response, MewsGetCompaniesPreModel::class, 'json');

        return $companies;
    }

    public function createCustomer($enquiry, $company)
    {
        return 'no need';
    }

    public function createReservationOptional($enquiry, $status, $optionDate)
    {
        return 'no need';
    }

    public function createReservationConfirmed($enquiry, $status)
    {
        return 'no need';
    }
}
