<?php

namespace App\Requestor;

use App\Entity\Enquiry;
use App\Entity\RoomType;
use App\Formater\ResultFormater;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractRequestor implements RequestorInterface
{
    public const URL = 'overwrite.willow.fr';
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ResultFormater
     */
    private $resultFormater;

    public function __construct(
        HttpClientInterface $client,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        ResultFormater $resultFormater
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->resultFormater = $resultFormater;
    }

    public function request(string $method, string $url, ?array $options): string
    {
        $response = $this->client->request($method, $url, $options);

        return $response->getContent();
    }

    /**
     * @param Enquiry $enquiry
     * @return RoomType[]
     */
    public function findRoomType(Enquiry $enquiry): array
    {
        $roomType = $this->entityManager->getRepository('App:RoomType')->findOrderByPriority($enquiry);

        return $this->resultFormater->changeKeyByProviderId($roomType);
    }
}
