<?php

namespace App\Requestor;

use App\Entity\Company;
use App\Entity\Enquiry;
use App\Model\Availability;
use App\Model\Rate;
use App\Requestor\Model\Mews\MewsGetCompaniesPreModel;

interface RequestorInterface
{
    /***
     * @param Enquiry $enquiry
     * @return Availability[]
     */
    public function getAvailabilities(Enquiry $enquiry): array;

    /**
     * @param Enquiry $enquiry
     * @param string $id
     * @return Rate[]
     */
    public function getRates(Enquiry $enquiry, string $id): array;

    /**
     * @return array
     */
    public function getRateList($brandToken, $propertyToken): array;

    /**
     * @return array
     */
    public function getRoomList($brandToken, $propertyToken): array;

    /**
     * @return array
     */
    public function getCompanyList(): array;

    /**
     * @param Company $company
     * @return MewsGetCompaniesPreModel
     */
    public function createCompany(Company $company): MewsGetCompaniesPreModel;

    /**
     * @param Company $company
     * @return MewsGetCompaniesPreModel
     */
    public function updateCompany(Company $company): MewsGetCompaniesPreModel;

    public function createCustomer($enquiry, $company);

    public function createReservationConfirmed($enquiry, $status);

    public function createReservationOptional($enquiry, $status, $optionDate);
}
