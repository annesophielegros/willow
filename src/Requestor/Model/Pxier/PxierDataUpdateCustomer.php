<?php

namespace App\Requestor\Model\Pxier;

class PxierDataUpdateCustomer
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $contactId;

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId(string $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getContactId(): string
    {
        return $this->contactId;
    }

    /**
     * @param string $contactId
     */
    public function setContactId(string $contactId): void
    {
        $this->contactId = $contactId;
    }
}