<?php

namespace App\Requestor\Model\Mews;

class MewsGetAllCompaniesModel
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var ?string
     */
    private $motherCompanyId;

    /**
     * @var ?string
     */
    private $invoicingEmail;

    /**
     * @var ?string
     */
    private $websiteUrl;

    /**
     * @var ?string
     */
    private $invoiceDueInterval;

    /**
     * @var ?array
     */
    private $nchClassifications;

    /**
     * @var ?array
     */
    private $options;

    /**
     * @var ?array
     */
    private $creditRating;

    /**
     * @var ?string
     */
    private $department;

    /**
     * @var ?string
     */
    private $dunsNumber;

    /**
     * @var ?string
     */
    private $referenceIdentifier;

    /**
     * @var ?string
     */
    private $accountingCode;

    /**
     * @var ?string
     */
    private $additionalTaxIdentifier;

    /**
     * @var ?string
     */
    private $billingCode;

    /**
     * @var ?string
     */
    private $contact;

    /**
     * @var ?string
     */
    private $contactPerson;

    /**
     * @var ?string
     */
    private $electronicInvoiceIdentifier;

    /**
     * @var ?string
     */
    private $identifier;

    /**
     * @var ?string
     */
    private $iata;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * @var ?string
     */
    private $notes;

    /**
     * @var ?int
     */
    private $number;

    /**
     * @var ?string
     */
    private $taxIdentifier;

    /**
     * @var ?string
     */
    private $telephone;

    /**
     * @var ?string
     */
    private $createdUtc;

    /**
     * @var ?string
     */
    private $updatedUtc;

    /**
     * @var ?array
     */
    private $address;

    /**
     * @var ?string
     */
    private $addressId;

    /**
     * @var ?string
     */
    private $taxIdentificationNumber;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getMotherCompanyId(): ?string
    {
        return $this->motherCompanyId;
    }

    /**
     * @param string|null $motherCompanyId
     */
    public function setMotherCompanyId(?string $motherCompanyId): void
    {
        $this->motherCompanyId = $motherCompanyId;
    }

    /**
     * @return string|null
     */
    public function getInvoicingEmail(): ?string
    {
        return $this->invoicingEmail;
    }

    /**
     * @param string|null $invoicingEmail
     */
    public function setInvoicingEmail(?string $invoicingEmail): void
    {
        $this->invoicingEmail = $invoicingEmail;
    }

    /**
     * @return string|null
     */
    public function getWebsiteUrl(): ?string
    {
        return $this->websiteUrl;
    }

    /**
     * @param string|null $websiteUrl
     */
    public function setWebsiteUrl(?string $websiteUrl): void
    {
        $this->websiteUrl = $websiteUrl;
    }

    /**
     * @return string|null
     */
    public function getInvoiceDueInterval(): ?string
    {
        return $this->invoiceDueInterval;
    }

    /**
     * @param string|null $invoiceDueInterval
     */
    public function setInvoiceDueInterval(?string $invoiceDueInterval): void
    {
        $this->invoiceDueInterval = $invoiceDueInterval;
    }

    /**
     * @return array|null
     */
    public function getNchClassifications(): ?array
    {
        return $this->nchClassifications;
    }

    /**
     * @param array|null $nchClassifications
     */
    public function setNchClassifications(?array $nchClassifications): void
    {
        $this->nchClassifications = $nchClassifications;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     */
    public function setOptions(?array $options): void
    {
        $this->options = $options;
    }

    /**
     * @return array|null
     */
    public function getCreditRating(): ?array
    {
        return $this->creditRating;
    }

    /**
     * @param array|null $creditRating
     */
    public function setCreditRating(?array $creditRating): void
    {
        $this->creditRating = $creditRating;
    }

    /**
     * @return string|null
     */
    public function getDepartment(): ?string
    {
        return $this->department;
    }

    /**
     * @param string|null $department
     */
    public function setDepartment(?string $department): void
    {
        $this->department = $department;
    }

    /**
     * @return string|null
     */
    public function getDunsNumber(): ?string
    {
        return $this->dunsNumber;
    }

    /**
     * @param string|null $dunsNumber
     */
    public function setDunsNumber(?string $dunsNumber): void
    {
        $this->dunsNumber = $dunsNumber;
    }

    /**
     * @return string|null
     */
    public function getReferenceIdentifier(): ?string
    {
        return $this->referenceIdentifier;
    }

    /**
     * @param string|null $referenceIdentifier
     */
    public function setReferenceIdentifier(?string $referenceIdentifier): void
    {
        $this->referenceIdentifier = $referenceIdentifier;
    }

    /**
     * @return string|null
     */
    public function getAccountingCode(): ?string
    {
        return $this->accountingCode;
    }

    /**
     * @param string|null $accountingCode
     */
    public function setAccountingCode(?string $accountingCode): void
    {
        $this->accountingCode = $accountingCode;
    }

    /**
     * @return string|null
     */
    public function getAdditionalTaxIdentifier(): ?string
    {
        return $this->additionalTaxIdentifier;
    }

    /**
     * @param string|null $additionalTaxIdentifier
     */
    public function setAdditionalTaxIdentifier(?string $additionalTaxIdentifier): void
    {
        $this->additionalTaxIdentifier = $additionalTaxIdentifier;
    }

    /**
     * @return string|null
     */
    public function getBillingCode(): ?string
    {
        return $this->billingCode;
    }

    /**
     * @param string|null $billingCode
     */
    public function setBillingCode(?string $billingCode): void
    {
        $this->billingCode = $billingCode;
    }

    /**
     * @return string|null
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @param string|null $contact
     */
    public function setContact(?string $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return string|null
     */
    public function getContactPerson(): ?string
    {
        return $this->contactPerson;
    }

    /**
     * @param string|null $contactPerson
     */
    public function setContactPerson(?string $contactPerson): void
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return string|null
     */
    public function getElectronicInvoiceIdentifier(): ?string
    {
        return $this->electronicInvoiceIdentifier;
    }

    /**
     * @param string|null $electronicInvoiceIdentifier
     */
    public function setElectronicInvoiceIdentifier(?string $electronicInvoiceIdentifier): void
    {
        $this->electronicInvoiceIdentifier = $electronicInvoiceIdentifier;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     */
    public function setIdentifier(?string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string|null
     */
    public function getIata(): ?string
    {
        return $this->iata;
    }

    /**
     * @param string|null $iata
     */
    public function setIata(?string $iata): void
    {
        $this->iata = $iata;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string|null $notes
     */
    public function setNotes(?string $notes): void
    {
        $this->notes = $notes;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int|null $number
     */
    public function setNumber(?int $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string|null
     */
    public function getTaxIdentifier(): ?string
    {
        return $this->taxIdentifier;
    }

    /**
     * @param string|null $taxIdentifier
     */
    public function setTaxIdentifier(?string $taxIdentifier): void
    {
        $this->taxIdentifier = $taxIdentifier;
    }

    /**
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     */
    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string|null
     */
    public function getCreatedUtc(): ?string
    {
        return $this->createdUtc;
    }

    /**
     * @param string|null $createdUtc
     */
    public function setCreatedUtc(?string $createdUtc): void
    {
        $this->createdUtc = $createdUtc;
    }

    /**
     * @return string|null
     */
    public function getUpdatedUtc(): ?string
    {
        return $this->updatedUtc;
    }

    /**
     * @param string|null $updatedUtc
     */
    public function setUpdatedUtc(?string $updatedUtc): void
    {
        $this->updatedUtc = $updatedUtc;
    }

    /**
     * @return array|null
     */
    public function getAddress(): ?array
    {
        return $this->address;
    }

    /**
     * @param array|null $address
     */
    public function setAddress(?array $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getAddressId(): ?string
    {
        return $this->addressId;
    }

    /**
     * @param string|null $addressId
     */
    public function setAddressId(?string $addressId): void
    {
        $this->addressId = $addressId;
    }

    /**
     * @return string|null
     */
    public function getTaxIdentificationNumber(): ?string
    {
        return $this->taxIdentificationNumber;
    }

    /**
     * @param string|null $taxIdentificationNumber
     */
    public function setTaxIdentificationNumber(?string $taxIdentificationNumber): void
    {
        $this->taxIdentificationNumber = $taxIdentificationNumber;
    }
}
