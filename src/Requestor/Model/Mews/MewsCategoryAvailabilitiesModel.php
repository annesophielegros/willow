<?php

namespace App\Requestor\Model\Mews;

class MewsCategoryAvailabilitiesModel
{
    /**
     * @var string
     */
    private $CategoryId;

    /**
     * @var array
     */
    private $availabilities;

    /**
     * @var array
     */
    private $adjustments;

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->CategoryId;
    }

    /**
     * @param string $CategoryId
     */
    public function setCategoryId(string $CategoryId): void
    {
        $this->CategoryId = $CategoryId;
    }

    /**
     * @return array
     */
    public function getAvailabilities(): array
    {
        return $this->availabilities;
    }

    /**
     * @param array $availabilities
     */
    public function setAvailabilities(array $availabilities): void
    {
        $this->availabilities = $availabilities;
    }

    /**
     * @return array
     */
    public function getAdjustments(): array
    {
        return $this->adjustments;
    }

    /**
     * @param array $adjustments
     */
    public function setAdjustments(array $adjustments): void
    {
        $this->adjustments = $adjustments;
    }
}
