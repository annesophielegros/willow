<?php

namespace App\Requestor\Model\Mews;

class MewsResourceCategoriesModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $serviceId;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * @var string
     */
    private $type;

    /**
     * @var MewsStringModel
     */
    private $names;

    /**
     * @var MewsStringModel
     */
    private $shortNames;

    /**
     * @var MewsStringModel
     */
    private $descriptions;

    /***
     * @var int
     */
    private $ordering;

    /**
     * @var int
     */
    private $capacity;

    /**
     * @var int
     */
    private $extraCapacity;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->serviceId;
    }

    /**
     * @param string $serviceId
     */
    public function setServiceId(string $serviceId): void
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return MewsStringModel
     */
    public function getNames(): MewsStringModel
    {
        return $this->names;
    }

    /**
     * @param MewsStringModel $names
     */
    public function setNames(MewsStringModel $names): void
    {
        $this->names = $names;
    }

    /**
     * @return MewsStringModel
     */
    public function getShortNames(): MewsStringModel
    {
        return $this->shortNames;
    }

    /**
     * @param MewsStringModel $shortNames
     */
    public function setShortNames(MewsStringModel $shortNames): void
    {
        $this->shortNames = $shortNames;
    }

    /**
     * @return MewsStringModel
     */
    public function getDescriptions(): MewsStringModel
    {
        return $this->descriptions;
    }

    /**
     * @param MewsStringModel $descriptions
     */
    public function setDescriptions(MewsStringModel $descriptions): void
    {
        $this->descriptions = $descriptions;
    }

    /**
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity(int $capacity): void
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getExtraCapacity(): int
    {
        return $this->extraCapacity;
    }

    /**
     * @param int $extraCapacity
     */
    public function setExtraCapacity(int $extraCapacity): void
    {
        $this->extraCapacity = $extraCapacity;
    }
}
