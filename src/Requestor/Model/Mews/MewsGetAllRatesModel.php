<?php

namespace App\Requestor\Model\Mews;

class MewsGetAllRatesModel
{
    /**
     * @var ?array
     */
    private $rates;

    /**
     * @var ?array
     */
    private $rateGroups;

    /**
     * @var ?array
     */
    private $rateRestrictions;

    /**
     * @return MewsRatesModel[]|null
     */
    public function getRates(): ?array
    {
        return $this->rates;
    }

    /**
     * @param MewsRatesModel[]|null $rates
     */
    public function setRates(?array $rates): void
    {
        $this->rates = $rates;
    }

    /**
     * @param array|null $rateGroups
     */
    public function setRateGroups(?array $rateGroups): void
    {
        $this->rateGroups = $rateGroups;
    }

    /**
     * @return array|null
     */
    public function getRateGroups(): ?array
    {
        return $this->rateGroups;
    }

    /**
     * @param array|null $rateRestrictions
     */
    public function setRateRestrictions(?array $rateRestrictions): void
    {
        $this->rateRestrictions = $rateRestrictions;
    }

    /**
     * @return array|null
     */
    public function getRateRestrictions(): ?array
    {
        return $this->rateRestrictions;
    }
}
