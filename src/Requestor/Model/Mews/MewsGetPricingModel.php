<?php

namespace App\Requestor\Model\Mews;

class MewsGetPricingModel
{
    /**
     * @var string
     */
    private $currency;

    /**
     * @var array
     */
    private $datesUtc;

    /**
     * @var ?array
     */
    private $basePrices;

    /**
     * @var MewsCategoryPriceModel[]
     */
    private $categoryPrices;

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return array
     */
    public function getDatesUtc(): array
    {
        return $this->datesUtc;
    }

    /**
     * @param array $datesUtc
     */
    public function setDatesUtc(array $datesUtc): void
    {
        $this->datesUtc = $datesUtc;
    }

    /**
     * @return array|null
     */
    public function getBasePrices(): ?array
    {
        return $this->basePrices;
    }

    /**
     * @param array|null $basePrices
     */
    public function setBasePrices(?array $basePrices): void
    {
        $this->basePrices = $basePrices;
    }

    /**
     * @return MewsCategoryPriceModel[]
     */
    public function getCategoryPrices(): array
    {
        return $this->categoryPrices;
    }

    /**
     * @param MewsCategoryPriceModel[] $categoryPrices
     */
    public function setCategoryPrices(array $categoryPrices): void
    {
        $this->categoryPrices = $categoryPrices;
    }
}
