<?php

namespace App\Requestor\Model\Mews;

class MewsGetAvailabilityModel
{
    /**
     * @var array
     */
    private $datesUtc;

    /**
     * @var MewsCategoryAvailabilitiesModel[]
     */
    private $categoryAvailabilities;

    /**
     * @return array
     */
    public function getDatesUtc(): array
    {
        return $this->datesUtc;
    }

    /**
     * @param array $datesUtc
     */
    public function setDatesUtc(array $datesUtc): void
    {
        $this->datesUtc = $datesUtc;
    }

    /**
     * @return MewsCategoryAvailabilitiesModel[]
     */
    public function getCategoryAvailabilities(): array
    {
        return $this->categoryAvailabilities;
    }

    /**
     * @param MewsCategoryAvailabilitiesModel[] $categoryAvailabilities
     */
    public function setCategoryAvailabilities(array $categoryAvailabilities): void
    {
        $this->categoryAvailabilities = $categoryAvailabilities;
    }
}
