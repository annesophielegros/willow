<?php

namespace App\Generator;

use App\Entity\Brand;
use Doctrine\ORM\EntityManagerInterface;

class BrandListGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {

        $this->entityManager = $entityManager;
    }

    /**
     * @return Brand[]
     */
    public function list(): array
    {
        return $this->entityManager->getRepository('App:Brand')->findAll();
    }
}
