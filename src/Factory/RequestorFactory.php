<?php

namespace App\Factory;

use App\Entity\Brand;
use App\Requestor\MewsRequestor;
use App\Requestor\MockRequestor;
use App\Requestor\PxierRequestor;
use App\Requestor\RequestorInterface;
use function PHPUnit\Framework\throwException;

class RequestorFactory
{
    /**
     * @var MewsRequestor
     */
    private $mewsRequestor;

    /**
     * @var MockRequestor
     */
    private $mockRequestor;

    /**
     * @var PxierRequestor
     */
    private $pxierRequestor;

    public function __construct(
        MewsRequestor $mewsRequestor,
        MockRequestor $mockRequestor,
        PxierRequestor $pxierRequestor
    ) {
        $this->mewsRequestor = $mewsRequestor;
        $this->mockRequestor = $mockRequestor;
        $this->pxierRequestor = $pxierRequestor;
    }

    public function create(Brand $brand): RequestorInterface
    {
        $provider = $brand->getPmsGetData()->getLabel();
        switch ($provider) {
            case 'Mews':
                $requestor = $this->mewsRequestor;
                break;
            case 'Mock':
                $requestor = $this->mockRequestor;
                break;
            case 'Pxier':
                $requestor = $this->pxierRequestor;
                break;
            default:
                throw new \Exception("No API was set up");
        }

        return $requestor;
    }

    public function createSendToPms(Brand $brand): RequestorInterface
    {
        $provider = $brand->getPmsPostData()->getLabel();
        switch ($provider) {
            case 'Mews':
                $requestor = $this->mewsRequestor;
                break;
            case 'Mock':
                $requestor = $this->mockRequestor;
                break;
            case 'Pxier':
                $requestor = $this->pxierRequestor;
                break;
            default:
                throw new \Exception("No API was set up");
        }

        return $requestor;
    }
}
