<?php

namespace App\Factory;

use App\Factory\Template\AbstractProposalTemplate;
use App\Factory\Template\TableExtra;
use App\Factory\Template\TableRoom;
use App\Factory\Template\TotalProposal;

class ProposalTemplateFactory
{
    private TableRoom $tableRoom;
    private TableExtra $tableExtra;
    private TotalProposal $totalProposal;

    public function __construct(
        TableRoom $tableRoom,
        TableExtra $tableExtra,
        TotalProposal $totalProposal
    ) {
        $this->tableRoom = $tableRoom;
        $this->tableExtra = $tableExtra;
        $this->totalProposal = $totalProposal;
    }

    public function make($param): AbstractProposalTemplate
    {
        switch ($param) {
            case 'table_room':
                $template = $this->tableRoom;
                break;
            case 'table_extra':
                $template = $this->tableExtra;
                break;
            case 'total_proposal':
                $template = $this->totalProposal;
                break;
            default:
                throw new \Exception('template not found');
        }

        return $template;
    }
}