<?php

namespace App\Form;

use App\Entity\TemplatePresentation;
use App\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplatePresentationType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $propertyId = $this->session->get('property_id');
        $property = $this->entityManager->getRepository('App:Property')->find($propertyId);

        $builder
            ->add('label', TextType::class, [
                'label' => 'Label *',
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Presentation',
                'required' => false,
            ])
            ->add('features', TextareaType::class, [
                'label' => 'Features',
                'required' => false,
            ])
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choices' => $property->getLanguage(),
                'choice_label' => 'label',
                'label' => 'Language *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TemplatePresentation::class,
        ]);
    }
}
