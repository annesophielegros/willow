<?php

namespace App\Form;

use App\Entity\Age;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class, [
                'label' => 'Label *',
            ])
            ->add('min', NumberType::class, [
                'html5' => true,
                'label' => 'Minimum',
                'required' => false,
            ])
            ->add('max', NumberType::class, [
                'html5' => true,
                'label' => 'Maximum',
                'required' => false,
            ])
            ->add('formCode', TextType::class, [
                'label' => 'Online Form code',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Age::class,
        ]);
    }
}
