<?php

namespace App\Form;

use App\Entity\Currency;
use App\Entity\Language;
use App\Entity\Property;
use App\Entity\RateDisplay;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add('apiKey', TextareaType::class, [
                'label' => 'token/key API',
                'required' => false
            ])
            ->add('rate_display', EntityType::class, [
                'class' => RateDisplay::class,
                'choice_label' =>  'label',
                'label' => 'Rate Display'
            ])
            ->add('emptyBedsCharged', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
                'label' => 'Empty beds charged per default'
            ])
            ->add('followupDays', NumberType::class, [
                'html5' => true,
                'label' => 'Number of days after creating an enquiry to create an automatic follow up',
                'required' => false
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'choice_label' => 'label',
                'placeholder' => 'Choose a currency',
                'label' => 'Currency *',
            ])
            ->add('address', AddressType::class, [
                'label' => false,
            ])
            ->add('preference_email', PreferenceEmailType::class, [
                'label' => false,
            ])
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choice_label' => 'label',
                'multiple' => true,
                'expanded' => false,
                'label' => 'Language *',
            ])
            ->add('defaultLanguage', EntityType::class, [
                'class' => Language::class,
                'choice_label' => 'label',
                'label' => 'Default Language *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
        ]);
    }
}
