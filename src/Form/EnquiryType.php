<?php

namespace App\Form;

use App\Entity\Age;
use App\Entity\CancelReason;
use App\Entity\Company;
use App\Entity\RateDisplay;
use App\Entity\TemplateCondition;
use App\Entity\Country;
use App\Entity\Enquiry;
use App\Entity\Language;
use App\Entity\TemplateProposal;
use App\Entity\RateType;
use App\Entity\Status;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class EnquiryType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        Security $security,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $propertyId = $this->session->get('property_id');
        $property = $this->entityManager->getRepository('App:Property')->find($propertyId);

        $brandId = null;
        if (null !== $user->getBrand()) {
            $brandId = $user->getBrand()->getId();
        }
        if (null !== $this->session->get('brand')) {
            $brandId = $this->session->get('brand')->getId();
        }

        $builder
            ->add('group_name', TextareaType::class, [
                'required' => true,
                'label' => 'Group Name *'
            ])
            ->add('contact_first_name', TextareaType::class, [
                'required' => false,
            ])
            ->add('contact_last_name', TextareaType::class, [
                'required' => false,
            ])
            ->add('phone_number', TextareaType::class, [
                'required' => false,
            ])
            ->add('client_comment', TextareaType::class, [
                'required' => false,
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('c')
                        ->where('c.brand = :brand')
                        ->setParameter('brand', $brandId)
                        ->orderBy('c.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Company / Agency name',
                'placeholder' => '',
                'required' => false,
            ])
            ->add('hotel_comment', TextareaType::class, [
                'required' => false,
            ])
            ->add('email_address', TextareaType::class, [
                'required' => true,
                'label' => 'Email Address *',
            ])
            ->add('check_in_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Check In Date *',
            ])
            ->add('check_out_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Check Out Date *',
            ])
            ->add('number_of_persons', NumberType::class, [
                'required' => true,
                'label' => 'Number of persons *',
            ])
            ->add('age', EntityType::class, [
                'class' => Age::class,
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.brand = :brand')
                        ->setParameter('brand', $brandId);
                },
                'choice_label' => 'label',
                'label' => 'Age range',
                'placeholder' => '',
                'required' => false,
            ])
            ->add('status', EntityType::class, [
                'class' => Status::class,
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.brand = :brand')
                        ->setParameter('brand', $brandId);
                },
                'choice_label' => 'label',
                'label' => 'Status *',
            ])
            ->add('ratetype', EntityType::class, [
                'class' => RateType::class,
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('rt')
                        ->where('rt.property = :property')
                        ->setParameter('property', $propertyId)
                        ->andWhere('rt.activated = 1')
                        ->orderBy('rt.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Rate Type of the Enquiry when you send it to your PMS',
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'label',
                'label' => 'Nationality',
                'placeholder' => '',
                'required' => false,
            ])
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choices' => $property->getLanguage(),
                'choice_label' => 'label',
            ])
            ->add('templateCondition', EntityType::class, [
                'class' => TemplateCondition::class,
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('c')
                        ->where('c.property = :property')
                        ->setParameter('property', $propertyId);
                },
                'choice_label' => 'label',
                'placeholder' => '',
                'required' => false,
            ])
            ->add('customConditions', TextareaType::class, [
                'label' => 'Custom TemplateCondition',
                'required' => false,
            ])
            ->add('templateProposal', EntityType::class, [
                'class' => TemplateProposal::class,
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.property = :property')
                        ->setParameter('property', $propertyId);
                },
                'choice_label' => 'label',
                'placeholder' => '',
                'required' => false,
            ])
            ->add('cancelReason', EntityType::class, [
                'class' => CancelReason::class,
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.brand = :brand')
                        ->setParameter('brand', $brandId);
                },
                'choice_label' => 'label',
                'required' => false,
            ])
            ->add('rate_display', EntityType::class, [
                'class' => RateDisplay::class,
                'choice_label' =>  'label',
                'label' => 'Display of the rate'
            ])
            ->add('emptyBedsCharged', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
                'label' => 'Empty beds charged per default'
            ])
            ->add('breakdownEnquiries', CollectionType::class, [
                'entry_type' => BreakdownEnquiryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Enquiry::class,
        ]);
    }
}
