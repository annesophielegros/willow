<?php

namespace App\Form;

use App\Entity\OfferRoom;
use App\Entity\RoomType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferRoomType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = $this->session->get('property_id');
        $builder
            ->add('roomQuantity', NumberType::class, [
                'attr' => ['style' => 'text-align:center'],
                'label' => 'Room Quantity *',
            ])
            ->add('roomType', EntityType::class, [
                'class' => RoomType::class,
                'choice_label' => 'label',
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('rt')
                        ->where('rt.property = :property')
                        ->setParameter('property', $propertyId)
                        ->andWhere('rt.activated = true')
                        ->orderBy('rt.label', 'ASC');
                },
                'label' => 'Room Type *',
            ])
            ->add('checkInDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'check-in-date-field'],
                'label' => 'Check In Date *',
            ])
            ->add('checkOutDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'check-out-date-field'],
                'label' => 'Check Out Date *',
            ])
            ->add('personsInTheRoom', NumberType::class, [
                'label' => 'Persons *',
                'attr' => ['style' => 'text-align:center'],
            ])
            ->add('average', NumberType::class, [
                'label' => 'Average *',
                'mapped' => false,
                'required' => true,
                'attr' => ['class' => 'average-field', 'style' => 'text-align:center'],
            ])
            ->add('rates', CollectionType::class, [
                'entry_type' => OfferRoomRateType::class,
                'entry_options' => [
                    'attr' => ['class' => 'row'],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OfferRoom::class,
        ]);
    }
}
