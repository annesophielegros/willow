<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class CompanyNewType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session,
        Security $security
    ) {
        $this->session = $session;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $brandId = null;
        if (null !== $user->getBrand()) {
            $brandId = $user->getBrand()->getId();
        }
        if (null !== $this->session->get('brand')) {
            $brandId = $this->session->get('brand')->getId();
        }
        $builder
            ->add('label', TextType::class, [
                'label' => 'Company Name *',
            ])
            ->add('address', AddressType::class, [
                'label' => false,
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.brand = :brand')
                        ->setParameter('brand', $brandId)
                        ->orderBy('p.firstname', 'ASC');
                },
                'choice_label' => function ($category) {
                    return $category->getFirstName() . ' ' . $category->getLastName();
                },
                'placeholder' => '',
                'label' => 'Person in charge of the account',
                'required' => false,
                'disabled' => true,
            ])
            ->add('additional_info', TextType::class, [
                'label' => 'Additional Information',
                'disabled' => true,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save Company',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
