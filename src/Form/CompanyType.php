<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class CompanyType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session,
        Security $security
    ) {
        $this->session = $session;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $brandId = null;
        if (null !== $user->getBrand()) {
            $brandId = $user->getBrand()->getId();
        }
        if (null !== $this->session->get('brand')) {
            $brandId = $this->session->get('brand')->getId();
        }
        $builder
            ->add('label', TextType::class, [
                'label' => 'Company Name *',
            ])
            ->add('User', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.brand = :brand')
                        ->setParameter('brand', $brandId)
                        ->orderBy('p.firstname', 'ASC');
                },
                'choice_label' => function ($category) {
                    return $category->getFirstName() . ' ' . $category->getLastName();
                },
                'placeholder' => '',
                'label' => 'Person in charge of the account',
                'required' => false,
            ])
            ->add('address', AddressType::class, [
                'label' => false,
                'required' => false,
            ])
            ->add('additional_info', TextType::class, [
                'label' => 'Additional Information',
                'required' => false,
            ])
            ->add('defaultSelection', ChoiceType::class, [
                'choices'  => [
                    'No' => false,
                    'Yes' => true,
                ],
                'label' => 'Default selection when sending enquiry to PMS',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
