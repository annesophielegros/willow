<?php

namespace App\Form;

use App\Entity\RateRule;
use App\Entity\RateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DefaultRateRuleType extends AbstractType
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = $this->session->get('property_id');
        $builder
            ->add('defaultSelection', HiddenType::class, [
                'data' => 1
            ])
            ->add('priority', HiddenType::class, [
                'data' => 4
            ])
            ->add('active', HiddenType::class, [
                'data' => 1
            ])
            ->add('base_rate_variable', NumberType::class, [
                'html5' => true,
                'label' => 'Percentage of the rate code applicable',
                'required' => false
            ])
            ->add('base_rate_fix', NumberType::class, [
                'html5' => true,
                'label' => 'Fix amount added or discounted to the rate code',
                'required' => false
            ])
            ->add('rate_type', EntityType::class, [
                'class' => RateType::class,
                'label' => 'Rate code *',
                'choice_label' => 'label',
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('rt')
                        ->where('rt.property = :property')
                        ->setParameter('property', $propertyId)
                        ->orderBy('rt.label', 'ASC');
                },
                'placeholder' => 'Choose a rate type'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RateRule::class,
        ]);
    }
}
