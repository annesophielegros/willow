<?php

namespace App\Form;

use App\Entity\BreakdownPreferences;
use App\Entity\RoomType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class RoomTypeType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = $this->session->get('property_id');
        $property = $this->entityManager->getRepository('App:Property')->find($propertyId);

        $builder
            ->add('groupOffers')
            ->add('priority')
            ->add('breakdownType', EntityType::class, [
                'class' => BreakdownPreferences::class,
                'choices' => $property->getBreakdownTypes(),
                'choice_label' => 'label',
                'label' => 'Breakdown Type when generating automatic offer',
                'placeholder' => '',
                'multiple' => true,
                'expanded' => false,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RoomType::class,
        ]);
    }
}
