<?php

namespace App\Form;

use App\Entity\Enquiry;
use App\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class SimulateType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        Security $security,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $brand = $user->getBrand();
        if (!$brand) {
            $brandId = $this->session->get('brand')->getId();
        } else {
            $brandId = $brand->getId();
        }
        if ($this->security->isGranted('ROLE_ADMIN_BRAND')) {
            $propertyList = $this->entityManager->getRepository('App:Property')->findAllByBrand($brandId);
        } else {
            $propertyList = $user->getProperties();
        }
        $builder
            ->add('check_in_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Check In Date *',
            ])
            ->add('check_out_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Check Out Date *',
            ])
            ->add('number_of_persons', NumberType::class, [
                'label' => 'Number of Persons *'
            ])
            ->add('property', EntityType::class, [
                'class' => Property::class,
                'choices' => $propertyList,
                'choice_label' => 'label',
                'placeholder' => 'Choose a property',
                'label' => 'Property *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Enquiry::class,
        ]);
    }
}
