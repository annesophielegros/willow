<?php

namespace App\Form;

use App\Entity\PreferenceEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreferenceEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sender_enquiry', TextType::class, [
                'label' => 'Mail from where enquiries are sent *',
            ])
            ->add('receiver_enquiry_filtered', TextType::class, [
                'label' => 'Mail to receive the requests that cannot be quoted automatically *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PreferenceEmail::class,
        ]);
    }
}
