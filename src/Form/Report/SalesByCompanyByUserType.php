<?php

namespace App\Form\Report;

use App\Entity\Property;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class SalesByCompanyByUserType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        Security $security,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $brand = $user->getBrand();
        if (!$brand) {
            $brandId = $this->session->get('brand')->getId();
        } else {
            $brandId = $brand->getId();
        }
        $companiesWithUser = $this->entityManager->getRepository('App:Company')->findByBrandWithUser($brandId);
        $salesManager = [];
        foreach ($companiesWithUser as $company) {
            $manager = $company->getUser();
            if (array_key_exists($manager->getId(), $salesManager)) {
                continue;
            }
            $salesManager[$manager->getId()] = $manager;
        }
        if ($this->security->isGranted('ROLE_ADMIN_BRAND')) {
            $propertyList = $this->entityManager->getRepository('App:Property')->findAllByBrand($brandId);
        } else {
            $propertyList = $user->getProperties();
        }
        $builder
            ->add('account_manager', EntityType::class, [
                'class' => User::class,
                'choices' => $salesManager,
                'choice_label' => function ($category) {
                    return $category->getFirstName() . ' ' . $category->getLastName();
                },
                'placeholder' => 'All',
                'label' => 'Account Manager *',
                'required' => false,
            ])
            ->add('property', EntityType::class, [
                'class' => Property::class,
                'choices' => $propertyList,
                'choice_label' => 'label',
                'placeholder' => 'Choose one',
                'label' => 'Property *',
            ])
            ->add('created_from', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Res created from *',
            ])
            ->add('created_until', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Res created until *',
                'data' => new \DateTime(),
            ])
            ->add('from', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Res starting from *',
                'data' => new \DateTime(),
            ])
            ->add('until', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Res starting until *',
            ])
            ->add('generate_report', SubmitType::class, [
                'label' => 'Generate Report',
            ])
            ->add('export_excel', SubmitType::class, [
                'label' => 'Export to Excel',
            ])
        ;
    }
}
