<?php

namespace App\Form;

use App\Entity\BreakdownPreferences;
use App\Entity\Currency;
use App\Entity\Language;
use App\Entity\Property;
use App\Entity\RateDisplay;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class PreferenceType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Security
     */
    private $security;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager,
        Security $security
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = $this->session->get('property_id');
        $property = $this->entityManager->getRepository('App:Property')->find($propertyId);
        $brandId = null;
        if (null !== $this->security->getUser()->getBrand()) {
            $brandId = $this->security->getUser()->getBrand()->getId();
        }
        if (null !== $this->session->get('brand')) {
            $brandId = $this->session->get('brand')->getId();
        }

        $builder
            ->add('label', TextType::class, [
                'label' => 'Label *',
            ])
            ->add('apiKey', TextType::class, [
                'label' => 'token/key API',
                'required' => false,
            ])
            ->add('rate_display', EntityType::class, [
                'class' => RateDisplay::class,
                'choice_label' =>  'label',
                'label' => 'Rate Display *'
            ])
            ->add('emptyBedsCharged', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
                'label' => 'Empty beds charged per default *'
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'choice_label' => 'label',
                'placeholder' => 'Choose a currency',
                'label' => 'Currency *',
            ])
            ->add('address', AddressType::class, [
                'label' => false,
            ])
            ->add('preference_email', PreferenceEmailType::class, [
                'label' => false,
            ])
            ->add('defaultLanguage', EntityType::class, [
                'class' => Language::class,
                'choices' => $property->getLanguage(),
                'choice_label' => 'label',
                'label' => 'Default Language *',
            ])
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choice_label' => 'label',
                'multiple' => true,
                'expanded' => false,
                'label' => 'Language *',
            ])
            ->add('breakdownTypes', EntityType::class, [
                'class' => BreakdownPreferences::class,
                'query_builder' => function (EntityRepository $er) use ($brandId) {
                    return $er->createQueryBuilder('b')
                        ->where('b.brand = :brand')
                        ->setParameter('brand', $brandId);
                },
                'choice_label' => 'label',
                'label' => 'Breakdown Type that can be used when generating automatic offer',
                'placeholder' => '',
                'multiple' => true,
                'expanded' => false,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
        ]);
    }
}
