<?php

namespace App\Form;

use App\Entity\Age;
use App\Entity\BreakdownRuleTotal;
use App\Entity\RoomType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class BreakdownRuleTotalType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session,
        Security $security
    ) {
        $this->security = $security;
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $propertyId = $this->session->get('property_id');
        $builder
            ->add('active')
            ->add('label', TextAreaType::class, [
                'label' => 'Rule name *'
            ])
            ->add('description', TextAreaType::class, [
                'required' => false,
            ])
            ->add('pax_min', NumberType::class, [
                'html5' => true,
                'label' => 'Minimum number of persons',
                'required' => false
            ])
            ->add('pax_max', NumberType::class, [
                'html5' => true,
                'label' => 'Maximum number of persons',
                'required' => false
            ])
            ->add('length_min', NumberType::class, [
                'html5' => true,
                'label' => 'Minimum length of stay',
                'required' => false
            ])
            ->add('length_max', NumberType::class, [
                'html5' => true,
                'label' => 'Maximum length of stay',
                'required' => false
            ])
            ->add('occupancyMin', NumberType::class, [
                'label' => 'Minimum occupancy to apply the rule',
                'html5' => true,
                'required' => false
            ])
            ->add('occupancyMax', NumberType::class, [
                'label' => 'Maximum occupancy to apply the rule',
                'html5' => true,
                'required' => false
            ])
            ->add('ageMin', EntityType::class, [
                'class' => Age::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('a')
                        ->where('a.brand = :brand')
                        ->setParameter('brand', $user->getBrand()->getId())
                        ->orderBy('a.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Minimum age range to apply the rule',
                'required' => false,
                'choice_attr' => function ($choices, $key, $value) {
                    return ['data-min' => $choices->getMin()];
                },
            ])
            ->add('ageMax', EntityType::class, [
                'class' => Age::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('a')
                        ->where('a.brand = :brand')
                        ->setParameter('brand', $user->getBrand()->getId())
                        ->orderBy('a.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Maximum age range to apply the rule',
                'required' => false,
                'choice_attr' => function ($choices, $key, $value) {
                    return ['data-min' => $choices->getMin()];
                },
            ])
            ->add('date_start', DateType::class, [
                'label' => 'Date from',
                'widget' => 'single_text',
                'required' => false
            ])
            ->add('date_end', DateType::class, [
                'label' => 'Date until',
                'widget' => 'single_text',
                'required' => false
            ])
            ->add('daySelection', ChoiceType::class, [
                'choices'  => [
                    'Monday' => 'Monday',
                    'Tuesday' => 'Tuesday',
                    'Wednesday' => 'Wednesday',
                    'Thursday' => 'Thursday',
                    'Friday' => 'Friday',
                    'Saturday' => 'Saturday',
                    'Sunday' => 'Sunday',
                ],
                'multiple' => true,
                'required' => false
            ])
            ->add('percentage_left', NumberType::class, [
                'html5' => true,
                'label' => 'Max percentage of the total rooms usable *'
            ])
            ->add('room', EntityType::class, [
                'class' => RoomType::class,
                'label' => 'Room affected by the rule *',
                'choice_label' => 'label',
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('rt')
                        ->where('rt.property = :property')
                        ->setParameter('property', $propertyId)
                        ->andWhere('rt.activated = true')
                        ->andWhere('rt.groupOffers = true')
                        ->orderBy('rt.label', 'ASC');
                },
                'placeholder' => 'Choose a room'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BreakdownRuleTotal::class,
        ]);
    }
}
