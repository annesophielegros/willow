<?php

namespace App\Form;

use App\Entity\BreakdownEnquiry;
use App\Entity\BreakdownPreferences;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BreakdownEnquiryType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('breakdownPref', EntityType::class, [
                'class' => BreakdownPreferences::class,
                'label' => 'Room Type',
                'choice_label' => 'label',
                'disabled' => true,
            ])
            ->add('persons', NumberType::class, [
                'label' => 'Persons',
                'attr' => ['style' => 'text-align:center'],
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BreakdownEnquiry::class,
        ]);
    }
}
