<?php

namespace App\Tests\Func\Login;

use App\Repository\PropertyRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

trait LoginTrait
{
    /**
     * @return \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    public function logIn(): KernelBrowser
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin1@admin.fr');
        $client->loginUser($testUser);
        $brandId = $testUser->getBrand()->getId();
        $properties = static::$container->get(PropertyRepository::class)->findAllByBrand($brandId);
        $container = static::$kernel->getContainer();
        $session = $container->get('session');

        $session->set('property_id', $properties[0]->getId());
        $session->save();

        return $client;
    }
}
