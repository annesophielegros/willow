<?php

namespace App\Tests\Func\Controller;

use App\Entity\RateType;
use App\Repository\AgeRepository;
use App\Repository\BrandRepository;
use App\Repository\EnquiryRepository;
use App\Repository\RateTypeRepository;
use App\Repository\StatusRepository;
use App\Tests\Func\Login\LoginTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EnquiryControllerTest extends WebTestCase
{
    use LoginTrait;

    private KernelBrowser $client;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->client = $this->logIn();
    }

    /**
     * @return void
     */
    public function testNew(): void
    {
        $crawlerNew = $this->client->request('GET', '/enquiry/new');
        $this->assertResponseIsSuccessful();
        $this->assertSame('Reservation summary', $crawlerNew->filter('h4')->eq(0)->text());
        $nodeButton = $crawlerNew->selectButton('Create');
        $form = $nodeButton->form();
        $statusRepository = static::$container->get(StatusRepository::class);
        $brandRepository = static::$container->get(BrandRepository::class);
        $brand = $brandRepository->findOneBy([], ['id' => 'asc']);
        $status = $statusRepository->findDefaultStatusByBrand($brand->getId());
        $form['enquiry[status]'] = $status->getId();
        $date = new \DateTime();
        $dateCheckin = clone $date;
        $dateCheckin = $dateCheckin->modify('+1 day');
        $dateCheckout = clone $date;
        $dateCheckout = $dateCheckout->modify('+4 day');
        $form['enquiry[check_in_date]'] = $dateCheckin->format('Y-m-d');
        $form['enquiry[check_out_date]'] = $dateCheckout->format('Y-m-d');
        $form['enquiry[number_of_persons]'] = '33';
        $ageRepository = static::$container->get(AgeRepository::class);
        $ages = $ageRepository->findByBrand($brand->getId());
        $form['enquiry[age]'] = $ages[0]->getId();
        $form['enquiry[client_comment]'] = 'admin';
        $form['enquiry[hotel_comment]'] = 'admin';
        $form['enquiry[groupe_name]'] = 'admin';
        $form['enquiry[company]'] = '';
        $form['enquiry[contact_first_name]'] = 'admin';
        $form['enquiry[contact_last_name]'] = 'admin';
        $form['enquiry[email_address]'] = 'admin';
        $form['enquiry[phone_number]'] = 'admin';
        $form['enquiry[ratetype]'] = $brand->getProperties()[0]->getRateTypes()[0]->getId();
        $this->client->submit($form);

        $crawlerNewEnquiry = $this->client->followRedirect();
        $this->assertSame('Reservation summary', $crawlerNewEnquiry->filter('h4')->eq(0)->text());
        $this->assertSame('33', $crawlerNewEnquiry->filter('table')->first()->filter('td')->eq(7)->text());
    }

    /**
     * @return void
     */
    public function testEdit(): void
    {
        $enquiryRepository = static::$container->get(EnquiryRepository::class);
        $lastEnquiry = $enquiryRepository->findOneBy([], ['id' => 'desc']);
        $crawlerPage = $this->client->request('GET', "/enquiry/{$lastEnquiry->getId()}");
        $this->assertSame('Reservation summary', $crawlerPage->filter('h4')->eq(0)->text());
        $nodeButton = $crawlerPage->filter('#enquiry_informations')->selectButton('Update');
        $form = $nodeButton->form();
        $form['enquiry[number_of_persons]'] = 66;
        $this->client->submit($form);

        $crawlerEdited = $this->client->followRedirect();

        $this->assertSame('Reservation summary', $crawlerEdited->filter('h4')->eq(0)->text());
        $this->assertSame('66', $crawlerEdited->filter('table')->first()->filter('td')->eq(7)->text());
    }
}
