<?php

namespace App\Tests\Func\Controller;


use App\Repository\AgeRepository;
use App\Repository\BrandRepository;
use App\Repository\BreakdownRuleAddRepository;
use App\Repository\PropertyRepository;
use App\Repository\RoomTypeRepository;
use App\Tests\Func\Login\LoginTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BreakdownRulesAddControllerTest extends WebTestCase
{
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = $this->logIn();
    }
    
    public function testIndex()
    {
        $crawlerPage = $this->client->request('GET', '/revenue-configuration');

        $this->assertSame('Revenue configuration', $crawlerPage->filter('h3')->text());
//        $this->assertSame('no records found', $crawlerPage->filter('td')->eq(0)->text());
    }

//    public function testNew()
//    {
//        $crawlerNew = $this->client->request('GET', '/breakdown/rule/add/new');
//        $this->assertSame('Create new Breakdown Rule to add rooms', $crawlerNew->filter('h1')->text());
//        $nodeButton = $crawlerNew->selectButton('Save');
//        $form = $nodeButton->form();
//        $form['breakdown_rule_add[active]'] = '1';
//        $form['breakdown_rule_add[label]'] = '10 pax 1 twin';
//        $form['breakdown_rule_add[description]'] = 'add a twin every 10 persons';
//        $form['breakdown_rule_add[pax_min]'] = '2';
//        $form['breakdown_rule_add[pax_max]'] = '100';
//        $form['breakdown_rule_add[ratio]'] = '10';
//        $form['breakdown_rule_add[length_min]'] = '3';
//        $form['breakdown_rule_add[length_max]'] = '40';
//        $form['breakdown_rule_add[occupancyMin]'] = '40';
//        $form['breakdown_rule_add[occupancyMax]'] = '100';
//        $brandRepository = static::$container->get(BrandRepository::class);
//        $brand = $brandRepository->findOneBy([], ['id' => 'asc']);
//        $ageRepository = static::$container->get(AgeRepository::class);
//        $ages = $ageRepository->findByBrand($brand->getId());
//        $form['breakdown_rule_add[ageMin]'] = $ages[0]->getId();
//        $form['breakdown_rule_add[ageMax]'] = $ages[1]->getId();
//        $date = new \DateTime();
//        $dateMin = clone $date;
//        $dateMin = $dateMin->modify('+1 day');
//        $form['breakdown_rule_add[date_start]'] = $dateMin->format('Y-m-d');
//        $dateMax = clone $date;
//        $dateMax = $dateMax->modify('+4 day');
//        $form['breakdown_rule_add[date_end]'] = $dateMax->format('Y-m-d');
//        $propertyRepository = static::$container->get(PropertyRepository::class);
//        $property = $propertyRepository->findOneBy([], ['id' => 'asc']);
//        $roomsRepository = static::$container->get(RoomTypeRepository::class);
//        $rooms = $roomsRepository->findAllByProperty($property->getId());
//        $form['breakdown_rule_add[room_to_add]'] = $rooms[1]->getId();
//        $form['breakdown_rule_add[quantity]'] = '1';
//        $this->client->submit($form);
//        $crawlerFindNew = $this->client->followRedirect();
//        $this->assertSame('Revenue configuration', $crawlerFindNew->filter('h3')->text());
//        $this->assertSame('10 pax 1 twin', $crawlerFindNew->filter('#breakdown_rules_add tr')->last()->filter('td')->eq(1)->text());
//        $this->assertSame('2', $crawlerFindNew->filter('#breakdown_rules_add tr')->last()->filter('td')->eq(2)->text());
//        $this->assertSame('14BR', $crawlerFindNew->filter('#breakdown_rules_add tr')->last()->filter('td')->eq(13)->text());
//    }

//    public function testEdit()
//    {
//        $breakdownRulesAddRepository = static::$container->get(BreakdownRuleAddRepository::class);
//        $lastBreakdownRulesAdd = $breakdownRulesAddRepository->findOneBy([], ['id' => 'desc']);
//
//        $crawlerPage = $this->client->request('GET', "/breakdown/rule/add/{$lastBreakdownRulesAdd->getId()}/edit");
//        $this->assertSame('Edit Breakdown Rule to add rooms', $crawlerPage->filter('h1')->text());
//        $nodeButton = $crawlerPage->selectButton('Update');
//        $form = $nodeButton->form();
//        $form['breakdown_rule_add[label]'] = '10 pax 2 twins';
//        $form['breakdown_rule_add[quantity]'] = '2';
//        $this->client->submit($form);
//
//        $crawlerFindNew = $this->client->followRedirect();
//
//        $this->assertSame('Revenue configuration', $crawlerFindNew->filter('h3')->text());
//        $this->assertSame('10 pax 2 twins', $crawlerFindNew->filter('#breakdown_rules_add tr')->last()->filter('td')->eq(1)->text());
//        $this->assertSame('2', $crawlerFindNew->filter('#breakdown_rules_add tr')->last()->filter('td')->eq(14)->text());
//    }
//
//    public function testDelete()
//    {
//        $crawlerPage = $this->client->request('GET', '/revenue-configuration');
//        $this->assertSame('Revenue configuration', $crawlerPage->filter('h3')->text());
//        $nodeButton = $crawlerPage->filter('#breakdown_rules_add tr')->last()->selectButton('Delete');
//        $form = $nodeButton->form();
//        $this->client->submit($form);
//
//        $crawlerFindNew = $this->client->followRedirect();
//
//        $this->assertSame('Revenue configuration', $crawlerFindNew->filter('h3')->text());
//        $this->assertNotEquals('10 pax 2 twins', $crawlerFindNew->filter('#breakdown_rules_add tr')->last()->filter('td')->eq(1)->text());
}