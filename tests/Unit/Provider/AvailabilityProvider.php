<?php

namespace App\Tests\Unit\Provider;

use App\Model\Availability;
use App\Model\AvailabilityRoom;
use DateTimeImmutable;

class AvailabilityProvider
{
    private $availabilities;

    public function __construct(
        array $availabilities
    ) {
        $this->availabilities = $availabilities;
    }

    public function get(): array
    {
        $start = new DateTimeImmutable();
        $end = $start->modify('+5 day');
        $period = new \DatePeriod(
            $start,
            new \DateInterval('P1D'),
            $end
        );

        $availabilities = [];

        foreach ($period as $date) {
            $dateFormat = $date->format('d/m/Y');
            $availabilities[$date->format('d/m/Y')] = $this->createAvailability($dateFormat);
        }

        return $availabilities;
    }

    public function getDynamic()
    {
        $availabilities = [];

        $start = new DateTimeImmutable();
        foreach ($this->availabilities as $availability) {
            $availability = $this->createAvailability($availability);
            $availabilities[$start->format('d/m/Y')] = $availability;
            $start = $start->modify('+1 day');
        }

        return $availabilities;
    }

    private function createAvailability(string $dateFormat): Availability
    {
        $booking = new Availability();
        $booking->setDate($dateFormat);

        foreach ($this->availabilities as $availabilityRoom) {
            $rooms[$availabilityRoom[1]] = $this->createAvailabilityRooms(
                isset($availabilityRoom[0]) ? $availabilityRoom[0] : null,
                isset($availabilityRoom[1]) ? $availabilityRoom[1] : null,
                isset($availabilityRoom['availabilityrate']) ? $availabilityRoom['availabilityrate'] : null
            );
        }

        $booking->setRooms($rooms);

        return $booking;
    }

    private function createAvailabilityRooms(?int $availability, ?string $label, ?array $rates): AvailabilityRoom
    {
        $room = new AvailabilityRoom();
        if ($availability) {
            $room->setAvailability($availability);
        }
        $room->setLabel($label);
        if ($rates) {
//            $room->setRates($this->createAvailabilityRates($rates));
        }

        return $room;
    }

//    /**
//     * @param array $rates
//     * @return AvailabilityRate[]
//     */
//    private function createAvailabilityRates(array $rates): array
//    {
//        $ret = [];
//        foreach ($rates as $rate) {
//            $availabilityRate = new AvailabilityRate();
//            $availabilityRate->setRule($rate['rate']);
//            $availabilityRate->setPrice($rate['price']);
//            $ret[$rate['rate']] = $availabilityRate;
//        }
//
//        return $ret;
//    }
}
