<?php

namespace App\Tests\Unit\Provider;

use App\Entity\RateRule;
use App\Entity\RateType;

class RateRuleProvider
{
    /**
     * @param array $params
     * @return RateRule[]
     */
    public function get(array $params): array
    {
        $ret = [];
        foreach ($params as $raterule) {
            $ret[] = $this->createRateRule(
                $raterule['baseratefix'],
                $raterule['baseratevariable'],
                $raterule['ratetypeslabel']
            );
        }

        return $ret;
    }

    /**
     * @param float $baseRateFix
     * @param float $baseRateVariable
     * @param string $rateTypeLabel
     * @return RateRule
     */
    private function createRateRule(
        float $baseRateFix,
        float $baseRateVariable,
        string $rateTypeLabel
    ): RateRule {
        $rateRule = new RateRule();
        $rateRule->setBaseRateFix($baseRateFix);
        $rateRule->setBaseRateVariable($baseRateVariable);
        $rateRule->setRateType($this->createRateType($rateTypeLabel));

        return $rateRule;
    }

    private function createRateType(string $label): RateType
    {
        $rateType = new RateType();
        $rateType->setLabel($label);

        return $rateType;
    }
}
