<?php

namespace App\Tests\Unit\Provider;

use App\Entity\BreakdownRuleTotal;
use App\Entity\RoomType;

class BreakdownRuleTotalProvider
{
    public function get(RoomType $room, $param): BreakdownRuleTotal
    {
        $entity = new BreakdownRuleTotal();
        $entity->setPercentageLeft($param['percentage']);
        $entity->setRoom($room);

        return $entity;
    }
}
