

##Coding standars 
### PHP CS Fixer
```
phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src
```
### PHP Stan
```
vendor/bin/phpstan analyse src tests
```
##Units tests
###Unit and functionnal tests
create test database
```
php bin/console doctrine:database:create --env=test
```
create schema
```
php bin/console doctrine:migrations:migrate --env=test
```
load fixtures
```
php bin/console doctrine:fixtures:load --group=testGroup --env=test
```
make tests
```
php bin/phpunit --testdox
```